#include "PythonLoader.h"
#include "wrappers/x64dbg_api.h"
#include "python_extra.h"

std::vector<PyObject *> LoadedPythonModules;

int CurrentPluginHandle = -1;

void pythonLoaderInit(PLUG_INITSTRUCT* initStruct)
{
	CurrentPluginHandle = initStruct->pluginHandle;

	Wow64EnableWow64FsRedirection(true);

	char iniPath[deflen] = "";
    GetCurrentDirectoryA(deflen, iniPath);
	strncat(iniPath, "/PythonLoader.ini", deflen);

	char pythonPath[deflen*4];
	GetPrivateProfileString("PYTHON","PYTHONPATH", NULL, pythonPath, deflen * 4, iniPath);

	std::vector<char *> pythonPaths;

	int length = strlen(pythonPath);
	for (int i = 0; i < length; i++)
	{
		if (i == 0 && pythonPath[i] != '\0')
			pythonPaths.push_back(pythonPath);

		if (pythonPath[i] == ';')
		{
			pythonPath[i] = '\0';
			pythonPaths.push_back(&pythonPath[i+1]);
		}
	}

	_plugin_logprintf("[PythonLoader] Initializing python\n");
	Py_Initialize();

	for (auto it = pythonPaths.begin(); it != pythonPaths.end(); ++it)
	{
		PyObject* sysPath = PySys_GetObject((char*)"path");
		PyObject* pPythonPath = PyString_FromString(*it);
		PyList_Insert(sysPath, 0, pPythonPath);
		Py_DECREF(pPythonPath);
	}

	// Initiliaze module for C wrapped functions in python
	_plugin_logprintf("[PythonLoader] Initializing wrapped functions\n");
	initx64dbg_api();

	// Load python base
	_plugin_logprintf("[PythonLoader] Loading python base...\n");
	LoadDirectory("pybase", false);

	// Load python plugins
	_plugin_logprintf("[PythonLoader] Loading python plugins...\n");
	LoadDirectory("pyplugins", true);

	Wow64EnableWow64FsRedirection(false);

}

/* UNUSED */
void pythonLoaderStop()
{

}

void pythonLoaderSetup()
{
    /*
	 * TODO:	
	 * - Python mappings for creating a menu
	 * - Calling python plugin for setting it up
	 */
}

void LoadDirectory(const char *dir, bool runInit)
{
	PyObject *pName, *pModule, *pFunc, *pValue;

	char currentDir[deflen] = "";
	char fullPluginDir[deflen] = "";
	char pythonImport[deflen] = "";

	const char* pluginDir = dir;

    GetCurrentDirectoryA(deflen, currentDir);
    SetCurrentDirectoryA(pluginDir);

	PyObject* sysPath = PySys_GetObject((char*)"path");
	PyObject* pPluginPath = PyString_FromString(fullPluginDir);
	PyList_Append(sysPath, pPluginPath);
	Py_DECREF(pPluginPath);

    WIN32_FIND_DATA foundData;
    HANDLE hSearch = FindFirstFileA("*.py", &foundData);
    if(hSearch == INVALID_HANDLE_VALUE)
    {
        SetCurrentDirectoryA(currentDir);
        return;
    }
    do
    {
		strncpy(pythonImport, foundData.cFileName, deflen);
		pythonImport[strlen(pythonImport) - 3] = '\0';

		_plugin_logprintf("[PythonLoader] Loading %s\n", pythonImport);

		pName = PyString_FromString(pythonImport);

		pModule = PyImport_Import(pName);
		Py_DECREF(pName);

        if (pModule != NULL && runInit) {
			pFunc = PyObject_GetAttrString(pModule, "pluginit");

			if (pFunc && PyCallable_Check(pFunc)) {
				pValue = PyObject_CallObject(pFunc, NULL);
				if (pValue != NULL) {
					if (PyInt_AsLong(pValue))
					{
						_plugin_logprintf("[PythonLoader] Loaded %s\n", pythonImport);
						LoadedPythonModules.push_back(pModule);
					} else
					{
						_plugin_logprintf("[PythonLoader] Failed loading %s\n", pythonImport);
					}
					Py_DECREF(pValue);
				}
				pythonLogError();
			}
			else {
				pythonLogError();
			}
			Py_XDECREF(pFunc);
		} else {
			pythonLogError();
		}
    }
    while(FindNextFileA(hSearch, &foundData));
    SetCurrentDirectoryA(currentDir);
}

bool pythonLogError()
{
	if (PyErr_Occurred())
	{
					
		PyObject *ptype, *pvalue, *ptraceback;
		PyErr_Fetch(&ptype, &pvalue, &ptraceback);

		if (ptype && pvalue && ptraceback)
		{
			char *pStrErrorMessage = PyString_AsString(pvalue);

			PyTracebackObject* traceback = (PyTracebackObject *) ptraceback;
			PyFrameObject *frame = (PyFrameObject *) traceback->tb_frame;

			while (NULL != frame) {
				int line = frame->f_lineno;
				const char *filename = PyString_AsString(frame->f_code->co_filename);
				const char *funcname = PyString_AsString(frame->f_code->co_name);
				_plugin_logprintf("[PythonLoader] ERROR - %s(%d): %s - %s\n", filename, line, funcname, pStrErrorMessage);
				frame = frame->f_back;
			}
		}

		Py_XDECREF(ptype);
		Py_XDECREF(pvalue);
		Py_XDECREF(ptraceback);

		return true;
	}

	return false;
}

// ######################################################
// ####################### EVENTS #######################
// ######################################################

void pythonEventExecute(const char *eventName, void *info, long infoSize)
{
	for (auto it = LoadedPythonModules.begin(); it != LoadedPythonModules.end(); it++) 
	{
		PyObject *pModule = *it;
		PyObject *pFunc = PyObject_GetAttrString(pModule, eventName);

		if (pFunc && PyCallable_Check(pFunc)) 
		{
			PyObject *args = Py_BuildValue("(s#)", (char*) info, infoSize);

			pythonLogError();

			if (args)
				{
				PyObject *pValue = PyObject_CallObject(pFunc, args);
				if (pValue != NULL) {
					Py_DECREF(pValue);
				}
				Py_DECREF(args);
			}

			pythonLogError();
		} else {
			if (PyErr_Occurred())
			{
				// Clearing last error, ignore the fact if a script doesnt have this callback
				PyObject *ptype, *pvalue, *ptraceback;
				PyErr_Fetch(&ptype, &pvalue, &ptraceback);
				Py_XDECREF(ptype);
				Py_XDECREF(pvalue);
				Py_XDECREF(ptraceback);
			}
		}
		Py_XDECREF(pFunc);
	}
}

extern "C" __declspec(dllexport) void CBINITDEBUG(CBTYPE cbType, PLUG_CB_INITDEBUG* info)
{
	pythonEventExecute("CBINITDEBUG", info, sizeof(PLUG_CB_INITDEBUG));
}

extern "C" __declspec(dllexport) void CBSTOPDEBUG(CBTYPE cbType, PLUG_CB_STOPDEBUG* info)
{
    pythonEventExecute("CBSTOPDEBUG", info, sizeof(PLUG_CB_STOPDEBUG));
} 

extern "C" __declspec(dllexport) void CBCREATEPROCESS(CBTYPE cbType, PLUG_CB_CREATEPROCESS* info)
{
    pythonEventExecute("CBCREATEPROCESS", info, sizeof(PLUG_CB_CREATEPROCESS));
} 

extern "C" __declspec(dllexport) void CBEXITPROCESS(CBTYPE cbType, PLUG_CB_EXITPROCESS* info)
{
    pythonEventExecute("CBEXITPROCESS", info, sizeof(PLUG_CB_EXITPROCESS));
} 

extern "C" __declspec(dllexport) void CBCREATETHREAD(CBTYPE cbType, PLUG_CB_CREATETHREAD* info)
{
    pythonEventExecute("CBCREATETHREAD", info, sizeof(PLUG_CB_CREATETHREAD));
} 

extern "C" __declspec(dllexport) void CBEXITTHREAD(CBTYPE cbType, PLUG_CB_EXITTHREAD* info)
{
    pythonEventExecute("CBEXITTHREAD", info, sizeof(PLUG_CB_EXITTHREAD));
} 

extern "C" __declspec(dllexport) void CBSYSTEMBREAKPOINT(CBTYPE cbType, PLUG_CB_SYSTEMBREAKPOINT* info)
{
    pythonEventExecute("CBSYSTEMBREAKPOINT", info, sizeof(PLUG_CB_SYSTEMBREAKPOINT));
} 

extern "C" __declspec(dllexport) void CBLOADDLL(CBTYPE cbType, PLUG_CB_LOADDLL* info)
{
    pythonEventExecute("CBLOADDLL", info, sizeof(PLUG_CB_LOADDLL));
} 

extern "C" __declspec(dllexport) void CBUNLOADDLL(CBTYPE cbType, PLUG_CB_UNLOADDLL* info)
{
    pythonEventExecute("CBUNLOADDLL", info, sizeof(PLUG_CB_UNLOADDLL));
}

extern "C" __declspec(dllexport) void CBOUTPUTDEBUGSTRING(CBTYPE cbType, PLUG_CB_OUTPUTDEBUGSTRING* info)
{
    pythonEventExecute("CBOUTPUTDEBUGSTRING", info, sizeof(PLUG_CB_OUTPUTDEBUGSTRING));
}

extern "C" __declspec(dllexport) void CBEXCEPTION(CBTYPE cbType, PLUG_CB_EXCEPTION* info)
{
    pythonEventExecute("CBEXCEPTION", info, sizeof(PLUG_CB_EXCEPTION));
}

extern "C" __declspec(dllexport) void CBBREAKPOINT(CBTYPE cbType, PLUG_CB_BREAKPOINT* info)
{
    pythonEventExecute("CBBREAKPOINT", info, sizeof(PLUG_CB_BREAKPOINT));
}

extern "C" __declspec(dllexport) void CBPAUSEDEBUG(CBTYPE cbType, PLUG_CB_PAUSEDEBUG* info)
{
    pythonEventExecute("CBPAUSEDEBUG", info, sizeof(PLUG_CB_PAUSEDEBUG));
}

extern "C" __declspec(dllexport) void CBRESUMEDEBUG(CBTYPE cbType, PLUG_CB_RESUMEDEBUG* info)
{
    pythonEventExecute("CBRESUMEDEBUG", info, sizeof(PLUG_CB_RESUMEDEBUG));
}

extern "C" __declspec(dllexport) void CBSTEPPED(CBTYPE cbType, PLUG_CB_STEPPED* info)
{
    pythonEventExecute("CBSTEPPED", info, sizeof(PLUG_CB_STEPPED));
}

extern "C" __declspec(dllexport) void CBATTACH(CBTYPE cbType, PLUG_CB_ATTACH* info)
{
    pythonEventExecute("CBATTACH", info, sizeof(PLUG_CB_ATTACH));
}

extern "C" __declspec(dllexport) void CBDETACH(CBTYPE cbType, PLUG_CB_WINEVENT* info)
{
    pythonEventExecute("CBDETACH", info, sizeof(PLUG_CB_WINEVENT));
}

extern "C" __declspec(dllexport) void CBDEBUGEVENT(CBTYPE cbType, PLUG_CB_DEBUGEVENT* info)
{
    pythonEventExecute("CBDEBUGEVENT", info, sizeof(PLUG_CB_DEBUGEVENT));
}

extern "C" __declspec(dllexport) void CBMENUENTRY(CBTYPE cbType, PLUG_CB_MENUENTRY* info)
{
    pythonEventExecute("CBMENUENTRY", info, sizeof(PLUG_CB_MENUENTRY));
}


extern "C" __declspec(dllexport) void CBWINEVENT(CBTYPE cbType, PLUG_CB_WINEVENT* info)
{
    pythonEventExecute("CBWINEVENT", info, sizeof(PLUG_CB_WINEVENT));
}

extern "C" __declspec(dllexport) void CBWINEVENTGLOBAL(CBTYPE cbType, PLUG_CB_WINEVENTGLOBAL* info)
{
    pythonEventExecute("CBWINEVENTGLOBAL", info, sizeof(PLUG_CB_WINEVENTGLOBAL));
}
