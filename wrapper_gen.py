#! /usr/bin/env python

import sys

import pybindgen
from pybindgen import ReturnValue, Parameter, Module, Function, FileCodeSink
from pybindgen import param, retval

def my_module_gen(out_file):

	mod = Module('x64dbg_api')
	mod.add_include('"../wrappers/_plugins_wrapper.h"')
	mod.add_include('"../pluginsdk/bridgemain.h"')
	
	##################################
	###	_plugins_wrapper.h
	##################################
	
	mod.add_function('put_log', "void", [param("const char *", "text")])
	mod.add_function('is_32_bit', "bool", [])
	mod.add_function('is_64_bit', "bool", [])
	mod.add_function('register_command', "bool", [param("PyObject *", "module", transfer_ownership=False), param("const char *", "functionName"), param("bool", "debugOnly")])
	mod.add_function('unregister_command', "bool", [param("PyObject *", "module", transfer_ownership=False), param("const char *", "functionName")])

	mod.generate(FileCodeSink(out_file) )

if __name__ == '__main__':
	file = open("wrappers/x64dbg_api.cpp", "w")
	my_module_gen(file)
	