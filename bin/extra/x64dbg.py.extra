##
## Extra definitions from x64bd_struct.py.extra
##

# Forward the api functions

import x64dbg_api

put_log = x64dbg_api.put_log
register_command = x64dbg_api.register_command
unregister_command = x64dbg_api.unregister_command

# ctype unpack

def Unpack(ctype, buf):
    cstring = create_string_buffer(buf)
    ctype_instance = cast(pointer(cstring), POINTER(ctype)).contents
    return ctype_instance

# Decorator functions

def register(debug_only):
	def _register(func):
		register_command(sys.modules[func.__module__], func.__name__, debug_only)
		
		def register_and_call(*args, **kwargs):
			return func(*args, **kwargs)
		return register_and_call
	return _register
	
def event_param(param_type):
	def _event_param(func):
		def event_param_and_call(*args, **kwargs):
			ctype_struct = Unpack(param_type, args[0])
			return func(ctype_struct)
		return event_param_and_call
	return _event_param

# XMMREGISTER struct

class struct__XMMREGISTER(Structure):
    pass

struct__XMMREGISTER.__slots__ = [
    'Low',
    'High'
]
struct__XMMREGISTER._fields_ = [
    ('Low', c_ulonglong),
    ('High', c_longlong)
]

XMMREGISTER = struct__XMMREGISTER

# YMMREGISTER struct

class struct__YMMREGISTER(Structure):
    pass

struct__YMMREGISTER.__slots__ = [
    'Low',
    'High'
]
struct__YMMREGISTER._fields_ = [
    ('Low', XMMREGISTER),
    ('High', XMMREGISTER)
]

YMMREGISTER = struct__YMMREGISTER

# REGISTERCONTEXT struct

class struct__REGISTERCONTEXT(Structure):
    pass

if (x64dbg_api.is_32_bit()):
	struct__REGISTERCONTEXT.__slots__ = [
		'cax',
		'ccx',
		'cdx',
		'cbx',
		'csp',
		'cbp',
		'csi',
		'cdi',
		'cip',
		'eflags',
		'gs',
		'fs',
		'es',
		'ds',
		'cs',
		'ss',
		'dr0',
		'dr1',
		'dr2',
		'dr3',
		'dr6',
		'dr7',
		'RegisteredArea',
		'x87fpu',
		'MxCsr',
		'XmmRegisters',
		'YmmRegisters'
	]
	struct__REGISTERCONTEXT._fields_ = [
		('cax', POINTER(c_ulong)),
		('ccx', POINTER(c_ulong)),
		('cdx', POINTER(c_ulong)),
		('cbx', POINTER(c_ulong)),
		('csp', POINTER(c_ulong)),
		('cbp', POINTER(c_ulong)),
		('csi', POINTER(c_ulong)),
		('cdi', POINTER(c_ulong)),
		('cip', POINTER(c_ulong)),
		('eflags', POINTER(c_ulong)),
		('gs', c_ushort),
		('fs', c_ushort),
		('es', c_ushort),
		('ds', c_ushort),
		('cs', c_ushort),
		('ss', c_ushort),
		('dr0', POINTER(c_ulong)),
		('dr1', POINTER(c_ulong)),
		('dr2', POINTER(c_ulong)),
		('dr3', POINTER(c_ulong)),
		('dr6', POINTER(c_ulong)),
		('dr7', POINTER(c_ulong)),
		('RegisteredArea', c_char * 80),
		('x87fpu', X87FPU),
		('MxCsr', DWORD),
		('XmmRegisters', XMMREGISTER * 8),  # is this correct?
		('YmmRegisters', YMMREGISTER * 8)   # is this correct?
	]
else:
	struct__REGISTERCONTEXT.__slots__ = [
		'cax',
		'ccx',
		'cdx',
		'cbx',
		'csp',
		'cbp',
		'csi',
		'cdi',
		'r8',
		'r9',
		'r10',
		'r11',
		'r12',
		'r13',
		'r14',
		'r15',
		'cip',
		'eflags',
		'gs',
		'fs',
		'es',
		'ds',
		'cs',
		'ss',
		'dr0',
		'dr1',
		'dr2',
		'dr3',
		'dr6',
		'dr7',
		'RegisteredArea',
		'x87fpu',
		'MxCsr',
		'XmmRegisters',
		'YmmRegisters'
	]
	struct__REGISTERCONTEXT._fields_ = [
		('cax', POINTER(c_ulong)),
		('ccx', POINTER(c_ulong)),
		('cdx', POINTER(c_ulong)),
		('cbx', POINTER(c_ulong)),
		('csp', POINTER(c_ulong)),
		('cbp', POINTER(c_ulong)),
		('csi', POINTER(c_ulong)),
		('cdi', POINTER(c_ulong)),
		('r8', POINTER(c_ulong)),
		('r9', POINTER(c_ulong)),
		('r10', POINTER(c_ulong)),
		('r11', POINTER(c_ulong)),
		('r12', POINTER(c_ulong)),
		('r13', POINTER(c_ulong)),
		('r14', POINTER(c_ulong)),
		('r15', POINTER(c_ulong)),
		('cip', POINTER(c_ulong)),
		('eflags', POINTER(c_ulong)),
		('gs', c_ushort),
		('fs', c_ushort),
		('es', c_ushort),
		('ds', c_ushort),
		('cs', c_ushort),
		('ss', c_ushort),
		('dr0', POINTER(c_ulong)),
		('dr1', POINTER(c_ulong)),
		('dr2', POINTER(c_ulong)),
		('dr3', POINTER(c_ulong)),
		('dr6', POINTER(c_ulong)),
		('dr7', POINTER(c_ulong)),
		('RegisteredArea', c_char * 80),
		('x87fpu', X87FPU),
		('MxCsr', DWORD),
		('XmmRegisters', XMMREGISTER * 16),
		('YmmRegisters', YMMREGISTER * 16)
	]
REGISTERCONTEXT = struct__REGISTERCONTEXT

# REGDUMP struct

class struct__REGDUMP(Structure):
    pass

struct__REGDUMP.__slots__ = [
    'regcontext',
    'flags',
	'x87FPURegisters',
	'mmx',
	'MxCsrFields',
	'x87StatusWordFields',
	'x87ControlWordFields',
]
struct__REGDUMP._fields_ = [
    ('regcontext', REGISTERCONTEXT),
	('flags', FLAGS),
	('x87FPURegisters', X87FPUREGISTER * 8),
	('mmx', c_ulonglong * 8),
	('MxCsrFields', MXCSRFIELDS),
	('x87StatusWordFields', X87STATUSWORDFIELDS),
    ('x87ControlWordFields', X87CONTROLWORDFIELDS)
]

REGDUMP = struct__REGDUMP

# DbgGetRegDump function

for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgGetRegDump'):
        continue
    DbgGetRegDump = _lib.DbgGetRegDump
    DbgGetRegDump.argtypes = [POINTER(REGDUMP)]
    DbgGetRegDump.restype = c_bool
    break