import x64dbg, x64dbg_struct
import TitanEngine as TE
import sys
import ctypes
from ctypes import *

def pluginit():
	x64dbg.put_log("[" + __name__ + "] Plugin init")
	return True

def plugstop():
	return True
	
@x64dbg.register(debug_only=True)
def module_name(args):
	entry = TE.GetContextData(TE.UE_CIP)
	mod = ctypes.create_string_buffer(x64dbg.MAX_MODULE_SIZE)
	
	if (x64dbg.DbgGetModuleAt(entry, cast(mod, c_char_p))):
		x64dbg.put_log("[" + __name__ + "] Module name: " + mod.value)
	else:
		x64dbg.put_log("[" + __name__ + "] Cannot get module name.")
		
	return True
	
@x64dbg.event_param(x64dbg_struct.PLUG_CB_UNLOADDLL)
def CBUNLOADDLL(info):
	x64dbg.put_log("[" + __name__ + "] DLL unloaded: " + hex(info.UnloadDll.contents.lpBaseOfDll))
	
@x64dbg.event_param(x64dbg_struct.PLUG_CB_LOADDLL)
def CBLOADDLL(info):
	x64dbg.put_log("[" + __name__ + "] DLL loaded: " + info.modname)

