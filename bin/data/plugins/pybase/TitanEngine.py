'''Wrapper for TitanEngine.h

Generated with:
E:\x64dbg_pythonloader\ctypesgen\ctypesgen.py -l TitanEngine.dll -o bin\data\plugins\pybase\TitanEngine.py pluginsdk\TitanEngine\TitanEngine.h

Do not modify this file.
'''

__docformat__ =  'restructuredtext'

# Begin preamble

import ctypes, os, sys
from ctypes import *

_int_types = (c_int16, c_int32)
if hasattr(ctypes, 'c_int64'):
    # Some builds of ctypes apparently do not have c_int64
    # defined; it's a pretty good bet that these builds do not
    # have 64-bit pointers.
    _int_types += (c_int64,)
for t in _int_types:
    if sizeof(t) == sizeof(c_size_t):
        c_ptrdiff_t = t
del t
del _int_types

class c_void(Structure):
    # c_void_p is a buggy return type, converting to int, so
    # POINTER(None) == c_void_p is actually written as
    # POINTER(c_void), so it can be treated as a real pointer.
    _fields_ = [('dummy', c_int)]

def POINTER(obj):
    p = ctypes.POINTER(obj)

    # Convert None to a real NULL pointer to work around bugs
    # in how ctypes handles None on 64-bit platforms
    if not isinstance(p.from_param, classmethod):
        def from_param(cls, x):
            if x is None:
                return cls()
            else:
                return x
        p.from_param = classmethod(from_param)

    return p

class UserString:
    def __init__(self, seq):
        if isinstance(seq, basestring):
            self.data = seq
        elif isinstance(seq, UserString):
            self.data = seq.data[:]
        else:
            self.data = str(seq)
    def __str__(self): return str(self.data)
    def __repr__(self): return repr(self.data)
    def __int__(self): return int(self.data)
    def __long__(self): return long(self.data)
    def __float__(self): return float(self.data)
    def __complex__(self): return complex(self.data)
    def __hash__(self): return hash(self.data)

    def __cmp__(self, string):
        if isinstance(string, UserString):
            return cmp(self.data, string.data)
        else:
            return cmp(self.data, string)
    def __contains__(self, char):
        return char in self.data

    def __len__(self): return len(self.data)
    def __getitem__(self, index): return self.__class__(self.data[index])
    def __getslice__(self, start, end):
        start = max(start, 0); end = max(end, 0)
        return self.__class__(self.data[start:end])

    def __add__(self, other):
        if isinstance(other, UserString):
            return self.__class__(self.data + other.data)
        elif isinstance(other, basestring):
            return self.__class__(self.data + other)
        else:
            return self.__class__(self.data + str(other))
    def __radd__(self, other):
        if isinstance(other, basestring):
            return self.__class__(other + self.data)
        else:
            return self.__class__(str(other) + self.data)
    def __mul__(self, n):
        return self.__class__(self.data*n)
    __rmul__ = __mul__
    def __mod__(self, args):
        return self.__class__(self.data % args)

    # the following methods are defined in alphabetical order:
    def capitalize(self): return self.__class__(self.data.capitalize())
    def center(self, width, *args):
        return self.__class__(self.data.center(width, *args))
    def count(self, sub, start=0, end=sys.maxint):
        return self.data.count(sub, start, end)
    def decode(self, encoding=None, errors=None): # XXX improve this?
        if encoding:
            if errors:
                return self.__class__(self.data.decode(encoding, errors))
            else:
                return self.__class__(self.data.decode(encoding))
        else:
            return self.__class__(self.data.decode())
    def encode(self, encoding=None, errors=None): # XXX improve this?
        if encoding:
            if errors:
                return self.__class__(self.data.encode(encoding, errors))
            else:
                return self.__class__(self.data.encode(encoding))
        else:
            return self.__class__(self.data.encode())
    def endswith(self, suffix, start=0, end=sys.maxint):
        return self.data.endswith(suffix, start, end)
    def expandtabs(self, tabsize=8):
        return self.__class__(self.data.expandtabs(tabsize))
    def find(self, sub, start=0, end=sys.maxint):
        return self.data.find(sub, start, end)
    def index(self, sub, start=0, end=sys.maxint):
        return self.data.index(sub, start, end)
    def isalpha(self): return self.data.isalpha()
    def isalnum(self): return self.data.isalnum()
    def isdecimal(self): return self.data.isdecimal()
    def isdigit(self): return self.data.isdigit()
    def islower(self): return self.data.islower()
    def isnumeric(self): return self.data.isnumeric()
    def isspace(self): return self.data.isspace()
    def istitle(self): return self.data.istitle()
    def isupper(self): return self.data.isupper()
    def join(self, seq): return self.data.join(seq)
    def ljust(self, width, *args):
        return self.__class__(self.data.ljust(width, *args))
    def lower(self): return self.__class__(self.data.lower())
    def lstrip(self, chars=None): return self.__class__(self.data.lstrip(chars))
    def partition(self, sep):
        return self.data.partition(sep)
    def replace(self, old, new, maxsplit=-1):
        return self.__class__(self.data.replace(old, new, maxsplit))
    def rfind(self, sub, start=0, end=sys.maxint):
        return self.data.rfind(sub, start, end)
    def rindex(self, sub, start=0, end=sys.maxint):
        return self.data.rindex(sub, start, end)
    def rjust(self, width, *args):
        return self.__class__(self.data.rjust(width, *args))
    def rpartition(self, sep):
        return self.data.rpartition(sep)
    def rstrip(self, chars=None): return self.__class__(self.data.rstrip(chars))
    def split(self, sep=None, maxsplit=-1):
        return self.data.split(sep, maxsplit)
    def rsplit(self, sep=None, maxsplit=-1):
        return self.data.rsplit(sep, maxsplit)
    def splitlines(self, keepends=0): return self.data.splitlines(keepends)
    def startswith(self, prefix, start=0, end=sys.maxint):
        return self.data.startswith(prefix, start, end)
    def strip(self, chars=None): return self.__class__(self.data.strip(chars))
    def swapcase(self): return self.__class__(self.data.swapcase())
    def title(self): return self.__class__(self.data.title())
    def translate(self, *args):
        return self.__class__(self.data.translate(*args))
    def upper(self): return self.__class__(self.data.upper())
    def zfill(self, width): return self.__class__(self.data.zfill(width))

class MutableString(UserString):
    """mutable string objects

    Python strings are immutable objects.  This has the advantage, that
    strings may be used as dictionary keys.  If this property isn't needed
    and you insist on changing string values in place instead, you may cheat
    and use MutableString.

    But the purpose of this class is an educational one: to prevent
    people from inventing their own mutable string class derived
    from UserString and than forget thereby to remove (override) the
    __hash__ method inherited from UserString.  This would lead to
    errors that would be very hard to track down.

    A faster and better solution is to rewrite your program using lists."""
    def __init__(self, string=""):
        self.data = string
    def __hash__(self):
        raise TypeError("unhashable type (it is mutable)")
    def __setitem__(self, index, sub):
        if index < 0:
            index += len(self.data)
        if index < 0 or index >= len(self.data): raise IndexError
        self.data = self.data[:index] + sub + self.data[index+1:]
    def __delitem__(self, index):
        if index < 0:
            index += len(self.data)
        if index < 0 or index >= len(self.data): raise IndexError
        self.data = self.data[:index] + self.data[index+1:]
    def __setslice__(self, start, end, sub):
        start = max(start, 0); end = max(end, 0)
        if isinstance(sub, UserString):
            self.data = self.data[:start]+sub.data+self.data[end:]
        elif isinstance(sub, basestring):
            self.data = self.data[:start]+sub+self.data[end:]
        else:
            self.data =  self.data[:start]+str(sub)+self.data[end:]
    def __delslice__(self, start, end):
        start = max(start, 0); end = max(end, 0)
        self.data = self.data[:start] + self.data[end:]
    def immutable(self):
        return UserString(self.data)
    def __iadd__(self, other):
        if isinstance(other, UserString):
            self.data += other.data
        elif isinstance(other, basestring):
            self.data += other
        else:
            self.data += str(other)
        return self
    def __imul__(self, n):
        self.data *= n
        return self

class String(MutableString, Union):

    _fields_ = [('raw', POINTER(c_char)),
                ('data', c_char_p)]

    def __init__(self, obj=""):
        if isinstance(obj, (str, unicode, UserString)):
            self.data = str(obj)
        else:
            self.raw = obj

    def __len__(self):
        return self.data and len(self.data) or 0

    def from_param(cls, obj):
        # Convert None or 0
        if obj is None or obj == 0:
            return cls(POINTER(c_char)())

        # Convert from String
        elif isinstance(obj, String):
            return obj

        # Convert from str
        elif isinstance(obj, str):
            return cls(obj)

        # Convert from c_char_p
        elif isinstance(obj, c_char_p):
            return obj

        # Convert from POINTER(c_char)
        elif isinstance(obj, POINTER(c_char)):
            return obj

        # Convert from raw pointer
        elif isinstance(obj, int):
            return cls(cast(obj, POINTER(c_char)))

        # Convert from object
        else:
            return String.from_param(obj._as_parameter_)
    from_param = classmethod(from_param)

def ReturnString(obj, func=None, arguments=None):
    return String.from_param(obj)

# As of ctypes 1.0, ctypes does not support custom error-checking
# functions on callbacks, nor does it support custom datatypes on
# callbacks, so we must ensure that all callbacks return
# primitive datatypes.
#
# Non-primitive return values wrapped with UNCHECKED won't be
# typechecked, and will be converted to c_void_p.
def UNCHECKED(type):
    if (hasattr(type, "_type_") and isinstance(type._type_, str)
        and type._type_ != "P"):
        return type
    else:
        return c_void_p

# ctypes doesn't have direct support for variadic functions, so we have to write
# our own wrapper class
class _variadic_function(object):
    def __init__(self,func,restype,argtypes):
        self.func=func
        self.func.restype=restype
        self.argtypes=argtypes
    def _as_parameter_(self):
        # So we can pass this variadic function as a function pointer
        return self.func
    def __call__(self,*args):
        fixed_args=[]
        i=0
        for argtype in self.argtypes:
            # Typecheck what we can
            fixed_args.append(argtype.from_param(args[i]))
            i+=1
        return self.func(*fixed_args+list(args[i:]))

# End preamble

_libs = {}
_libdirs = []

# Begin loader

# ----------------------------------------------------------------------------
# Copyright (c) 2008 David James
# Copyright (c) 2006-2008 Alex Holkner
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of pyglet nor the names of its
#    contributors may be used to endorse or promote products
#    derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# ----------------------------------------------------------------------------

import os.path, re, sys, glob
import platform
import ctypes
import ctypes.util

def _environ_path(name):
    if name in os.environ:
        return os.environ[name].split(":")
    else:
        return []

class LibraryLoader(object):
    def __init__(self):
        self.other_dirs=[]

    def load_library(self,libname):
        """Given the name of a library, load it."""
        paths = self.getpaths(libname)

        for path in paths:
            if os.path.exists(path):
                return self.load(path)

        raise ImportError("%s not found." % libname)

    def load(self,path):
        """Given a path to a library, load it."""
        try:
            # Darwin requires dlopen to be called with mode RTLD_GLOBAL instead
            # of the default RTLD_LOCAL.  Without this, you end up with
            # libraries not being loadable, resulting in "Symbol not found"
            # errors
            if sys.platform == 'darwin':
                return ctypes.CDLL(path, ctypes.RTLD_GLOBAL)
            else:
                return ctypes.cdll.LoadLibrary(path)
        except OSError,e:
            raise ImportError(e)

    def getpaths(self,libname):
        """Return a list of paths where the library might be found."""
        if os.path.isabs(libname):
            yield libname
        else:
            # FIXME / TODO return '.' and os.path.dirname(__file__)
            for path in self.getplatformpaths(libname):
                yield path

            path = ctypes.util.find_library(libname)
            if path: yield path

    def getplatformpaths(self, libname):
        return []

# Darwin (Mac OS X)

class DarwinLibraryLoader(LibraryLoader):
    name_formats = ["lib%s.dylib", "lib%s.so", "lib%s.bundle", "%s.dylib",
                "%s.so", "%s.bundle", "%s"]

    def getplatformpaths(self,libname):
        if os.path.pathsep in libname:
            names = [libname]
        else:
            names = [format % libname for format in self.name_formats]

        for dir in self.getdirs(libname):
            for name in names:
                yield os.path.join(dir,name)

    def getdirs(self,libname):
        '''Implements the dylib search as specified in Apple documentation:

        http://developer.apple.com/documentation/DeveloperTools/Conceptual/
            DynamicLibraries/Articles/DynamicLibraryUsageGuidelines.html

        Before commencing the standard search, the method first checks
        the bundle's ``Frameworks`` directory if the application is running
        within a bundle (OS X .app).
        '''

        dyld_fallback_library_path = _environ_path("DYLD_FALLBACK_LIBRARY_PATH")
        if not dyld_fallback_library_path:
            dyld_fallback_library_path = [os.path.expanduser('~/lib'),
                                          '/usr/local/lib', '/usr/lib']

        dirs = []

        if '/' in libname:
            dirs.extend(_environ_path("DYLD_LIBRARY_PATH"))
        else:
            dirs.extend(_environ_path("LD_LIBRARY_PATH"))
            dirs.extend(_environ_path("DYLD_LIBRARY_PATH"))

        dirs.extend(self.other_dirs)
        dirs.append(".")
        dirs.append(os.path.dirname(__file__))

        if hasattr(sys, 'frozen') and sys.frozen == 'macosx_app':
            dirs.append(os.path.join(
                os.environ['RESOURCEPATH'],
                '..',
                'Frameworks'))

        dirs.extend(dyld_fallback_library_path)

        return dirs

# Posix

class PosixLibraryLoader(LibraryLoader):
    _ld_so_cache = None

    def _create_ld_so_cache(self):
        # Recreate search path followed by ld.so.  This is going to be
        # slow to build, and incorrect (ld.so uses ld.so.cache, which may
        # not be up-to-date).  Used only as fallback for distros without
        # /sbin/ldconfig.
        #
        # We assume the DT_RPATH and DT_RUNPATH binary sections are omitted.

        directories = []
        for name in ("LD_LIBRARY_PATH",
                     "SHLIB_PATH", # HPUX
                     "LIBPATH", # OS/2, AIX
                     "LIBRARY_PATH", # BE/OS
                    ):
            if name in os.environ:
                directories.extend(os.environ[name].split(os.pathsep))
        directories.extend(self.other_dirs)
        directories.append(".")
        directories.append(os.path.dirname(__file__))

        try: directories.extend([dir.strip() for dir in open('/etc/ld.so.conf')])
        except IOError: pass

        unix_lib_dirs_list = ['/lib', '/usr/lib', '/lib64', '/usr/lib64']
        if sys.platform.startswith('linux'):
            # Try and support multiarch work in Ubuntu
            # https://wiki.ubuntu.com/MultiarchSpec
            bitage = platform.architecture()[0]
            if bitage.startswith('32'):
                # Assume Intel/AMD x86 compat
                unix_lib_dirs_list += ['/lib/i386-linux-gnu', '/usr/lib/i386-linux-gnu']
            elif bitage.startswith('64'):
                # Assume Intel/AMD x86 compat
                unix_lib_dirs_list += ['/lib/x86_64-linux-gnu', '/usr/lib/x86_64-linux-gnu']
            else:
                # guess...
                unix_lib_dirs_list += glob.glob('/lib/*linux-gnu')
        directories.extend(unix_lib_dirs_list)

        cache = {}
        lib_re = re.compile(r'lib(.*)\.s[ol]')
        ext_re = re.compile(r'\.s[ol]$')
        for dir in directories:
            try:
                for path in glob.glob("%s/*.s[ol]*" % dir):
                    file = os.path.basename(path)

                    # Index by filename
                    if file not in cache:
                        cache[file] = path

                    # Index by library name
                    match = lib_re.match(file)
                    if match:
                        library = match.group(1)
                        if library not in cache:
                            cache[library] = path
            except OSError:
                pass

        self._ld_so_cache = cache

    def getplatformpaths(self, libname):
        if self._ld_so_cache is None:
            self._create_ld_so_cache()

        result = self._ld_so_cache.get(libname)
        if result: yield result

        path = ctypes.util.find_library(libname)
        if path: yield os.path.join("/lib",path)

# Windows

class _WindowsLibrary(object):
    def __init__(self, path):
        self.cdll = ctypes.cdll.LoadLibrary(path)
        self.windll = ctypes.windll.LoadLibrary(path)

    def __getattr__(self, name):
        try: return getattr(self.cdll,name)
        except AttributeError:
            try: return getattr(self.windll,name)
            except AttributeError:
                raise

class WindowsLibraryLoader(LibraryLoader):
    name_formats = ["%s.dll", "lib%s.dll", "%slib.dll"]

    def load_library(self, libname):
        try:
            result = LibraryLoader.load_library(self, libname)
        except ImportError:
            result = None
            if os.path.sep not in libname:
                for name in self.name_formats:
                    try:
                        result = getattr(ctypes.cdll, name % libname)
                        if result:
                            break
                    except WindowsError:
                        result = None
            if result is None:
                try:
                    result = getattr(ctypes.cdll, libname)
                except WindowsError:
                    result = None
            if result is None:
                raise ImportError("%s not found." % libname)
        return result

    def load(self, path):
        return _WindowsLibrary(path)

    def getplatformpaths(self, libname):
        if os.path.sep not in libname:
            for name in self.name_formats:
                dll_in_current_dir = os.path.abspath(name % libname)
                if os.path.exists(dll_in_current_dir):
                    yield dll_in_current_dir
                path = ctypes.util.find_library(name % libname)
                if path:
                    yield path

# Platform switching

# If your value of sys.platform does not appear in this dict, please contact
# the Ctypesgen maintainers.

loaderclass = {
    "darwin":   DarwinLibraryLoader,
    "cygwin":   WindowsLibraryLoader,
    "win32":    WindowsLibraryLoader
}

loader = loaderclass.get(sys.platform, PosixLibraryLoader)()

def add_library_search_dirs(other_dirs):
    loader.other_dirs = other_dirs

load_library = loader.load_library

del loaderclass

# End loader

add_library_search_dirs([])

# Begin libraries

_libs["TitanEngine.dll"] = load_library("TitanEngine.dll")

# 1 libraries
# End libraries

# No modules

ULONG = c_ulong # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 25

BYTE = c_ubyte # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 115

WORD = c_ushort # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 116

DWORD = c_ulong # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 117

LPBYTE = POINTER(BYTE) # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 121

LPVOID = POINTER(None) # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 129

ULONG_PTR = c_ulong # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\basetsd.h: 56

LONG = c_long # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winnt.h: 192

WCHAR = c_wchar # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winnt.h: 196

LPWSTR = POINTER(WCHAR) # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winnt.h: 200

HANDLE = POINTER(None) # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winnt.h: 247

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 210
class struct_HINSTANCE__(Structure):
    pass

struct_HINSTANCE__.__slots__ = [
    'unused',
]
struct_HINSTANCE__._fields_ = [
    ('unused', c_int),
]

HINSTANCE = POINTER(struct_HINSTANCE__) # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 210

HMODULE = HINSTANCE # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 211

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 191
class struct__PROCESS_INFORMATION(Structure):
    pass

struct__PROCESS_INFORMATION.__slots__ = [
    'hProcess',
    'hThread',
    'dwProcessId',
    'dwThreadId',
]
struct__PROCESS_INFORMATION._fields_ = [
    ('hProcess', HANDLE),
    ('hThread', HANDLE),
    ('dwProcessId', DWORD),
    ('dwThreadId', DWORD),
]

PROCESS_INFORMATION = struct__PROCESS_INFORMATION # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 191

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 1761
class struct__STARTUPINFOW(Structure):
    pass

struct__STARTUPINFOW.__slots__ = [
    'cb',
    'lpReserved',
    'lpDesktop',
    'lpTitle',
    'dwX',
    'dwY',
    'dwXSize',
    'dwYSize',
    'dwXCountChars',
    'dwYCountChars',
    'dwFillAttribute',
    'dwFlags',
    'wShowWindow',
    'cbReserved2',
    'lpReserved2',
    'hStdInput',
    'hStdOutput',
    'hStdError',
]
struct__STARTUPINFOW._fields_ = [
    ('cb', DWORD),
    ('lpReserved', LPWSTR),
    ('lpDesktop', LPWSTR),
    ('lpTitle', LPWSTR),
    ('dwX', DWORD),
    ('dwY', DWORD),
    ('dwXSize', DWORD),
    ('dwYSize', DWORD),
    ('dwXCountChars', DWORD),
    ('dwYCountChars', DWORD),
    ('dwFillAttribute', DWORD),
    ('dwFlags', DWORD),
    ('wShowWindow', WORD),
    ('cbReserved2', WORD),
    ('lpReserved2', LPBYTE),
    ('hStdInput', HANDLE),
    ('hStdOutput', HANDLE),
    ('hStdError', HANDLE),
]

STARTUPINFOW = struct__STARTUPINFOW # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 1761

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 352
class struct_anon_539(Structure):
    pass

struct_anon_539.__slots__ = [
    'PE32Offset',
    'ImageBase',
    'OriginalEntryPoint',
    'BaseOfCode',
    'BaseOfData',
    'NtSizeOfImage',
    'NtSizeOfHeaders',
    'SizeOfOptionalHeaders',
    'FileAlignment',
    'SectionAligment',
    'ImportTableAddress',
    'ImportTableSize',
    'ResourceTableAddress',
    'ResourceTableSize',
    'ExportTableAddress',
    'ExportTableSize',
    'TLSTableAddress',
    'TLSTableSize',
    'RelocationTableAddress',
    'RelocationTableSize',
    'TimeDateStamp',
    'SectionNumber',
    'CheckSum',
    'SubSystem',
    'Characteristics',
    'NumberOfRvaAndSizes',
]
struct_anon_539._fields_ = [
    ('PE32Offset', DWORD),
    ('ImageBase', DWORD),
    ('OriginalEntryPoint', DWORD),
    ('BaseOfCode', DWORD),
    ('BaseOfData', DWORD),
    ('NtSizeOfImage', DWORD),
    ('NtSizeOfHeaders', DWORD),
    ('SizeOfOptionalHeaders', WORD),
    ('FileAlignment', DWORD),
    ('SectionAligment', DWORD),
    ('ImportTableAddress', DWORD),
    ('ImportTableSize', DWORD),
    ('ResourceTableAddress', DWORD),
    ('ResourceTableSize', DWORD),
    ('ExportTableAddress', DWORD),
    ('ExportTableSize', DWORD),
    ('TLSTableAddress', DWORD),
    ('TLSTableSize', DWORD),
    ('RelocationTableAddress', DWORD),
    ('RelocationTableSize', DWORD),
    ('TimeDateStamp', DWORD),
    ('SectionNumber', WORD),
    ('CheckSum', DWORD),
    ('SubSystem', WORD),
    ('Characteristics', WORD),
    ('NumberOfRvaAndSizes', DWORD),
]

PE32Struct = struct_anon_539 # E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 352

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 324
class struct_anon_540(Structure):
    pass

struct_anon_540.__slots__ = [
    'PE32Offset',
    'ImageBase',
    'OriginalEntryPoint',
    'BaseOfCode',
    'BaseOfData',
    'NtSizeOfImage',
    'NtSizeOfHeaders',
    'SizeOfOptionalHeaders',
    'FileAlignment',
    'SectionAligment',
    'ImportTableAddress',
    'ImportTableSize',
    'ResourceTableAddress',
    'ResourceTableSize',
    'ExportTableAddress',
    'ExportTableSize',
    'TLSTableAddress',
    'TLSTableSize',
    'RelocationTableAddress',
    'RelocationTableSize',
    'TimeDateStamp',
    'SectionNumber',
    'CheckSum',
    'SubSystem',
    'Characteristics',
    'NumberOfRvaAndSizes',
]
struct_anon_540._fields_ = [
    ('PE32Offset', DWORD),
    ('ImageBase', DWORD),
    ('OriginalEntryPoint', DWORD),
    ('BaseOfCode', DWORD),
    ('BaseOfData', DWORD),
    ('NtSizeOfImage', DWORD),
    ('NtSizeOfHeaders', DWORD),
    ('SizeOfOptionalHeaders', WORD),
    ('FileAlignment', DWORD),
    ('SectionAligment', DWORD),
    ('ImportTableAddress', DWORD),
    ('ImportTableSize', DWORD),
    ('ResourceTableAddress', DWORD),
    ('ResourceTableSize', DWORD),
    ('ExportTableAddress', DWORD),
    ('ExportTableSize', DWORD),
    ('TLSTableAddress', DWORD),
    ('TLSTableSize', DWORD),
    ('RelocationTableAddress', DWORD),
    ('RelocationTableSize', DWORD),
    ('TimeDateStamp', DWORD),
    ('SectionNumber', WORD),
    ('CheckSum', DWORD),
    ('SubSystem', WORD),
    ('Characteristics', WORD),
    ('NumberOfRvaAndSizes', DWORD),
]

PPE32Struct = POINTER(struct_anon_540) # E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 352

PEStruct = PE32Struct # E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 387

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 393
for _lib in _libs.values():
    try:
        NumberOfImports = (c_int).in_dll(_lib, 'NumberOfImports')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 394
for _lib in _libs.values():
    try:
        ImageBase = (ULONG_PTR).in_dll(_lib, 'ImageBase')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 395
for _lib in _libs.values():
    try:
        BaseImportThunk = (ULONG_PTR).in_dll(_lib, 'BaseImportThunk')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 396
for _lib in _libs.values():
    try:
        ImportThunk = (ULONG_PTR).in_dll(_lib, 'ImportThunk')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 397
for _lib in _libs.values():
    try:
        APIName = (String).in_dll(_lib, 'APIName')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 398
for _lib in _libs.values():
    try:
        DLLName = (String).in_dll(_lib, 'DLLName')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 414
class struct_anon_543(Structure):
    pass

struct_anon_543.__slots__ = [
    'hThread',
    'dwThreadId',
    'ThreadStartAddress',
    'ThreadLocalBase',
    'TebAddress',
    'WaitTime',
    'Priority',
    'BasePriority',
    'ContextSwitches',
    'ThreadState',
    'WaitReason',
]
struct_anon_543._fields_ = [
    ('hThread', HANDLE),
    ('dwThreadId', DWORD),
    ('ThreadStartAddress', POINTER(None)),
    ('ThreadLocalBase', POINTER(None)),
    ('TebAddress', POINTER(None)),
    ('WaitTime', ULONG),
    ('Priority', LONG),
    ('BasePriority', LONG),
    ('ContextSwitches', ULONG),
    ('ThreadState', ULONG),
    ('WaitReason', ULONG),
]

THREAD_ITEM_DATA = struct_anon_543 # E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 414

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 401
class struct_anon_544(Structure):
    pass

struct_anon_544.__slots__ = [
    'hThread',
    'dwThreadId',
    'ThreadStartAddress',
    'ThreadLocalBase',
    'TebAddress',
    'WaitTime',
    'Priority',
    'BasePriority',
    'ContextSwitches',
    'ThreadState',
    'WaitReason',
]
struct_anon_544._fields_ = [
    ('hThread', HANDLE),
    ('dwThreadId', DWORD),
    ('ThreadStartAddress', POINTER(None)),
    ('ThreadLocalBase', POINTER(None)),
    ('TebAddress', POINTER(None)),
    ('WaitTime', ULONG),
    ('Priority', LONG),
    ('BasePriority', LONG),
    ('ContextSwitches', ULONG),
    ('ThreadState', ULONG),
    ('WaitReason', ULONG),
]

PTHREAD_ITEM_DATA = POINTER(struct_anon_544) # E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 414

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 424
class struct_anon_545(Structure):
    pass

struct_anon_545.__slots__ = [
    'hFile',
    'BaseOfDll',
    'hFileMapping',
    'hFileMappingView',
    'szLibraryPath',
    'szLibraryName',
]
struct_anon_545._fields_ = [
    ('hFile', HANDLE),
    ('BaseOfDll', POINTER(None)),
    ('hFileMapping', HANDLE),
    ('hFileMappingView', POINTER(None)),
    ('szLibraryPath', c_char * 260),
    ('szLibraryName', c_char * 260),
]

LIBRARY_ITEM_DATA = struct_anon_545 # E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 424

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 416
class struct_anon_546(Structure):
    pass

struct_anon_546.__slots__ = [
    'hFile',
    'BaseOfDll',
    'hFileMapping',
    'hFileMappingView',
    'szLibraryPath',
    'szLibraryName',
]
struct_anon_546._fields_ = [
    ('hFile', HANDLE),
    ('BaseOfDll', POINTER(None)),
    ('hFileMapping', HANDLE),
    ('hFileMappingView', POINTER(None)),
    ('szLibraryPath', c_char * 260),
    ('szLibraryName', c_char * 260),
]

PLIBRARY_ITEM_DATA = POINTER(struct_anon_546) # E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 424

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 434
class struct_anon_547(Structure):
    pass

struct_anon_547.__slots__ = [
    'hFile',
    'BaseOfDll',
    'hFileMapping',
    'hFileMappingView',
    'szLibraryPath',
    'szLibraryName',
]
struct_anon_547._fields_ = [
    ('hFile', HANDLE),
    ('BaseOfDll', POINTER(None)),
    ('hFileMapping', HANDLE),
    ('hFileMappingView', POINTER(None)),
    ('szLibraryPath', c_wchar * 260),
    ('szLibraryName', c_wchar * 260),
]

LIBRARY_ITEM_DATAW = struct_anon_547 # E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 434

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 426
class struct_anon_548(Structure):
    pass

struct_anon_548.__slots__ = [
    'hFile',
    'BaseOfDll',
    'hFileMapping',
    'hFileMappingView',
    'szLibraryPath',
    'szLibraryName',
]
struct_anon_548._fields_ = [
    ('hFile', HANDLE),
    ('BaseOfDll', POINTER(None)),
    ('hFileMapping', HANDLE),
    ('hFileMappingView', POINTER(None)),
    ('szLibraryPath', c_wchar * 260),
    ('szLibraryName', c_wchar * 260),
]

PLIBRARY_ITEM_DATAW = POINTER(struct_anon_548) # E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 434

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 446
class struct_anon_549(Structure):
    pass

struct_anon_549.__slots__ = [
    'hProcess',
    'dwProcessId',
    'hThread',
    'dwThreadId',
    'hFile',
    'BaseOfImage',
    'ThreadStartAddress',
    'ThreadLocalBase',
]
struct_anon_549._fields_ = [
    ('hProcess', HANDLE),
    ('dwProcessId', DWORD),
    ('hThread', HANDLE),
    ('dwThreadId', DWORD),
    ('hFile', HANDLE),
    ('BaseOfImage', POINTER(None)),
    ('ThreadStartAddress', POINTER(None)),
    ('ThreadLocalBase', POINTER(None)),
]

PROCESS_ITEM_DATA = struct_anon_549 # E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 446

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 436
class struct_anon_550(Structure):
    pass

struct_anon_550.__slots__ = [
    'hProcess',
    'dwProcessId',
    'hThread',
    'dwThreadId',
    'hFile',
    'BaseOfImage',
    'ThreadStartAddress',
    'ThreadLocalBase',
]
struct_anon_550._fields_ = [
    ('hProcess', HANDLE),
    ('dwProcessId', DWORD),
    ('hThread', HANDLE),
    ('dwThreadId', DWORD),
    ('hFile', HANDLE),
    ('BaseOfImage', POINTER(None)),
    ('ThreadStartAddress', POINTER(None)),
    ('ThreadLocalBase', POINTER(None)),
]

PPROCESS_ITEM_DATA = POINTER(struct_anon_550) # E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 446

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 452
class struct_anon_551(Structure):
    pass

struct_anon_551.__slots__ = [
    'ProcessId',
    'hHandle',
]
struct_anon_551._fields_ = [
    ('ProcessId', ULONG),
    ('hHandle', HANDLE),
]

HandlerArray = struct_anon_551 # E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 452

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 448
class struct_anon_552(Structure):
    pass

struct_anon_552.__slots__ = [
    'ProcessId',
    'hHandle',
]
struct_anon_552._fields_ = [
    ('ProcessId', ULONG),
    ('hHandle', HANDLE),
]

PHandlerArray = POINTER(struct_anon_552) # E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 452

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 478
for _lib in _libs.values():
    try:
        HookType = (BYTE).in_dll(_lib, 'HookType')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 479
for _lib in _libs.values():
    try:
        HookSize = (DWORD).in_dll(_lib, 'HookSize')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 480
for _lib in _libs.values():
    try:
        HookAddress = (POINTER(None)).in_dll(_lib, 'HookAddress')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 481
for _lib in _libs.values():
    try:
        RedirectionAddress = (POINTER(None)).in_dll(_lib, 'RedirectionAddress')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 482
for _lib in _libs.values():
    try:
        HookBytes = (BYTE * 14).in_dll(_lib, 'HookBytes')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 483
for _lib in _libs.values():
    try:
        OriginalBytes = (BYTE * 14).in_dll(_lib, 'OriginalBytes')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 484
for _lib in _libs.values():
    try:
        IATHookModuleBase = (POINTER(None)).in_dll(_lib, 'IATHookModuleBase')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 485
for _lib in _libs.values():
    try:
        IATHookNameHash = (DWORD).in_dll(_lib, 'IATHookNameHash')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 488
for _lib in _libs.values():
    try:
        PatchedEntry = (POINTER(None)).in_dll(_lib, 'PatchedEntry')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 489
for _lib in _libs.values():
    try:
        RelocationInfo = (DWORD * 7).in_dll(_lib, 'RelocationInfo')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 490
for _lib in _libs.values():
    try:
        RelocationCount = (c_int).in_dll(_lib, 'RelocationCount')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 528
for _lib in _libs.values():
    try:
        SignatureMZ = (BYTE).in_dll(_lib, 'SignatureMZ')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 529
for _lib in _libs.values():
    try:
        SignaturePE = (BYTE).in_dll(_lib, 'SignaturePE')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 530
for _lib in _libs.values():
    try:
        EntryPoint = (BYTE).in_dll(_lib, 'EntryPoint')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 531
for _lib in _libs.values():
    try:
        ImageBase = (BYTE).in_dll(_lib, 'ImageBase')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 532
for _lib in _libs.values():
    try:
        SizeOfImage = (BYTE).in_dll(_lib, 'SizeOfImage')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 533
for _lib in _libs.values():
    try:
        FileAlignment = (BYTE).in_dll(_lib, 'FileAlignment')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 534
for _lib in _libs.values():
    try:
        SectionAlignment = (BYTE).in_dll(_lib, 'SectionAlignment')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 535
for _lib in _libs.values():
    try:
        ExportTable = (BYTE).in_dll(_lib, 'ExportTable')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 536
for _lib in _libs.values():
    try:
        RelocationTable = (BYTE).in_dll(_lib, 'RelocationTable')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 537
for _lib in _libs.values():
    try:
        ImportTable = (BYTE).in_dll(_lib, 'ImportTable')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 538
for _lib in _libs.values():
    try:
        ImportTableSection = (BYTE).in_dll(_lib, 'ImportTableSection')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 539
for _lib in _libs.values():
    try:
        ImportTableData = (BYTE).in_dll(_lib, 'ImportTableData')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 540
for _lib in _libs.values():
    try:
        IATTable = (BYTE).in_dll(_lib, 'IATTable')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 541
for _lib in _libs.values():
    try:
        TLSTable = (BYTE).in_dll(_lib, 'TLSTable')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 542
for _lib in _libs.values():
    try:
        LoadConfigTable = (BYTE).in_dll(_lib, 'LoadConfigTable')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 543
for _lib in _libs.values():
    try:
        BoundImportTable = (BYTE).in_dll(_lib, 'BoundImportTable')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 544
for _lib in _libs.values():
    try:
        COMHeaderTable = (BYTE).in_dll(_lib, 'COMHeaderTable')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 545
for _lib in _libs.values():
    try:
        ResourceTable = (BYTE).in_dll(_lib, 'ResourceTable')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 546
for _lib in _libs.values():
    try:
        ResourceData = (BYTE).in_dll(_lib, 'ResourceData')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 547
for _lib in _libs.values():
    try:
        SectionTable = (BYTE).in_dll(_lib, 'SectionTable')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 557
for _lib in _libs.values():
    try:
        OriginalRelocationTableAddress = (DWORD).in_dll(_lib, 'OriginalRelocationTableAddress')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 558
for _lib in _libs.values():
    try:
        OriginalRelocationTableSize = (DWORD).in_dll(_lib, 'OriginalRelocationTableSize')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 561
for _lib in _libs.values():
    try:
        OriginalExportTableAddress = (DWORD).in_dll(_lib, 'OriginalExportTableAddress')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 562
for _lib in _libs.values():
    try:
        OriginalExportTableSize = (DWORD).in_dll(_lib, 'OriginalExportTableSize')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 565
for _lib in _libs.values():
    try:
        OriginalResourceTableAddress = (DWORD).in_dll(_lib, 'OriginalResourceTableAddress')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 566
for _lib in _libs.values():
    try:
        OriginalResourceTableSize = (DWORD).in_dll(_lib, 'OriginalResourceTableSize')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 569
for _lib in _libs.values():
    try:
        OriginalTLSTableAddress = (DWORD).in_dll(_lib, 'OriginalTLSTableAddress')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 570
for _lib in _libs.values():
    try:
        OriginalTLSTableSize = (DWORD).in_dll(_lib, 'OriginalTLSTableSize')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 573
for _lib in _libs.values():
    try:
        OriginalLoadConfigTableAddress = (DWORD).in_dll(_lib, 'OriginalLoadConfigTableAddress')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 574
for _lib in _libs.values():
    try:
        OriginalLoadConfigTableSize = (DWORD).in_dll(_lib, 'OriginalLoadConfigTableSize')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 577
for _lib in _libs.values():
    try:
        OriginalBoundImportTableAddress = (DWORD).in_dll(_lib, 'OriginalBoundImportTableAddress')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 578
for _lib in _libs.values():
    try:
        OriginalBoundImportTableSize = (DWORD).in_dll(_lib, 'OriginalBoundImportTableSize')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 581
for _lib in _libs.values():
    try:
        OriginalImportAddressTableAddress = (DWORD).in_dll(_lib, 'OriginalImportAddressTableAddress')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 582
for _lib in _libs.values():
    try:
        OriginalImportAddressTableSize = (DWORD).in_dll(_lib, 'OriginalImportAddressTableSize')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 585
for _lib in _libs.values():
    try:
        OriginalCOMTableAddress = (DWORD).in_dll(_lib, 'OriginalCOMTableAddress')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 586
for _lib in _libs.values():
    try:
        OriginalCOMTableSize = (DWORD).in_dll(_lib, 'OriginalCOMTableSize')
        break
    except:
        pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 606
class struct_anon_554(Structure):
    pass

struct_anon_554.__slots__ = [
    'data',
    'st_value',
    'tag',
]
struct_anon_554._fields_ = [
    ('data', BYTE * 10),
    ('st_value', c_int),
    ('tag', c_int),
]

x87FPURegister_t = struct_anon_554 # E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 606

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 618
class struct_anon_555(Structure):
    pass

struct_anon_555.__slots__ = [
    'ControlWord',
    'StatusWord',
    'TagWord',
    'ErrorOffset',
    'ErrorSelector',
    'DataOffset',
    'DataSelector',
    'Cr0NpxState',
]
struct_anon_555._fields_ = [
    ('ControlWord', WORD),
    ('StatusWord', WORD),
    ('TagWord', WORD),
    ('ErrorOffset', DWORD),
    ('ErrorSelector', DWORD),
    ('DataOffset', DWORD),
    ('DataSelector', DWORD),
    ('Cr0NpxState', DWORD),
]

x87FPU_t = struct_anon_555 # E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 618

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 707
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'AddNewSectionEx'):
        continue
    AddNewSectionEx = _lib.AddNewSectionEx
    AddNewSectionEx.argtypes = [String, String, DWORD, DWORD, LPVOID, DWORD]
    AddNewSectionEx.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 708
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'AddNewSectionExW'):
        continue
    AddNewSectionExW = _lib.AddNewSectionExW
    AddNewSectionExW.argtypes = [POINTER(c_wchar), String, DWORD, DWORD, LPVOID, DWORD]
    AddNewSectionExW.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 709
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'AddNewSection'):
        continue
    AddNewSection = _lib.AddNewSection
    AddNewSection.argtypes = [String, String, DWORD]
    AddNewSection.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 710
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'AddNewSectionW'):
        continue
    AddNewSectionW = _lib.AddNewSectionW
    AddNewSectionW.argtypes = [POINTER(c_wchar), String, DWORD]
    AddNewSectionW.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 713
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'SetSharedOverlay'):
        continue
    SetSharedOverlay = _lib.SetSharedOverlay
    SetSharedOverlay.argtypes = [String]
    SetSharedOverlay.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 714
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'SetSharedOverlayW'):
        continue
    SetSharedOverlayW = _lib.SetSharedOverlayW
    SetSharedOverlayW.argtypes = [POINTER(c_wchar)]
    SetSharedOverlayW.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 715
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetSharedOverlay'):
        continue
    GetSharedOverlay = _lib.GetSharedOverlay
    GetSharedOverlay.argtypes = []
    if sizeof(c_int) == sizeof(c_void_p):
        GetSharedOverlay.restype = ReturnString
    else:
        GetSharedOverlay.restype = String
        GetSharedOverlay.errcheck = ReturnString
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 716
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetSharedOverlayW'):
        continue
    GetSharedOverlayW = _lib.GetSharedOverlayW
    GetSharedOverlayW.argtypes = []
    GetSharedOverlayW.restype = POINTER(c_wchar)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 721
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetPE32DataFromMappedFile'):
        continue
    GetPE32DataFromMappedFile = _lib.GetPE32DataFromMappedFile
    GetPE32DataFromMappedFile.argtypes = [ULONG_PTR, DWORD, DWORD]
    GetPE32DataFromMappedFile.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 722
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetPE32Data'):
        continue
    GetPE32Data = _lib.GetPE32Data
    GetPE32Data.argtypes = [String, DWORD, DWORD]
    GetPE32Data.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 723
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetPE32DataW'):
        continue
    GetPE32DataW = _lib.GetPE32DataW
    GetPE32DataW.argtypes = [POINTER(c_wchar), DWORD, DWORD]
    GetPE32DataW.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 733
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetPE32SectionNumberFromVA'):
        continue
    GetPE32SectionNumberFromVA = _lib.GetPE32SectionNumberFromVA
    GetPE32SectionNumberFromVA.argtypes = [ULONG_PTR, ULONG_PTR]
    GetPE32SectionNumberFromVA.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 743
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'RealignPE'):
        continue
    RealignPE = _lib.RealignPE
    RealignPE.argtypes = [ULONG_PTR, DWORD, DWORD]
    RealignPE.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 744
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'RealignPEEx'):
        continue
    RealignPEEx = _lib.RealignPEEx
    RealignPEEx.argtypes = [String, DWORD, DWORD]
    RealignPEEx.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 745
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'RealignPEExW'):
        continue
    RealignPEExW = _lib.RealignPEExW
    RealignPEExW.argtypes = [POINTER(c_wchar), DWORD, DWORD]
    RealignPEExW.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 755
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetPEBLocation'):
        continue
    GetPEBLocation = _lib.GetPEBLocation
    GetPEBLocation.argtypes = [HANDLE]
    GetPEBLocation.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 756
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetPEBLocation64'):
        continue
    GetPEBLocation64 = _lib.GetPEBLocation64
    GetPEBLocation64.argtypes = [HANDLE]
    GetPEBLocation64.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 757
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetTEBLocation'):
        continue
    GetTEBLocation = _lib.GetTEBLocation
    GetTEBLocation.argtypes = [HANDLE]
    GetTEBLocation.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 758
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetTEBLocation64'):
        continue
    GetTEBLocation64 = _lib.GetTEBLocation64
    GetTEBLocation64.argtypes = [HANDLE]
    GetTEBLocation64.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 762
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'RelocaterCleanup'):
        continue
    RelocaterCleanup = _lib.RelocaterCleanup
    RelocaterCleanup.argtypes = []
    RelocaterCleanup.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 763
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'RelocaterInit'):
        continue
    RelocaterInit = _lib.RelocaterInit
    RelocaterInit.argtypes = [DWORD, ULONG_PTR, ULONG_PTR]
    RelocaterInit.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 764
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'RelocaterAddNewRelocation'):
        continue
    RelocaterAddNewRelocation = _lib.RelocaterAddNewRelocation
    RelocaterAddNewRelocation.argtypes = [HANDLE, ULONG_PTR, DWORD]
    RelocaterAddNewRelocation.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 765
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'RelocaterEstimatedSize'):
        continue
    RelocaterEstimatedSize = _lib.RelocaterEstimatedSize
    RelocaterEstimatedSize.argtypes = []
    RelocaterEstimatedSize.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 781
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ResourcerLoadFileForResourceUse'):
        continue
    ResourcerLoadFileForResourceUse = _lib.ResourcerLoadFileForResourceUse
    ResourcerLoadFileForResourceUse.argtypes = [String]
    ResourcerLoadFileForResourceUse.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 782
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ResourcerLoadFileForResourceUseW'):
        continue
    ResourcerLoadFileForResourceUseW = _lib.ResourcerLoadFileForResourceUseW
    ResourcerLoadFileForResourceUseW.argtypes = [POINTER(c_wchar)]
    ResourcerLoadFileForResourceUseW.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 790
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ResourcerEnumerateResource'):
        continue
    ResourcerEnumerateResource = _lib.ResourcerEnumerateResource
    ResourcerEnumerateResource.argtypes = [String, POINTER(None)]
    ResourcerEnumerateResource.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 791
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ResourcerEnumerateResourceW'):
        continue
    ResourcerEnumerateResourceW = _lib.ResourcerEnumerateResourceW
    ResourcerEnumerateResourceW.argtypes = [POINTER(c_wchar), POINTER(None)]
    ResourcerEnumerateResourceW.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 792
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ResourcerEnumerateResourceEx'):
        continue
    ResourcerEnumerateResourceEx = _lib.ResourcerEnumerateResourceEx
    ResourcerEnumerateResourceEx.argtypes = [ULONG_PTR, DWORD, POINTER(None)]
    ResourcerEnumerateResourceEx.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 795
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ThreaderGetThreadInfo'):
        continue
    ThreaderGetThreadInfo = _lib.ThreaderGetThreadInfo
    ThreaderGetThreadInfo.argtypes = [HANDLE, DWORD]
    ThreaderGetThreadInfo.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 796
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ThreaderEnumThreadInfo'):
        continue
    ThreaderEnumThreadInfo = _lib.ThreaderEnumThreadInfo
    ThreaderEnumThreadInfo.argtypes = [POINTER(None)]
    ThreaderEnumThreadInfo.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 808
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ThreaderSetCallBackForNextExitThreadEvent'):
        continue
    ThreaderSetCallBackForNextExitThreadEvent = _lib.ThreaderSetCallBackForNextExitThreadEvent
    ThreaderSetCallBackForNextExitThreadEvent.argtypes = [LPVOID]
    ThreaderSetCallBackForNextExitThreadEvent.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 813
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ThreaderGetOpenHandleForThread'):
        continue
    ThreaderGetOpenHandleForThread = _lib.ThreaderGetOpenHandleForThread
    ThreaderGetOpenHandleForThread.argtypes = [DWORD]
    ThreaderGetOpenHandleForThread.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 816
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'StaticDisassembleEx'):
        continue
    StaticDisassembleEx = _lib.StaticDisassembleEx
    StaticDisassembleEx.argtypes = [ULONG_PTR, LPVOID]
    StaticDisassembleEx.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 817
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'StaticDisassemble'):
        continue
    StaticDisassemble = _lib.StaticDisassemble
    StaticDisassemble.argtypes = [LPVOID]
    StaticDisassemble.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 819
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'Disassemble'):
        continue
    Disassemble = _lib.Disassemble
    Disassemble.argtypes = [LPVOID]
    Disassemble.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 820
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'StaticLengthDisassemble'):
        continue
    StaticLengthDisassemble = _lib.StaticLengthDisassemble
    StaticLengthDisassemble.argtypes = [LPVOID]
    StaticLengthDisassemble.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 821
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'LengthDisassembleEx'):
        continue
    LengthDisassembleEx = _lib.LengthDisassembleEx
    LengthDisassembleEx.argtypes = [HANDLE, LPVOID]
    LengthDisassembleEx.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 822
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'LengthDisassemble'):
        continue
    LengthDisassemble = _lib.LengthDisassemble
    LengthDisassemble.argtypes = [LPVOID]
    LengthDisassemble.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 823
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'InitDebug'):
        continue
    InitDebug = _lib.InitDebug
    InitDebug.argtypes = [String, String, String]
    InitDebug.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 824
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'InitDebugW'):
        continue
    InitDebugW = _lib.InitDebugW
    InitDebugW.argtypes = [POINTER(c_wchar), POINTER(c_wchar), POINTER(c_wchar)]
    InitDebugW.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 825
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'InitDebugEx'):
        continue
    InitDebugEx = _lib.InitDebugEx
    InitDebugEx.argtypes = [String, String, String, LPVOID]
    InitDebugEx.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 826
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'InitDebugExW'):
        continue
    InitDebugExW = _lib.InitDebugExW
    InitDebugExW.argtypes = [POINTER(c_wchar), POINTER(c_wchar), POINTER(c_wchar), LPVOID]
    InitDebugExW.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 830
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'SetBPXOptions'):
        continue
    SetBPXOptions = _lib.SetBPXOptions
    SetBPXOptions.argtypes = [c_long]
    SetBPXOptions.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 848
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetContextDataEx'):
        continue
    GetContextDataEx = _lib.GetContextDataEx
    GetContextDataEx.argtypes = [HANDLE, DWORD]
    GetContextDataEx.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 849
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetContextData'):
        continue
    GetContextData = _lib.GetContextData
    GetContextData.argtypes = [DWORD]
    GetContextData.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 855
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ClearExceptionNumber'):
        continue
    ClearExceptionNumber = _lib.ClearExceptionNumber
    ClearExceptionNumber.argtypes = []
    ClearExceptionNumber.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 856
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'CurrentExceptionNumber'):
        continue
    CurrentExceptionNumber = _lib.CurrentExceptionNumber
    CurrentExceptionNumber.argtypes = []
    CurrentExceptionNumber.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 859
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'FindEx'):
        continue
    FindEx = _lib.FindEx
    FindEx.argtypes = [HANDLE, LPVOID, DWORD, LPVOID, DWORD, LPBYTE]
    FindEx.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 860
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'Find'):
        continue
    Find = _lib.Find
    Find.argtypes = [LPVOID, DWORD, LPVOID, DWORD, LPBYTE]
    Find.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 867
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetDebugData'):
        continue
    GetDebugData = _lib.GetDebugData
    GetDebugData.argtypes = []
    GetDebugData.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 868
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetTerminationData'):
        continue
    GetTerminationData = _lib.GetTerminationData
    GetTerminationData.argtypes = []
    GetTerminationData.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 869
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetExitCode'):
        continue
    GetExitCode = _lib.GetExitCode
    GetExitCode.argtypes = []
    GetExitCode.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 870
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetDebuggedDLLBaseAddress'):
        continue
    GetDebuggedDLLBaseAddress = _lib.GetDebuggedDLLBaseAddress
    GetDebuggedDLLBaseAddress.argtypes = []
    GetDebuggedDLLBaseAddress.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 871
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetDebuggedFileBaseAddress'):
        continue
    GetDebuggedFileBaseAddress = _lib.GetDebuggedFileBaseAddress
    GetDebuggedFileBaseAddress.argtypes = []
    GetDebuggedFileBaseAddress.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 873
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetFunctionParameter'):
        continue
    GetFunctionParameter = _lib.GetFunctionParameter
    GetFunctionParameter.argtypes = [HANDLE, DWORD, DWORD, DWORD]
    GetFunctionParameter.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 875
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetJumpDestination'):
        continue
    GetJumpDestination = _lib.GetJumpDestination
    GetJumpDestination.argtypes = [HANDLE, ULONG_PTR]
    GetJumpDestination.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 878
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'SetCustomHandler'):
        continue
    SetCustomHandler = _lib.SetCustomHandler
    SetCustomHandler.argtypes = [DWORD, LPVOID]
    SetCustomHandler.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 879
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ForceClose'):
        continue
    ForceClose = _lib.ForceClose
    ForceClose.argtypes = []
    ForceClose.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 880
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'StepInto'):
        continue
    StepInto = _lib.StepInto
    StepInto.argtypes = [LPVOID]
    StepInto.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 881
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'StepOver'):
        continue
    StepOver = _lib.StepOver
    StepOver.argtypes = [LPVOID]
    StepOver.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 883
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'SingleStep'):
        continue
    SingleStep = _lib.SingleStep
    SingleStep.argtypes = [DWORD, LPVOID]
    SingleStep.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 889
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'TitanGetProcessInformation'):
        continue
    TitanGetProcessInformation = _lib.TitanGetProcessInformation
    TitanGetProcessInformation.argtypes = []
    TitanGetProcessInformation.restype = POINTER(PROCESS_INFORMATION)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 890
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'TitanGetStartupInformation'):
        continue
    TitanGetStartupInformation = _lib.TitanGetStartupInformation
    TitanGetStartupInformation.argtypes = []
    TitanGetStartupInformation.restype = POINTER(STARTUPINFOW)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 891
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DebugLoop'):
        continue
    DebugLoop = _lib.DebugLoop
    DebugLoop.argtypes = []
    DebugLoop.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 892
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'SetDebugLoopTimeOut'):
        continue
    SetDebugLoopTimeOut = _lib.SetDebugLoopTimeOut
    SetDebugLoopTimeOut.argtypes = [DWORD]
    SetDebugLoopTimeOut.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 893
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'SetNextDbgContinueStatus'):
        continue
    SetNextDbgContinueStatus = _lib.SetNextDbgContinueStatus
    SetNextDbgContinueStatus.argtypes = [DWORD]
    SetNextDbgContinueStatus.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 897
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DebugLoopEx'):
        continue
    DebugLoopEx = _lib.DebugLoopEx
    DebugLoopEx.argtypes = [DWORD]
    DebugLoopEx.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 903
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'FindOEPInit'):
        continue
    FindOEPInit = _lib.FindOEPInit
    FindOEPInit.argtypes = []
    FindOEPInit.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 907
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterAddNewDll'):
        continue
    ImporterAddNewDll = _lib.ImporterAddNewDll
    ImporterAddNewDll.argtypes = [String, ULONG_PTR]
    ImporterAddNewDll.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 908
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterAddNewAPI'):
        continue
    ImporterAddNewAPI = _lib.ImporterAddNewAPI
    ImporterAddNewAPI.argtypes = [String, ULONG_PTR]
    ImporterAddNewAPI.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 909
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterAddNewOrdinalAPI'):
        continue
    ImporterAddNewOrdinalAPI = _lib.ImporterAddNewOrdinalAPI
    ImporterAddNewOrdinalAPI.argtypes = [ULONG_PTR, ULONG_PTR]
    ImporterAddNewOrdinalAPI.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 910
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetAddedDllCount'):
        continue
    ImporterGetAddedDllCount = _lib.ImporterGetAddedDllCount
    ImporterGetAddedDllCount.argtypes = []
    ImporterGetAddedDllCount.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 911
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetAddedAPICount'):
        continue
    ImporterGetAddedAPICount = _lib.ImporterGetAddedAPICount
    ImporterGetAddedAPICount.argtypes = []
    ImporterGetAddedAPICount.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 913
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterEstimatedSize'):
        continue
    ImporterEstimatedSize = _lib.ImporterEstimatedSize
    ImporterEstimatedSize.argtypes = []
    ImporterEstimatedSize.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 916
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterFindAPIWriteLocation'):
        continue
    ImporterFindAPIWriteLocation = _lib.ImporterFindAPIWriteLocation
    ImporterFindAPIWriteLocation.argtypes = [String]
    ImporterFindAPIWriteLocation.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 917
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterFindOrdinalAPIWriteLocation'):
        continue
    ImporterFindOrdinalAPIWriteLocation = _lib.ImporterFindOrdinalAPIWriteLocation
    ImporterFindOrdinalAPIWriteLocation.argtypes = [ULONG_PTR]
    ImporterFindOrdinalAPIWriteLocation.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 918
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterFindAPIByWriteLocation'):
        continue
    ImporterFindAPIByWriteLocation = _lib.ImporterFindAPIByWriteLocation
    ImporterFindAPIByWriteLocation.argtypes = [ULONG_PTR]
    ImporterFindAPIByWriteLocation.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 919
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterFindDLLByWriteLocation'):
        continue
    ImporterFindDLLByWriteLocation = _lib.ImporterFindDLLByWriteLocation
    ImporterFindDLLByWriteLocation.argtypes = [ULONG_PTR]
    ImporterFindDLLByWriteLocation.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 920
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetDLLName'):
        continue
    ImporterGetDLLName = _lib.ImporterGetDLLName
    ImporterGetDLLName.argtypes = [ULONG_PTR]
    ImporterGetDLLName.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 921
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetDLLNameW'):
        continue
    ImporterGetDLLNameW = _lib.ImporterGetDLLNameW
    ImporterGetDLLNameW.argtypes = [ULONG_PTR]
    ImporterGetDLLNameW.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 922
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetAPIName'):
        continue
    ImporterGetAPIName = _lib.ImporterGetAPIName
    ImporterGetAPIName.argtypes = [ULONG_PTR]
    ImporterGetAPIName.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 923
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetAPIOrdinalNumber'):
        continue
    ImporterGetAPIOrdinalNumber = _lib.ImporterGetAPIOrdinalNumber
    ImporterGetAPIOrdinalNumber.argtypes = [ULONG_PTR]
    ImporterGetAPIOrdinalNumber.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 924
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetAPINameEx'):
        continue
    ImporterGetAPINameEx = _lib.ImporterGetAPINameEx
    ImporterGetAPINameEx.argtypes = [ULONG_PTR, ULONG_PTR]
    ImporterGetAPINameEx.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 925
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetRemoteAPIAddress'):
        continue
    ImporterGetRemoteAPIAddress = _lib.ImporterGetRemoteAPIAddress
    ImporterGetRemoteAPIAddress.argtypes = [HANDLE, ULONG_PTR]
    ImporterGetRemoteAPIAddress.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 926
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetRemoteAPIAddressEx'):
        continue
    ImporterGetRemoteAPIAddressEx = _lib.ImporterGetRemoteAPIAddressEx
    ImporterGetRemoteAPIAddressEx.argtypes = [String, String]
    ImporterGetRemoteAPIAddressEx.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 927
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetLocalAPIAddress'):
        continue
    ImporterGetLocalAPIAddress = _lib.ImporterGetLocalAPIAddress
    ImporterGetLocalAPIAddress.argtypes = [HANDLE, ULONG_PTR]
    ImporterGetLocalAPIAddress.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 928
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetDLLNameFromDebugee'):
        continue
    ImporterGetDLLNameFromDebugee = _lib.ImporterGetDLLNameFromDebugee
    ImporterGetDLLNameFromDebugee.argtypes = [HANDLE, ULONG_PTR]
    ImporterGetDLLNameFromDebugee.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 929
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetDLLNameFromDebugeeW'):
        continue
    ImporterGetDLLNameFromDebugeeW = _lib.ImporterGetDLLNameFromDebugeeW
    ImporterGetDLLNameFromDebugeeW.argtypes = [HANDLE, ULONG_PTR]
    ImporterGetDLLNameFromDebugeeW.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 930
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetAPINameFromDebugee'):
        continue
    ImporterGetAPINameFromDebugee = _lib.ImporterGetAPINameFromDebugee
    ImporterGetAPINameFromDebugee.argtypes = [HANDLE, ULONG_PTR]
    ImporterGetAPINameFromDebugee.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 931
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetAPIOrdinalNumberFromDebugee'):
        continue
    ImporterGetAPIOrdinalNumberFromDebugee = _lib.ImporterGetAPIOrdinalNumberFromDebugee
    ImporterGetAPIOrdinalNumberFromDebugee.argtypes = [HANDLE, ULONG_PTR]
    ImporterGetAPIOrdinalNumberFromDebugee.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 932
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetDLLIndexEx'):
        continue
    ImporterGetDLLIndexEx = _lib.ImporterGetDLLIndexEx
    ImporterGetDLLIndexEx.argtypes = [ULONG_PTR, ULONG_PTR]
    ImporterGetDLLIndexEx.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 933
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetDLLIndex'):
        continue
    ImporterGetDLLIndex = _lib.ImporterGetDLLIndex
    ImporterGetDLLIndex.argtypes = [HANDLE, ULONG_PTR, ULONG_PTR]
    ImporterGetDLLIndex.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 934
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetRemoteDLLBase'):
        continue
    ImporterGetRemoteDLLBase = _lib.ImporterGetRemoteDLLBase
    ImporterGetRemoteDLLBase.argtypes = [HANDLE, HMODULE]
    ImporterGetRemoteDLLBase.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 935
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetRemoteDLLBaseEx'):
        continue
    ImporterGetRemoteDLLBaseEx = _lib.ImporterGetRemoteDLLBaseEx
    ImporterGetRemoteDLLBaseEx.argtypes = [HANDLE, String]
    ImporterGetRemoteDLLBaseEx.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 936
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetRemoteDLLBaseExW'):
        continue
    ImporterGetRemoteDLLBaseExW = _lib.ImporterGetRemoteDLLBaseExW
    ImporterGetRemoteDLLBaseExW.argtypes = [HANDLE, POINTER(c_wchar)]
    ImporterGetRemoteDLLBaseExW.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 938
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetForwardedAPIName'):
        continue
    ImporterGetForwardedAPIName = _lib.ImporterGetForwardedAPIName
    ImporterGetForwardedAPIName.argtypes = [HANDLE, ULONG_PTR]
    ImporterGetForwardedAPIName.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 939
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetForwardedDLLName'):
        continue
    ImporterGetForwardedDLLName = _lib.ImporterGetForwardedDLLName
    ImporterGetForwardedDLLName.argtypes = [HANDLE, ULONG_PTR]
    ImporterGetForwardedDLLName.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 940
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetForwardedDLLIndex'):
        continue
    ImporterGetForwardedDLLIndex = _lib.ImporterGetForwardedDLLIndex
    ImporterGetForwardedDLLIndex.argtypes = [HANDLE, ULONG_PTR, ULONG_PTR]
    ImporterGetForwardedDLLIndex.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 941
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetForwardedAPIOrdinalNumber'):
        continue
    ImporterGetForwardedAPIOrdinalNumber = _lib.ImporterGetForwardedAPIOrdinalNumber
    ImporterGetForwardedAPIOrdinalNumber.argtypes = [HANDLE, ULONG_PTR]
    ImporterGetForwardedAPIOrdinalNumber.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 942
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetNearestAPIAddress'):
        continue
    ImporterGetNearestAPIAddress = _lib.ImporterGetNearestAPIAddress
    ImporterGetNearestAPIAddress.argtypes = [HANDLE, ULONG_PTR]
    ImporterGetNearestAPIAddress.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 943
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterGetNearestAPIName'):
        continue
    ImporterGetNearestAPIName = _lib.ImporterGetNearestAPIName
    ImporterGetNearestAPIName.argtypes = [HANDLE, ULONG_PTR]
    ImporterGetNearestAPIName.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 950
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterAutoSearchIAT'):
        continue
    ImporterAutoSearchIAT = _lib.ImporterAutoSearchIAT
    ImporterAutoSearchIAT.argtypes = [DWORD, String, ULONG_PTR, LPVOID, LPVOID]
    ImporterAutoSearchIAT.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 951
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterAutoSearchIATW'):
        continue
    ImporterAutoSearchIATW = _lib.ImporterAutoSearchIATW
    ImporterAutoSearchIATW.argtypes = [DWORD, POINTER(c_wchar), ULONG_PTR, LPVOID, LPVOID]
    ImporterAutoSearchIATW.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 952
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterAutoSearchIATEx'):
        continue
    ImporterAutoSearchIATEx = _lib.ImporterAutoSearchIATEx
    ImporterAutoSearchIATEx.argtypes = [DWORD, ULONG_PTR, ULONG_PTR, LPVOID, LPVOID]
    ImporterAutoSearchIATEx.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 953
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterEnumAddedData'):
        continue
    ImporterEnumAddedData = _lib.ImporterEnumAddedData
    ImporterEnumAddedData.argtypes = [LPVOID]
    ImporterEnumAddedData.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 956
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterAutoFixIAT'):
        continue
    ImporterAutoFixIAT = _lib.ImporterAutoFixIAT
    ImporterAutoFixIAT.argtypes = [DWORD, String, ULONG_PTR]
    ImporterAutoFixIAT.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 957
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ImporterAutoFixIATW'):
        continue
    ImporterAutoFixIATW = _lib.ImporterAutoFixIATW
    ImporterAutoFixIATW.argtypes = [DWORD, POINTER(c_wchar), ULONG_PTR]
    ImporterAutoFixIATW.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 963
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'HooksGetTrampolineAddress'):
        continue
    HooksGetTrampolineAddress = _lib.HooksGetTrampolineAddress
    HooksGetTrampolineAddress.argtypes = [LPVOID]
    HooksGetTrampolineAddress.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 964
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'HooksGetHookEntryDetails'):
        continue
    HooksGetHookEntryDetails = _lib.HooksGetHookEntryDetails
    HooksGetHookEntryDetails.argtypes = [LPVOID]
    HooksGetHookEntryDetails.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 977
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'HooksScanModuleMemory'):
        continue
    HooksScanModuleMemory = _lib.HooksScanModuleMemory
    HooksScanModuleMemory.argtypes = [HMODULE, LPVOID]
    HooksScanModuleMemory.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 978
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'HooksScanEntireProcessMemory'):
        continue
    HooksScanEntireProcessMemory = _lib.HooksScanEntireProcessMemory
    HooksScanEntireProcessMemory.argtypes = [LPVOID]
    HooksScanEntireProcessMemory.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 979
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'HooksScanEntireProcessMemoryEx'):
        continue
    HooksScanEntireProcessMemoryEx = _lib.HooksScanEntireProcessMemoryEx
    HooksScanEntireProcessMemoryEx.argtypes = []
    HooksScanEntireProcessMemoryEx.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 981
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'TracerInit'):
        continue
    TracerInit = _lib.TracerInit
    TracerInit.argtypes = []
    TracerInit.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 982
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'TracerLevel1'):
        continue
    TracerLevel1 = _lib.TracerLevel1
    TracerLevel1.argtypes = [HANDLE, ULONG_PTR]
    TracerLevel1.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 983
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'HashTracerLevel1'):
        continue
    HashTracerLevel1 = _lib.HashTracerLevel1
    HashTracerLevel1.argtypes = [HANDLE, ULONG_PTR, DWORD]
    HashTracerLevel1.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 984
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'TracerDetectRedirection'):
        continue
    TracerDetectRedirection = _lib.TracerDetectRedirection
    TracerDetectRedirection.argtypes = [HANDLE, ULONG_PTR]
    TracerDetectRedirection.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 985
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'TracerFixKnownRedirection'):
        continue
    TracerFixKnownRedirection = _lib.TracerFixKnownRedirection
    TracerFixKnownRedirection.argtypes = [HANDLE, ULONG_PTR, DWORD]
    TracerFixKnownRedirection.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 986
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'TracerFixRedirectionViaModule'):
        continue
    TracerFixRedirectionViaModule = _lib.TracerFixRedirectionViaModule
    TracerFixRedirectionViaModule.argtypes = [HMODULE, HANDLE, ULONG_PTR, DWORD]
    TracerFixRedirectionViaModule.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 987
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'TracerFixRedirectionViaImpRecPlugin'):
        continue
    TracerFixRedirectionViaImpRecPlugin = _lib.TracerFixRedirectionViaImpRecPlugin
    TracerFixRedirectionViaImpRecPlugin.argtypes = [HANDLE, String, ULONG_PTR]
    TracerFixRedirectionViaImpRecPlugin.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 989
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ExporterCleanup'):
        continue
    ExporterCleanup = _lib.ExporterCleanup
    ExporterCleanup.argtypes = []
    ExporterCleanup.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 990
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ExporterSetImageBase'):
        continue
    ExporterSetImageBase = _lib.ExporterSetImageBase
    ExporterSetImageBase.argtypes = [ULONG_PTR]
    ExporterSetImageBase.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 991
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ExporterInit'):
        continue
    ExporterInit = _lib.ExporterInit
    ExporterInit.argtypes = [DWORD, ULONG_PTR, DWORD, String]
    ExporterInit.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 994
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ExporterGetAddedExportCount'):
        continue
    ExporterGetAddedExportCount = _lib.ExporterGetAddedExportCount
    ExporterGetAddedExportCount.argtypes = []
    ExporterGetAddedExportCount.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 995
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ExporterEstimatedSize'):
        continue
    ExporterEstimatedSize = _lib.ExporterEstimatedSize
    ExporterEstimatedSize.argtypes = []
    ExporterEstimatedSize.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1004
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'LibrarianGetLibraryInfo'):
        continue
    LibrarianGetLibraryInfo = _lib.LibrarianGetLibraryInfo
    LibrarianGetLibraryInfo.argtypes = [String]
    LibrarianGetLibraryInfo.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1005
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'LibrarianGetLibraryInfoW'):
        continue
    LibrarianGetLibraryInfoW = _lib.LibrarianGetLibraryInfoW
    LibrarianGetLibraryInfoW.argtypes = [POINTER(c_wchar)]
    LibrarianGetLibraryInfoW.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1006
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'LibrarianGetLibraryInfoEx'):
        continue
    LibrarianGetLibraryInfoEx = _lib.LibrarianGetLibraryInfoEx
    LibrarianGetLibraryInfoEx.argtypes = [POINTER(None)]
    LibrarianGetLibraryInfoEx.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1007
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'LibrarianGetLibraryInfoExW'):
        continue
    LibrarianGetLibraryInfoExW = _lib.LibrarianGetLibraryInfoExW
    LibrarianGetLibraryInfoExW.argtypes = [POINTER(None)]
    LibrarianGetLibraryInfoExW.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1008
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'LibrarianEnumLibraryInfo'):
        continue
    LibrarianEnumLibraryInfo = _lib.LibrarianEnumLibraryInfo
    LibrarianEnumLibraryInfo.argtypes = [POINTER(None)]
    LibrarianEnumLibraryInfo.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1009
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'LibrarianEnumLibraryInfoW'):
        continue
    LibrarianEnumLibraryInfoW = _lib.LibrarianEnumLibraryInfoW
    LibrarianEnumLibraryInfoW.argtypes = [POINTER(None)]
    LibrarianEnumLibraryInfoW.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1011
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetActiveProcessId'):
        continue
    GetActiveProcessId = _lib.GetActiveProcessId
    GetActiveProcessId.argtypes = [String]
    GetActiveProcessId.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1012
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GetActiveProcessIdW'):
        continue
    GetActiveProcessIdW = _lib.GetActiveProcessIdW
    GetActiveProcessIdW.argtypes = [POINTER(c_wchar)]
    GetActiveProcessIdW.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1013
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'EnumProcessesWithLibrary'):
        continue
    EnumProcessesWithLibrary = _lib.EnumProcessesWithLibrary
    EnumProcessesWithLibrary.argtypes = [String, POINTER(None)]
    EnumProcessesWithLibrary.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1032
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'TranslateNativeName'):
        continue
    TranslateNativeName = _lib.TranslateNativeName
    TranslateNativeName.argtypes = [String]
    TranslateNativeName.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1033
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'TranslateNativeNameW'):
        continue
    TranslateNativeNameW = _lib.TranslateNativeNameW
    TranslateNativeNameW.argtypes = [POINTER(c_wchar)]
    TranslateNativeNameW.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1035
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'HandlerGetActiveHandleCount'):
        continue
    HandlerGetActiveHandleCount = _lib.HandlerGetActiveHandleCount
    HandlerGetActiveHandleCount.argtypes = [DWORD]
    HandlerGetActiveHandleCount.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1039
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'HandlerEnumerateOpenHandles'):
        continue
    HandlerEnumerateOpenHandles = _lib.HandlerEnumerateOpenHandles
    HandlerEnumerateOpenHandles.argtypes = [DWORD, LPVOID, DWORD]
    HandlerEnumerateOpenHandles.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1040
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'HandlerGetHandleDetails'):
        continue
    HandlerGetHandleDetails = _lib.HandlerGetHandleDetails
    HandlerGetHandleDetails.argtypes = [HANDLE, DWORD, HANDLE, DWORD]
    HandlerGetHandleDetails.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1049
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'HandlerEnumerateOpenMutexes'):
        continue
    HandlerEnumerateOpenMutexes = _lib.HandlerEnumerateOpenMutexes
    HandlerEnumerateOpenMutexes.argtypes = [HANDLE, DWORD, LPVOID, DWORD]
    HandlerEnumerateOpenMutexes.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1050
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'HandlerGetOpenMutexHandle'):
        continue
    HandlerGetOpenMutexHandle = _lib.HandlerGetOpenMutexHandle
    HandlerGetOpenMutexHandle.argtypes = [HANDLE, DWORD, String]
    HandlerGetOpenMutexHandle.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1051
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'HandlerGetOpenMutexHandleW'):
        continue
    HandlerGetOpenMutexHandleW = _lib.HandlerGetOpenMutexHandleW
    HandlerGetOpenMutexHandleW.argtypes = [HANDLE, DWORD, POINTER(c_wchar)]
    HandlerGetOpenMutexHandleW.restype = ULONG_PTR
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1052
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'HandlerGetProcessIdWhichCreatedMutex'):
        continue
    HandlerGetProcessIdWhichCreatedMutex = _lib.HandlerGetProcessIdWhichCreatedMutex
    HandlerGetProcessIdWhichCreatedMutex.argtypes = [String]
    HandlerGetProcessIdWhichCreatedMutex.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1053
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'HandlerGetProcessIdWhichCreatedMutexW'):
        continue
    HandlerGetProcessIdWhichCreatedMutexW = _lib.HandlerGetProcessIdWhichCreatedMutexW
    HandlerGetProcessIdWhichCreatedMutexW.argtypes = [POINTER(c_wchar)]
    HandlerGetProcessIdWhichCreatedMutexW.restype = c_long
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1068
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'StaticFileClose'):
        continue
    StaticFileClose = _lib.StaticFileClose
    StaticFileClose.argtypes = [HANDLE]
    StaticFileClose.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1069
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'StaticMemoryDecrypt'):
        continue
    StaticMemoryDecrypt = _lib.StaticMemoryDecrypt
    StaticMemoryDecrypt.argtypes = [LPVOID, DWORD, DWORD, DWORD, ULONG_PTR]
    StaticMemoryDecrypt.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1070
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'StaticMemoryDecryptEx'):
        continue
    StaticMemoryDecryptEx = _lib.StaticMemoryDecryptEx
    StaticMemoryDecryptEx.argtypes = [LPVOID, DWORD, DWORD, POINTER(None)]
    StaticMemoryDecryptEx.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1071
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'StaticMemoryDecryptSpecial'):
        continue
    StaticMemoryDecryptSpecial = _lib.StaticMemoryDecryptSpecial
    StaticMemoryDecryptSpecial.argtypes = [LPVOID, DWORD, DWORD, DWORD, POINTER(None)]
    StaticMemoryDecryptSpecial.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1087
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'EngineUnpackerSetEntryPointAddress'):
        continue
    EngineUnpackerSetEntryPointAddress = _lib.EngineUnpackerSetEntryPointAddress
    EngineUnpackerSetEntryPointAddress.argtypes = [ULONG_PTR]
    EngineUnpackerSetEntryPointAddress.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1088
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'EngineUnpackerFinalizeUnpacking'):
        continue
    EngineUnpackerFinalizeUnpacking = _lib.EngineUnpackerFinalizeUnpacking
    EngineUnpackerFinalizeUnpacking.argtypes = []
    EngineUnpackerFinalizeUnpacking.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1096
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'EngineAddUnpackerWindowLogMessage'):
        continue
    EngineAddUnpackerWindowLogMessage = _lib.EngineAddUnpackerWindowLogMessage
    EngineAddUnpackerWindowLogMessage.argtypes = [String]
    EngineAddUnpackerWindowLogMessage.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 1107
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'ExtensionManagerGetPluginInfo'):
        continue
    ExtensionManagerGetPluginInfo = _lib.ExtensionManagerGetPluginInfo
    ExtensionManagerGetPluginInfo.argtypes = [String]
    ExtensionManagerGetPluginInfo.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 17
try:
    UE_STRUCT_PE32STRUCT = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 18
try:
    UE_STRUCT_PE64STRUCT = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 19
try:
    UE_STRUCT_PESTRUCT = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 20
try:
    UE_STRUCT_IMPORTENUMDATA = 4
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 21
try:
    UE_STRUCT_THREAD_ITEM_DATA = 5
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 22
try:
    UE_STRUCT_LIBRARY_ITEM_DATA = 6
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 23
try:
    UE_STRUCT_LIBRARY_ITEM_DATAW = 7
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 24
try:
    UE_STRUCT_PROCESS_ITEM_DATA = 8
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 25
try:
    UE_STRUCT_HANDLERARRAY = 9
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 26
try:
    UE_STRUCT_PLUGININFORMATION = 10
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 27
try:
    UE_STRUCT_HOOK_ENTRY = 11
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 28
try:
    UE_STRUCT_FILE_STATUS_INFO = 12
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 29
try:
    UE_STRUCT_FILE_FIX_INFO = 13
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 30
try:
    UE_STRUCT_X87FPUREGISTER = 14
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 31
try:
    UE_STRUCT_X87FPU = 15
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 32
try:
    UE_STRUCT_TITAN_ENGINE_CONTEXT = 16
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 34
try:
    UE_ACCESS_READ = 0
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 35
try:
    UE_ACCESS_WRITE = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 36
try:
    UE_ACCESS_ALL = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 38
try:
    UE_HIDE_PEBONLY = 0
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 39
try:
    UE_HIDE_BASIC = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 41
try:
    UE_PLUGIN_CALL_REASON_PREDEBUG = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 42
try:
    UE_PLUGIN_CALL_REASON_EXCEPTION = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 43
try:
    UE_PLUGIN_CALL_REASON_POSTDEBUG = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 44
try:
    UE_PLUGIN_CALL_REASON_UNHANDLEDEXCEPTION = 4
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 46
try:
    TEE_HOOK_NRM_JUMP = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 47
try:
    TEE_HOOK_NRM_CALL = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 48
try:
    TEE_HOOK_IAT = 5
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 50
try:
    UE_ENGINE_ALOW_MODULE_LOADING = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 51
try:
    UE_ENGINE_AUTOFIX_FORWARDERS = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 52
try:
    UE_ENGINE_PASS_ALL_EXCEPTIONS = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 53
try:
    UE_ENGINE_NO_CONSOLE_WINDOW = 4
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 54
try:
    UE_ENGINE_BACKUP_FOR_CRITICAL_FUNCTIONS = 5
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 55
try:
    UE_ENGINE_CALL_PLUGIN_CALLBACK = 6
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 56
try:
    UE_ENGINE_RESET_CUSTOM_HANDLER = 7
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 57
try:
    UE_ENGINE_CALL_PLUGIN_DEBUG_CALLBACK = 8
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 58
try:
    UE_ENGINE_SET_DEBUG_PRIVILEGE = 9
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 60
try:
    UE_OPTION_REMOVEALL = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 61
try:
    UE_OPTION_DISABLEALL = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 62
try:
    UE_OPTION_REMOVEALLDISABLED = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 63
try:
    UE_OPTION_REMOVEALLENABLED = 4
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 65
try:
    UE_STATIC_DECRYPTOR_XOR = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 66
try:
    UE_STATIC_DECRYPTOR_SUB = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 67
try:
    UE_STATIC_DECRYPTOR_ADD = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 69
try:
    UE_STATIC_DECRYPTOR_FOREWARD = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 70
try:
    UE_STATIC_DECRYPTOR_BACKWARD = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 72
try:
    UE_STATIC_KEY_SIZE_1 = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 73
try:
    UE_STATIC_KEY_SIZE_2 = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 74
try:
    UE_STATIC_KEY_SIZE_4 = 4
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 75
try:
    UE_STATIC_KEY_SIZE_8 = 8
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 77
try:
    UE_STATIC_APLIB = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 78
try:
    UE_STATIC_APLIB_DEPACK = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 79
try:
    UE_STATIC_LZMA = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 81
try:
    UE_STATIC_HASH_MD5 = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 82
try:
    UE_STATIC_HASH_SHA1 = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 83
try:
    UE_STATIC_HASH_CRC32 = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 85
try:
    UE_RESOURCE_LANGUAGE_ANY = (-1)
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 87
try:
    UE_PE_OFFSET = 0
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 88
try:
    UE_IMAGEBASE = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 89
try:
    UE_OEP = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 90
try:
    UE_SIZEOFIMAGE = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 91
try:
    UE_SIZEOFHEADERS = 4
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 92
try:
    UE_SIZEOFOPTIONALHEADER = 5
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 93
try:
    UE_SECTIONALIGNMENT = 6
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 94
try:
    UE_IMPORTTABLEADDRESS = 7
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 95
try:
    UE_IMPORTTABLESIZE = 8
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 96
try:
    UE_RESOURCETABLEADDRESS = 9
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 97
try:
    UE_RESOURCETABLESIZE = 10
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 98
try:
    UE_EXPORTTABLEADDRESS = 11
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 99
try:
    UE_EXPORTTABLESIZE = 12
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 100
try:
    UE_TLSTABLEADDRESS = 13
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 101
try:
    UE_TLSTABLESIZE = 14
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 102
try:
    UE_RELOCATIONTABLEADDRESS = 15
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 103
try:
    UE_RELOCATIONTABLESIZE = 16
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 104
try:
    UE_TIMEDATESTAMP = 17
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 105
try:
    UE_SECTIONNUMBER = 18
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 106
try:
    UE_CHECKSUM = 19
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 107
try:
    UE_SUBSYSTEM = 20
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 108
try:
    UE_CHARACTERISTICS = 21
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 109
try:
    UE_NUMBEROFRVAANDSIZES = 22
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 110
try:
    UE_BASEOFCODE = 23
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 111
try:
    UE_BASEOFDATA = 24
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 113
try:
    UE_SECTIONNAME = 40
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 114
try:
    UE_SECTIONVIRTUALOFFSET = 41
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 115
try:
    UE_SECTIONVIRTUALSIZE = 42
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 116
try:
    UE_SECTIONRAWOFFSET = 43
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 117
try:
    UE_SECTIONRAWSIZE = 44
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 118
try:
    UE_SECTIONFLAGS = 45
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 122
try:
    UE_CH_BREAKPOINT = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 123
try:
    UE_CH_SINGLESTEP = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 124
try:
    UE_CH_ACCESSVIOLATION = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 125
try:
    UE_CH_ILLEGALINSTRUCTION = 4
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 126
try:
    UE_CH_NONCONTINUABLEEXCEPTION = 5
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 127
try:
    UE_CH_ARRAYBOUNDSEXCEPTION = 6
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 128
try:
    UE_CH_FLOATDENORMALOPERAND = 7
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 129
try:
    UE_CH_FLOATDEVIDEBYZERO = 8
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 130
try:
    UE_CH_INTEGERDEVIDEBYZERO = 9
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 131
try:
    UE_CH_INTEGEROVERFLOW = 10
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 132
try:
    UE_CH_PRIVILEGEDINSTRUCTION = 11
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 133
try:
    UE_CH_PAGEGUARD = 12
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 134
try:
    UE_CH_EVERYTHINGELSE = 13
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 135
try:
    UE_CH_CREATETHREAD = 14
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 136
try:
    UE_CH_EXITTHREAD = 15
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 137
try:
    UE_CH_CREATEPROCESS = 16
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 138
try:
    UE_CH_EXITPROCESS = 17
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 139
try:
    UE_CH_LOADDLL = 18
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 140
try:
    UE_CH_UNLOADDLL = 19
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 141
try:
    UE_CH_OUTPUTDEBUGSTRING = 20
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 142
try:
    UE_CH_AFTEREXCEPTIONPROCESSING = 21
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 143
try:
    UE_CH_SYSTEMBREAKPOINT = 23
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 144
try:
    UE_CH_UNHANDLEDEXCEPTION = 24
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 145
try:
    UE_CH_RIPEVENT = 25
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 146
try:
    UE_CH_DEBUGEVENT = 26
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 148
try:
    UE_OPTION_HANDLER_RETURN_HANDLECOUNT = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 149
try:
    UE_OPTION_HANDLER_RETURN_ACCESS = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 150
try:
    UE_OPTION_HANDLER_RETURN_FLAGS = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 151
try:
    UE_OPTION_HANDLER_RETURN_TYPENAME = 4
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 153
try:
    UE_BREAKPOINT_INT3 = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 154
try:
    UE_BREAKPOINT_LONG_INT3 = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 155
try:
    UE_BREAKPOINT_UD2 = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 157
try:
    UE_BPXREMOVED = 0
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 158
try:
    UE_BPXACTIVE = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 159
try:
    UE_BPXINACTIVE = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 161
try:
    UE_BREAKPOINT = 0
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 162
try:
    UE_SINGLESHOOT = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 163
try:
    UE_HARDWARE = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 164
try:
    UE_MEMORY = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 165
try:
    UE_MEMORY_READ = 4
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 166
try:
    UE_MEMORY_WRITE = 5
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 167
try:
    UE_MEMORY_EXECUTE = 6
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 168
try:
    UE_BREAKPOINT_TYPE_INT3 = 268435456
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 169
try:
    UE_BREAKPOINT_TYPE_LONG_INT3 = 536870912
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 170
try:
    UE_BREAKPOINT_TYPE_UD2 = 805306368
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 172
try:
    UE_HARDWARE_EXECUTE = 4
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 173
try:
    UE_HARDWARE_WRITE = 5
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 174
try:
    UE_HARDWARE_READWRITE = 6
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 176
try:
    UE_HARDWARE_SIZE_1 = 7
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 177
try:
    UE_HARDWARE_SIZE_2 = 8
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 178
try:
    UE_HARDWARE_SIZE_4 = 9
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 179
try:
    UE_HARDWARE_SIZE_8 = 10
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 181
try:
    UE_ON_LIB_LOAD = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 182
try:
    UE_ON_LIB_UNLOAD = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 183
try:
    UE_ON_LIB_ALL = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 185
try:
    UE_APISTART = 0
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 186
try:
    UE_APIEND = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 188
try:
    UE_PLATFORM_x86 = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 189
try:
    UE_PLATFORM_x64 = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 190
try:
    UE_PLATFORM_ALL = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 192
try:
    UE_FUNCTION_STDCALL = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 193
try:
    UE_FUNCTION_CCALL = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 194
try:
    UE_FUNCTION_FASTCALL = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 195
try:
    UE_FUNCTION_STDCALL_RET = 4
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 196
try:
    UE_FUNCTION_CCALL_RET = 5
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 197
try:
    UE_FUNCTION_FASTCALL_RET = 6
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 198
try:
    UE_FUNCTION_STDCALL_CALL = 7
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 199
try:
    UE_FUNCTION_CCALL_CALL = 8
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 200
try:
    UE_FUNCTION_FASTCALL_CALL = 9
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 201
try:
    UE_PARAMETER_BYTE = 0
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 202
try:
    UE_PARAMETER_WORD = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 203
try:
    UE_PARAMETER_DWORD = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 204
try:
    UE_PARAMETER_QWORD = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 205
try:
    UE_PARAMETER_PTR_BYTE = 4
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 206
try:
    UE_PARAMETER_PTR_WORD = 5
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 207
try:
    UE_PARAMETER_PTR_DWORD = 6
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 208
try:
    UE_PARAMETER_PTR_QWORD = 7
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 209
try:
    UE_PARAMETER_STRING = 8
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 210
try:
    UE_PARAMETER_UNICODE = 9
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 212
try:
    UE_EAX = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 213
try:
    UE_EBX = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 214
try:
    UE_ECX = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 215
try:
    UE_EDX = 4
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 216
try:
    UE_EDI = 5
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 217
try:
    UE_ESI = 6
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 218
try:
    UE_EBP = 7
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 219
try:
    UE_ESP = 8
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 220
try:
    UE_EIP = 9
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 221
try:
    UE_EFLAGS = 10
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 222
try:
    UE_DR0 = 11
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 223
try:
    UE_DR1 = 12
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 224
try:
    UE_DR2 = 13
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 225
try:
    UE_DR3 = 14
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 226
try:
    UE_DR6 = 15
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 227
try:
    UE_DR7 = 16
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 228
try:
    UE_RAX = 17
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 229
try:
    UE_RBX = 18
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 230
try:
    UE_RCX = 19
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 231
try:
    UE_RDX = 20
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 232
try:
    UE_RDI = 21
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 233
try:
    UE_RSI = 22
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 234
try:
    UE_RBP = 23
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 235
try:
    UE_RSP = 24
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 236
try:
    UE_RIP = 25
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 237
try:
    UE_RFLAGS = 26
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 238
try:
    UE_R8 = 27
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 239
try:
    UE_R9 = 28
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 240
try:
    UE_R10 = 29
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 241
try:
    UE_R11 = 30
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 242
try:
    UE_R12 = 31
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 243
try:
    UE_R13 = 32
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 244
try:
    UE_R14 = 33
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 245
try:
    UE_R15 = 34
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 246
try:
    UE_CIP = 35
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 247
try:
    UE_CSP = 36
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 251
try:
    UE_CFLAGS = UE_EFLAGS
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 253
try:
    UE_SEG_GS = 37
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 254
try:
    UE_SEG_FS = 38
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 255
try:
    UE_SEG_ES = 39
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 256
try:
    UE_SEG_DS = 40
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 257
try:
    UE_SEG_CS = 41
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 258
try:
    UE_SEG_SS = 42
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 259
try:
    UE_x87_r0 = 43
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 260
try:
    UE_x87_r1 = 44
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 261
try:
    UE_x87_r2 = 45
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 262
try:
    UE_x87_r3 = 46
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 263
try:
    UE_x87_r4 = 47
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 264
try:
    UE_x87_r5 = 48
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 265
try:
    UE_x87_r6 = 49
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 266
try:
    UE_x87_r7 = 50
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 267
try:
    UE_X87_STATUSWORD = 51
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 268
try:
    UE_X87_CONTROLWORD = 52
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 269
try:
    UE_X87_TAGWORD = 53
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 270
try:
    UE_MXCSR = 54
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 271
try:
    UE_MMX0 = 55
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 272
try:
    UE_MMX1 = 56
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 273
try:
    UE_MMX2 = 57
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 274
try:
    UE_MMX3 = 58
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 275
try:
    UE_MMX4 = 59
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 276
try:
    UE_MMX5 = 60
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 277
try:
    UE_MMX6 = 61
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 278
try:
    UE_MMX7 = 62
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 279
try:
    UE_XMM0 = 63
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 280
try:
    UE_XMM1 = 64
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 281
try:
    UE_XMM2 = 65
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 282
try:
    UE_XMM3 = 66
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 283
try:
    UE_XMM4 = 67
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 284
try:
    UE_XMM5 = 68
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 285
try:
    UE_XMM6 = 69
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 286
try:
    UE_XMM7 = 70
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 287
try:
    UE_XMM8 = 71
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 288
try:
    UE_XMM9 = 72
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 289
try:
    UE_XMM10 = 73
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 290
try:
    UE_XMM11 = 74
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 291
try:
    UE_XMM12 = 75
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 292
try:
    UE_XMM13 = 76
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 293
try:
    UE_XMM14 = 77
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 294
try:
    UE_XMM15 = 78
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 295
try:
    UE_x87_ST0 = 79
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 296
try:
    UE_x87_ST1 = 80
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 297
try:
    UE_x87_ST2 = 81
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 298
try:
    UE_x87_ST3 = 82
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 299
try:
    UE_x87_ST4 = 83
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 300
try:
    UE_x87_ST5 = 84
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 301
try:
    UE_x87_ST6 = 85
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 302
try:
    UE_x87_ST7 = 86
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 303
try:
    UE_YMM0 = 87
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 304
try:
    UE_YMM1 = 88
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 305
try:
    UE_YMM2 = 89
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 306
try:
    UE_YMM3 = 90
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 307
try:
    UE_YMM4 = 91
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 308
try:
    UE_YMM5 = 92
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 309
try:
    UE_YMM6 = 93
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 310
try:
    UE_YMM7 = 94
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 311
try:
    UE_YMM8 = 95
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 312
try:
    UE_YMM9 = 96
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 313
try:
    UE_YMM10 = 97
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 314
try:
    UE_YMM11 = 98
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 315
try:
    UE_YMM12 = 99
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 316
try:
    UE_YMM13 = 100
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 317
try:
    UE_YMM14 = 101
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 318
try:
    UE_YMM15 = 102
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 467
try:
    TEE_MAXIMUM_HOOK_SIZE = 14
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 468
try:
    TEE_MAXIMUM_HOOK_RELOCS = 7
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 472
try:
    TEE_MAXIMUM_HOOK_INSERT_SIZE = 5
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 493
try:
    UE_DEPTH_SURFACE = 0
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 494
try:
    UE_DEPTH_DEEP = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 496
try:
    UE_UNPACKER_CONDITION_SEARCH_FROM_EP = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 498
try:
    UE_UNPACKER_CONDITION_LOADLIBRARY = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 499
try:
    UE_UNPACKER_CONDITION_GETPROCADDRESS = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 500
try:
    UE_UNPACKER_CONDITION_ENTRYPOINTBREAK = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 501
try:
    UE_UNPACKER_CONDITION_RELOCSNAPSHOT1 = 4
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 502
try:
    UE_UNPACKER_CONDITION_RELOCSNAPSHOT2 = 5
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 504
try:
    UE_FIELD_OK = 0
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 505
try:
    UE_FIELD_BROKEN_NON_FIXABLE = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 506
try:
    UE_FIELD_BROKEN_NON_CRITICAL = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 507
try:
    UE_FIELD_BROKEN_FIXABLE_FOR_STATIC_USE = 3
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 508
try:
    UE_FIELD_BROKEN_BUT_CAN_BE_EMULATED = 4
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 509
try:
    UE_FIELD_FIXABLE_NON_CRITICAL = 5
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 510
try:
    UE_FIELD_FIXABLE_CRITICAL = 6
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 511
try:
    UE_FIELD_NOT_PRESET = 7
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 512
try:
    UE_FIELD_NOT_PRESET_WARNING = 8
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 514
try:
    UE_RESULT_FILE_OK = 10
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 515
try:
    UE_RESULT_FILE_INVALID_BUT_FIXABLE = 11
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 516
try:
    UE_RESULT_FILE_INVALID_AND_NON_FIXABLE = 12
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\TitanEngine\\TitanEngine.h: 517
try:
    UE_RESULT_FILE_INVALID_FORMAT = 13
except:
    pass

# No inserted files

