'''Wrapper for BridgeMain.h

Generated with:
E:\x64dbg_pythonloader\ctypesgen\ctypesgen.py -l x32_bridge.dll -o bin\data\plugins\pybase\x64dbg.py pluginsdk\BridgeMain.h

Do not modify this file.
'''

__docformat__ =  'restructuredtext'

# Begin preamble

import ctypes, os, sys
from ctypes import *

_int_types = (c_int16, c_int32)
if hasattr(ctypes, 'c_int64'):
    # Some builds of ctypes apparently do not have c_int64
    # defined; it's a pretty good bet that these builds do not
    # have 64-bit pointers.
    _int_types += (c_int64,)
for t in _int_types:
    if sizeof(t) == sizeof(c_size_t):
        c_ptrdiff_t = t
del t
del _int_types

class c_void(Structure):
    # c_void_p is a buggy return type, converting to int, so
    # POINTER(None) == c_void_p is actually written as
    # POINTER(c_void), so it can be treated as a real pointer.
    _fields_ = [('dummy', c_int)]

def POINTER(obj):
    p = ctypes.POINTER(obj)

    # Convert None to a real NULL pointer to work around bugs
    # in how ctypes handles None on 64-bit platforms
    if not isinstance(p.from_param, classmethod):
        def from_param(cls, x):
            if x is None:
                return cls()
            else:
                return x
        p.from_param = classmethod(from_param)

    return p

class UserString:
    def __init__(self, seq):
        if isinstance(seq, basestring):
            self.data = seq
        elif isinstance(seq, UserString):
            self.data = seq.data[:]
        else:
            self.data = str(seq)
    def __str__(self): return str(self.data)
    def __repr__(self): return repr(self.data)
    def __int__(self): return int(self.data)
    def __long__(self): return long(self.data)
    def __float__(self): return float(self.data)
    def __complex__(self): return complex(self.data)
    def __hash__(self): return hash(self.data)

    def __cmp__(self, string):
        if isinstance(string, UserString):
            return cmp(self.data, string.data)
        else:
            return cmp(self.data, string)
    def __contains__(self, char):
        return char in self.data

    def __len__(self): return len(self.data)
    def __getitem__(self, index): return self.__class__(self.data[index])
    def __getslice__(self, start, end):
        start = max(start, 0); end = max(end, 0)
        return self.__class__(self.data[start:end])

    def __add__(self, other):
        if isinstance(other, UserString):
            return self.__class__(self.data + other.data)
        elif isinstance(other, basestring):
            return self.__class__(self.data + other)
        else:
            return self.__class__(self.data + str(other))
    def __radd__(self, other):
        if isinstance(other, basestring):
            return self.__class__(other + self.data)
        else:
            return self.__class__(str(other) + self.data)
    def __mul__(self, n):
        return self.__class__(self.data*n)
    __rmul__ = __mul__
    def __mod__(self, args):
        return self.__class__(self.data % args)

    # the following methods are defined in alphabetical order:
    def capitalize(self): return self.__class__(self.data.capitalize())
    def center(self, width, *args):
        return self.__class__(self.data.center(width, *args))
    def count(self, sub, start=0, end=sys.maxint):
        return self.data.count(sub, start, end)
    def decode(self, encoding=None, errors=None): # XXX improve this?
        if encoding:
            if errors:
                return self.__class__(self.data.decode(encoding, errors))
            else:
                return self.__class__(self.data.decode(encoding))
        else:
            return self.__class__(self.data.decode())
    def encode(self, encoding=None, errors=None): # XXX improve this?
        if encoding:
            if errors:
                return self.__class__(self.data.encode(encoding, errors))
            else:
                return self.__class__(self.data.encode(encoding))
        else:
            return self.__class__(self.data.encode())
    def endswith(self, suffix, start=0, end=sys.maxint):
        return self.data.endswith(suffix, start, end)
    def expandtabs(self, tabsize=8):
        return self.__class__(self.data.expandtabs(tabsize))
    def find(self, sub, start=0, end=sys.maxint):
        return self.data.find(sub, start, end)
    def index(self, sub, start=0, end=sys.maxint):
        return self.data.index(sub, start, end)
    def isalpha(self): return self.data.isalpha()
    def isalnum(self): return self.data.isalnum()
    def isdecimal(self): return self.data.isdecimal()
    def isdigit(self): return self.data.isdigit()
    def islower(self): return self.data.islower()
    def isnumeric(self): return self.data.isnumeric()
    def isspace(self): return self.data.isspace()
    def istitle(self): return self.data.istitle()
    def isupper(self): return self.data.isupper()
    def join(self, seq): return self.data.join(seq)
    def ljust(self, width, *args):
        return self.__class__(self.data.ljust(width, *args))
    def lower(self): return self.__class__(self.data.lower())
    def lstrip(self, chars=None): return self.__class__(self.data.lstrip(chars))
    def partition(self, sep):
        return self.data.partition(sep)
    def replace(self, old, new, maxsplit=-1):
        return self.__class__(self.data.replace(old, new, maxsplit))
    def rfind(self, sub, start=0, end=sys.maxint):
        return self.data.rfind(sub, start, end)
    def rindex(self, sub, start=0, end=sys.maxint):
        return self.data.rindex(sub, start, end)
    def rjust(self, width, *args):
        return self.__class__(self.data.rjust(width, *args))
    def rpartition(self, sep):
        return self.data.rpartition(sep)
    def rstrip(self, chars=None): return self.__class__(self.data.rstrip(chars))
    def split(self, sep=None, maxsplit=-1):
        return self.data.split(sep, maxsplit)
    def rsplit(self, sep=None, maxsplit=-1):
        return self.data.rsplit(sep, maxsplit)
    def splitlines(self, keepends=0): return self.data.splitlines(keepends)
    def startswith(self, prefix, start=0, end=sys.maxint):
        return self.data.startswith(prefix, start, end)
    def strip(self, chars=None): return self.__class__(self.data.strip(chars))
    def swapcase(self): return self.__class__(self.data.swapcase())
    def title(self): return self.__class__(self.data.title())
    def translate(self, *args):
        return self.__class__(self.data.translate(*args))
    def upper(self): return self.__class__(self.data.upper())
    def zfill(self, width): return self.__class__(self.data.zfill(width))

class MutableString(UserString):
    """mutable string objects

    Python strings are immutable objects.  This has the advantage, that
    strings may be used as dictionary keys.  If this property isn't needed
    and you insist on changing string values in place instead, you may cheat
    and use MutableString.

    But the purpose of this class is an educational one: to prevent
    people from inventing their own mutable string class derived
    from UserString and than forget thereby to remove (override) the
    __hash__ method inherited from UserString.  This would lead to
    errors that would be very hard to track down.

    A faster and better solution is to rewrite your program using lists."""
    def __init__(self, string=""):
        self.data = string
    def __hash__(self):
        raise TypeError("unhashable type (it is mutable)")
    def __setitem__(self, index, sub):
        if index < 0:
            index += len(self.data)
        if index < 0 or index >= len(self.data): raise IndexError
        self.data = self.data[:index] + sub + self.data[index+1:]
    def __delitem__(self, index):
        if index < 0:
            index += len(self.data)
        if index < 0 or index >= len(self.data): raise IndexError
        self.data = self.data[:index] + self.data[index+1:]
    def __setslice__(self, start, end, sub):
        start = max(start, 0); end = max(end, 0)
        if isinstance(sub, UserString):
            self.data = self.data[:start]+sub.data+self.data[end:]
        elif isinstance(sub, basestring):
            self.data = self.data[:start]+sub+self.data[end:]
        else:
            self.data =  self.data[:start]+str(sub)+self.data[end:]
    def __delslice__(self, start, end):
        start = max(start, 0); end = max(end, 0)
        self.data = self.data[:start] + self.data[end:]
    def immutable(self):
        return UserString(self.data)
    def __iadd__(self, other):
        if isinstance(other, UserString):
            self.data += other.data
        elif isinstance(other, basestring):
            self.data += other
        else:
            self.data += str(other)
        return self
    def __imul__(self, n):
        self.data *= n
        return self

class String(MutableString, Union):

    _fields_ = [('raw', POINTER(c_char)),
                ('data', c_char_p)]

    def __init__(self, obj=""):
        if isinstance(obj, (str, unicode, UserString)):
            self.data = str(obj)
        else:
            self.raw = obj

    def __len__(self):
        return self.data and len(self.data) or 0

    def from_param(cls, obj):
        # Convert None or 0
        if obj is None or obj == 0:
            return cls(POINTER(c_char)())

        # Convert from String
        elif isinstance(obj, String):
            return obj

        # Convert from str
        elif isinstance(obj, str):
            return cls(obj)

        # Convert from c_char_p
        elif isinstance(obj, c_char_p):
            return obj

        # Convert from POINTER(c_char)
        elif isinstance(obj, POINTER(c_char)):
            return obj

        # Convert from raw pointer
        elif isinstance(obj, int):
            return cls(cast(obj, POINTER(c_char)))

        # Convert from object
        else:
            return String.from_param(obj._as_parameter_)
    from_param = classmethod(from_param)

def ReturnString(obj, func=None, arguments=None):
    return String.from_param(obj)

# As of ctypes 1.0, ctypes does not support custom error-checking
# functions on callbacks, nor does it support custom datatypes on
# callbacks, so we must ensure that all callbacks return
# primitive datatypes.
#
# Non-primitive return values wrapped with UNCHECKED won't be
# typechecked, and will be converted to c_void_p.
def UNCHECKED(type):
    if (hasattr(type, "_type_") and isinstance(type._type_, str)
        and type._type_ != "P"):
        return type
    else:
        return c_void_p

# ctypes doesn't have direct support for variadic functions, so we have to write
# our own wrapper class
class _variadic_function(object):
    def __init__(self,func,restype,argtypes):
        self.func=func
        self.func.restype=restype
        self.argtypes=argtypes
    def _as_parameter_(self):
        # So we can pass this variadic function as a function pointer
        return self.func
    def __call__(self,*args):
        fixed_args=[]
        i=0
        for argtype in self.argtypes:
            # Typecheck what we can
            fixed_args.append(argtype.from_param(args[i]))
            i+=1
        return self.func(*fixed_args+list(args[i:]))

# End preamble

_libs = {}
_libdirs = []

# Begin loader

# ----------------------------------------------------------------------------
# Copyright (c) 2008 David James
# Copyright (c) 2006-2008 Alex Holkner
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of pyglet nor the names of its
#    contributors may be used to endorse or promote products
#    derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# ----------------------------------------------------------------------------

import os.path, re, sys, glob
import platform
import ctypes
import ctypes.util

def _environ_path(name):
    if name in os.environ:
        return os.environ[name].split(":")
    else:
        return []

class LibraryLoader(object):
    def __init__(self):
        self.other_dirs=[]

    def load_library(self,libname):
        """Given the name of a library, load it."""
        paths = self.getpaths(libname)

        for path in paths:
            if os.path.exists(path):
                return self.load(path)

        raise ImportError("%s not found." % libname)

    def load(self,path):
        """Given a path to a library, load it."""
        try:
            # Darwin requires dlopen to be called with mode RTLD_GLOBAL instead
            # of the default RTLD_LOCAL.  Without this, you end up with
            # libraries not being loadable, resulting in "Symbol not found"
            # errors
            if sys.platform == 'darwin':
                return ctypes.CDLL(path, ctypes.RTLD_GLOBAL)
            else:
                return ctypes.cdll.LoadLibrary(path)
        except OSError,e:
            raise ImportError(e)

    def getpaths(self,libname):
        """Return a list of paths where the library might be found."""
        if os.path.isabs(libname):
            yield libname
        else:
            # FIXME / TODO return '.' and os.path.dirname(__file__)
            for path in self.getplatformpaths(libname):
                yield path

            path = ctypes.util.find_library(libname)
            if path: yield path

    def getplatformpaths(self, libname):
        return []

# Darwin (Mac OS X)

class DarwinLibraryLoader(LibraryLoader):
    name_formats = ["lib%s.dylib", "lib%s.so", "lib%s.bundle", "%s.dylib",
                "%s.so", "%s.bundle", "%s"]

    def getplatformpaths(self,libname):
        if os.path.pathsep in libname:
            names = [libname]
        else:
            names = [format % libname for format in self.name_formats]

        for dir in self.getdirs(libname):
            for name in names:
                yield os.path.join(dir,name)

    def getdirs(self,libname):
        '''Implements the dylib search as specified in Apple documentation:

        http://developer.apple.com/documentation/DeveloperTools/Conceptual/
            DynamicLibraries/Articles/DynamicLibraryUsageGuidelines.html

        Before commencing the standard search, the method first checks
        the bundle's ``Frameworks`` directory if the application is running
        within a bundle (OS X .app).
        '''

        dyld_fallback_library_path = _environ_path("DYLD_FALLBACK_LIBRARY_PATH")
        if not dyld_fallback_library_path:
            dyld_fallback_library_path = [os.path.expanduser('~/lib'),
                                          '/usr/local/lib', '/usr/lib']

        dirs = []

        if '/' in libname:
            dirs.extend(_environ_path("DYLD_LIBRARY_PATH"))
        else:
            dirs.extend(_environ_path("LD_LIBRARY_PATH"))
            dirs.extend(_environ_path("DYLD_LIBRARY_PATH"))

        dirs.extend(self.other_dirs)
        dirs.append(".")
        dirs.append(os.path.dirname(__file__))

        if hasattr(sys, 'frozen') and sys.frozen == 'macosx_app':
            dirs.append(os.path.join(
                os.environ['RESOURCEPATH'],
                '..',
                'Frameworks'))

        dirs.extend(dyld_fallback_library_path)

        return dirs

# Posix

class PosixLibraryLoader(LibraryLoader):
    _ld_so_cache = None

    def _create_ld_so_cache(self):
        # Recreate search path followed by ld.so.  This is going to be
        # slow to build, and incorrect (ld.so uses ld.so.cache, which may
        # not be up-to-date).  Used only as fallback for distros without
        # /sbin/ldconfig.
        #
        # We assume the DT_RPATH and DT_RUNPATH binary sections are omitted.

        directories = []
        for name in ("LD_LIBRARY_PATH",
                     "SHLIB_PATH", # HPUX
                     "LIBPATH", # OS/2, AIX
                     "LIBRARY_PATH", # BE/OS
                    ):
            if name in os.environ:
                directories.extend(os.environ[name].split(os.pathsep))
        directories.extend(self.other_dirs)
        directories.append(".")
        directories.append(os.path.dirname(__file__))

        try: directories.extend([dir.strip() for dir in open('/etc/ld.so.conf')])
        except IOError: pass

        unix_lib_dirs_list = ['/lib', '/usr/lib', '/lib64', '/usr/lib64']
        if sys.platform.startswith('linux'):
            # Try and support multiarch work in Ubuntu
            # https://wiki.ubuntu.com/MultiarchSpec
            bitage = platform.architecture()[0]
            if bitage.startswith('32'):
                # Assume Intel/AMD x86 compat
                unix_lib_dirs_list += ['/lib/i386-linux-gnu', '/usr/lib/i386-linux-gnu']
            elif bitage.startswith('64'):
                # Assume Intel/AMD x86 compat
                unix_lib_dirs_list += ['/lib/x86_64-linux-gnu', '/usr/lib/x86_64-linux-gnu']
            else:
                # guess...
                unix_lib_dirs_list += glob.glob('/lib/*linux-gnu')
        directories.extend(unix_lib_dirs_list)

        cache = {}
        lib_re = re.compile(r'lib(.*)\.s[ol]')
        ext_re = re.compile(r'\.s[ol]$')
        for dir in directories:
            try:
                for path in glob.glob("%s/*.s[ol]*" % dir):
                    file = os.path.basename(path)

                    # Index by filename
                    if file not in cache:
                        cache[file] = path

                    # Index by library name
                    match = lib_re.match(file)
                    if match:
                        library = match.group(1)
                        if library not in cache:
                            cache[library] = path
            except OSError:
                pass

        self._ld_so_cache = cache

    def getplatformpaths(self, libname):
        if self._ld_so_cache is None:
            self._create_ld_so_cache()

        result = self._ld_so_cache.get(libname)
        if result: yield result

        path = ctypes.util.find_library(libname)
        if path: yield os.path.join("/lib",path)

# Windows

class _WindowsLibrary(object):
    def __init__(self, path):
        self.cdll = ctypes.cdll.LoadLibrary(path)
        self.windll = ctypes.windll.LoadLibrary(path)

    def __getattr__(self, name):
        try: return getattr(self.cdll,name)
        except AttributeError:
            try: return getattr(self.windll,name)
            except AttributeError:
                raise

class WindowsLibraryLoader(LibraryLoader):
    name_formats = ["%s.dll", "lib%s.dll", "%slib.dll"]

    def load_library(self, libname):
        try:
            result = LibraryLoader.load_library(self, libname)
        except ImportError:
            result = None
            if os.path.sep not in libname:
                for name in self.name_formats:
                    try:
                        result = getattr(ctypes.cdll, name % libname)
                        if result:
                            break
                    except WindowsError:
                        result = None
            if result is None:
                try:
                    result = getattr(ctypes.cdll, libname)
                except WindowsError:
                    result = None
            if result is None:
                raise ImportError("%s not found." % libname)
        return result

    def load(self, path):
        return _WindowsLibrary(path)

    def getplatformpaths(self, libname):
        if os.path.sep not in libname:
            for name in self.name_formats:
                dll_in_current_dir = os.path.abspath(name % libname)
                if os.path.exists(dll_in_current_dir):
                    yield dll_in_current_dir
                path = ctypes.util.find_library(name % libname)
                if path:
                    yield path

# Platform switching

# If your value of sys.platform does not appear in this dict, please contact
# the Ctypesgen maintainers.

loaderclass = {
    "darwin":   DarwinLibraryLoader,
    "cygwin":   WindowsLibraryLoader,
    "win32":    WindowsLibraryLoader
}

loader = loaderclass.get(sys.platform, PosixLibraryLoader)()

def add_library_search_dirs(other_dirs):
    loader.other_dirs = other_dirs

load_library = loader.load_library

del loaderclass

# End loader

add_library_search_dirs([])

# Begin libraries

_libs["x32_bridge.dll"] = load_library("x32_bridge.dll")

# 1 libraries
# End libraries

# No modules

BYTE = c_ubyte # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 115

WORD = c_ushort # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 116

DWORD = c_ulong # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 117

UINT = c_uint # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 135

UINT_PTR = c_uint # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\basetsd.h: 54

LONG_PTR = c_long # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\basetsd.h: 55

ULONG_PTR = c_ulong # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\basetsd.h: 56

SIZE_T = ULONG_PTR # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\basetsd.h: 149

PVOID = POINTER(None) # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winnt.h: 171

LONG = c_long # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winnt.h: 192

HANDLE = POINTER(None) # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winnt.h: 247

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winnt.h: 3840
class struct__MEMORY_BASIC_INFORMATION(Structure):
    pass

struct__MEMORY_BASIC_INFORMATION.__slots__ = [
    'BaseAddress',
    'AllocationBase',
    'AllocationProtect',
    'RegionSize',
    'State',
    'Protect',
    'Type',
]
struct__MEMORY_BASIC_INFORMATION._fields_ = [
    ('BaseAddress', PVOID),
    ('AllocationBase', PVOID),
    ('AllocationProtect', DWORD),
    ('RegionSize', SIZE_T),
    ('State', DWORD),
    ('Protect', DWORD),
    ('Type', DWORD),
]

MEMORY_BASIC_INFORMATION = struct__MEMORY_BASIC_INFORMATION # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winnt.h: 3840

WPARAM = UINT_PTR # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 144

LPARAM = LONG_PTR # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 145

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 168
class struct_HWND__(Structure):
    pass

struct_HWND__.__slots__ = [
    'unused',
]
struct_HWND__._fields_ = [
    ('unused', c_int),
]

HWND = POINTER(struct_HWND__) # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 168

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 252
class struct_tagPOINT(Structure):
    pass

struct_tagPOINT.__slots__ = [
    'x',
    'y',
]
struct_tagPOINT._fields_ = [
    ('x', LONG),
    ('y', LONG),
]

POINT = struct_tagPOINT # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 252

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winuser.h: 834
class struct_tagMSG(Structure):
    pass

struct_tagMSG.__slots__ = [
    'hwnd',
    'message',
    'wParam',
    'lParam',
    'time',
    'pt',
]
struct_tagMSG._fields_ = [
    ('hwnd', HWND),
    ('message', UINT),
    ('wParam', WPARAM),
    ('lParam', LPARAM),
    ('time', DWORD),
    ('pt', POINT),
]

MSG = struct_tagMSG # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winuser.h: 834

duint = c_ulong # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 21

dsint = c_long # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 22

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 43
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'BridgeInit'):
        continue
    BridgeInit = _lib.BridgeInit
    BridgeInit.argtypes = []
    if sizeof(c_int) == sizeof(c_void_p):
        BridgeInit.restype = ReturnString
    else:
        BridgeInit.restype = String
        BridgeInit.errcheck = ReturnString
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 44
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'BridgeStart'):
        continue
    BridgeStart = _lib.BridgeStart
    BridgeStart.argtypes = []
    if sizeof(c_int) == sizeof(c_void_p):
        BridgeStart.restype = ReturnString
    else:
        BridgeStart.restype = String
        BridgeStart.errcheck = ReturnString
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 45
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'BridgeAlloc'):
        continue
    BridgeAlloc = _lib.BridgeAlloc
    BridgeAlloc.argtypes = [c_size_t]
    BridgeAlloc.restype = POINTER(None)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 46
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'BridgeFree'):
        continue
    BridgeFree = _lib.BridgeFree
    BridgeFree.argtypes = [POINTER(None)]
    BridgeFree.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 47
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'BridgeSettingGet'):
        continue
    BridgeSettingGet = _lib.BridgeSettingGet
    BridgeSettingGet.argtypes = [String, String, String]
    BridgeSettingGet.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 48
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'BridgeSettingGetUint'):
        continue
    BridgeSettingGetUint = _lib.BridgeSettingGetUint
    BridgeSettingGetUint.argtypes = [String, String, POINTER(duint)]
    BridgeSettingGetUint.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 49
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'BridgeSettingSet'):
        continue
    BridgeSettingSet = _lib.BridgeSettingSet
    BridgeSettingSet.argtypes = [String, String, String]
    BridgeSettingSet.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 50
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'BridgeSettingSetUint'):
        continue
    BridgeSettingSetUint = _lib.BridgeSettingSetUint
    BridgeSettingSetUint.argtypes = [String, String, duint]
    BridgeSettingSetUint.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 51
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'BridgeGetDbgVersion'):
        continue
    BridgeGetDbgVersion = _lib.BridgeGetDbgVersion
    BridgeGetDbgVersion.argtypes = []
    BridgeGetDbgVersion.restype = c_int
    break

enum_anon_539 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 77

initialized = 0 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 77

paused = (initialized + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 77

running = (paused + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 77

stopped = (running + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 77

DBGSTATE = enum_anon_539 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 77

enum_anon_540 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 88

SEG_DEFAULT = 0 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 88

SEG_ES = (SEG_DEFAULT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 88

SEG_DS = (SEG_ES + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 88

SEG_FS = (SEG_DS + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 88

SEG_GS = (SEG_FS + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 88

SEG_CS = (SEG_GS + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 88

SEG_SS = (SEG_CS + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 88

SEGMENTREG = enum_anon_540 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 88

enum_anon_541 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 98

flagmodule = 1 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 98

flaglabel = 2 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 98

flagcomment = 4 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 98

flagbookmark = 8 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 98

flagfunction = 16 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 98

flagloop = 32 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 98

ADDRINFOFLAGS = enum_anon_541 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 98

enum_anon_542 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 106

bp_none = 0 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 106

bp_normal = 1 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 106

bp_hardware = 2 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 106

bp_memory = 4 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 106

BPXTYPE = enum_anon_542 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 106

enum_anon_543 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 115

FUNC_NONE = 0 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 115

FUNC_BEGIN = (FUNC_NONE + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 115

FUNC_MIDDLE = (FUNC_BEGIN + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 115

FUNC_END = (FUNC_MIDDLE + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 115

FUNC_SINGLE = (FUNC_END + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 115

FUNCTYPE = enum_anon_543 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 115

enum_anon_544 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 124

LOOP_NONE = 0 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 124

LOOP_BEGIN = (LOOP_NONE + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 124

LOOP_MIDDLE = (LOOP_BEGIN + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 124

LOOP_ENTRY = (LOOP_MIDDLE + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 124

LOOP_END = (LOOP_ENTRY + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 124

LOOPTYPE = enum_anon_544 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 124

enum_anon_545 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_SCRIPT_LOAD = 0 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_SCRIPT_UNLOAD = (DBG_SCRIPT_LOAD + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_SCRIPT_RUN = (DBG_SCRIPT_UNLOAD + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_SCRIPT_STEP = (DBG_SCRIPT_RUN + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_SCRIPT_BPTOGGLE = (DBG_SCRIPT_STEP + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_SCRIPT_BPGET = (DBG_SCRIPT_BPTOGGLE + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_SCRIPT_CMDEXEC = (DBG_SCRIPT_BPGET + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_SCRIPT_ABORT = (DBG_SCRIPT_CMDEXEC + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_SCRIPT_GETLINETYPE = (DBG_SCRIPT_ABORT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_SCRIPT_SETIP = (DBG_SCRIPT_GETLINETYPE + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_SCRIPT_GETBRANCHINFO = (DBG_SCRIPT_SETIP + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_SYMBOL_ENUM = (DBG_SCRIPT_GETBRANCHINFO + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_ASSEMBLE_AT = (DBG_SYMBOL_ENUM + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_MODBASE_FROM_NAME = (DBG_ASSEMBLE_AT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_DISASM_AT = (DBG_MODBASE_FROM_NAME + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_STACK_COMMENT_GET = (DBG_DISASM_AT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_GET_THREAD_LIST = (DBG_STACK_COMMENT_GET + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_SETTINGS_UPDATED = (DBG_GET_THREAD_LIST + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_DISASM_FAST_AT = (DBG_SETTINGS_UPDATED + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_MENU_ENTRY_CLICKED = (DBG_DISASM_FAST_AT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_FUNCTION_GET = (DBG_MENU_ENTRY_CLICKED + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_FUNCTION_OVERLAPS = (DBG_FUNCTION_GET + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_FUNCTION_ADD = (DBG_FUNCTION_OVERLAPS + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_FUNCTION_DEL = (DBG_FUNCTION_ADD + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_LOOP_GET = (DBG_FUNCTION_DEL + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_LOOP_OVERLAPS = (DBG_LOOP_GET + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_LOOP_ADD = (DBG_LOOP_OVERLAPS + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_LOOP_DEL = (DBG_LOOP_ADD + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_IS_RUN_LOCKED = (DBG_LOOP_DEL + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_IS_BP_DISABLED = (DBG_IS_RUN_LOCKED + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_SET_AUTO_COMMENT_AT = (DBG_IS_BP_DISABLED + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_DELETE_AUTO_COMMENT_RANGE = (DBG_SET_AUTO_COMMENT_AT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_SET_AUTO_LABEL_AT = (DBG_DELETE_AUTO_COMMENT_RANGE + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_DELETE_AUTO_LABEL_RANGE = (DBG_SET_AUTO_LABEL_AT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_SET_AUTO_BOOKMARK_AT = (DBG_DELETE_AUTO_LABEL_RANGE + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_DELETE_AUTO_BOOKMARK_RANGE = (DBG_SET_AUTO_BOOKMARK_AT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_SET_AUTO_FUNCTION_AT = (DBG_DELETE_AUTO_BOOKMARK_RANGE + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_DELETE_AUTO_FUNCTION_RANGE = (DBG_SET_AUTO_FUNCTION_AT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_GET_STRING_AT = (DBG_DELETE_AUTO_FUNCTION_RANGE + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_GET_FUNCTIONS = (DBG_GET_STRING_AT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_WIN_EVENT = (DBG_GET_FUNCTIONS + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBG_WIN_EVENT_GLOBAL = (DBG_WIN_EVENT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

DBGMSG = enum_anon_545 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 170

enum_anon_546 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 179

linecommand = 0 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 179

linebranch = (linecommand + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 179

linelabel = (linebranch + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 179

linecomment = (linelabel + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 179

lineempty = (linecomment + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 179

SCRIPTLINETYPE = enum_anon_546 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 179

enum_anon_547 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 192

scriptnobranch = 0 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 192

scriptjmp = (scriptnobranch + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 192

scriptjnejnz = (scriptjmp + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 192

scriptjejz = (scriptjnejnz + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 192

scriptjbjl = (scriptjejz + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 192

scriptjajg = (scriptjbjl + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 192

scriptjbejle = (scriptjajg + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 192

scriptjaejge = (scriptjbejle + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 192

scriptcall = (scriptjaejge + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 192

SCRIPTBRANCHTYPE = enum_anon_547 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 192

enum_anon_548 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 199

instr_normal = 0 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 199

instr_branch = (instr_normal + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 199

instr_stack = (instr_branch + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 199

DISASM_INSTRTYPE = enum_anon_548 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 199

enum_anon_549 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 205

arg_normal = 0 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 205

arg_memory = (arg_normal + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 205

DISASM_ARGTYPE = enum_anon_549 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 205

enum_anon_550 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 212

str_none = 0 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 212

str_ascii = (str_none + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 212

str_unicode = (str_ascii + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 212

STRING_TYPE = enum_anon_550 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 212

enum_anon_551 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 224

_PriorityIdle = (-15) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 224

_PriorityAboveNormal = 1 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 224

_PriorityBelowNormal = (-1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 224

_PriorityHighest = 2 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 224

_PriorityLowest = (-2) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 224

_PriorityNormal = 0 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 224

_PriorityTimeCritical = 15 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 224

_PriorityUnknown = 2147483647 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 224

THREADPRIORITY = enum_anon_551 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 224

enum_anon_552 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_Executive = 0 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_FreePage = 1 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_PageIn = 2 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_PoolAllocation = 3 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_DelayExecution = 4 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_Suspended = 5 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_UserRequest = 6 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrExecutive = 7 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrFreePage = 8 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrPageIn = 9 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrPoolAllocation = 10 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrDelayExecution = 11 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrSuspended = 12 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrUserRequest = 13 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrEventPair = 14 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrQueue = 15 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrLpcReceive = 16 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrLpcReply = 17 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrVirtualMemory = 18 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrPageOut = 19 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrRendezvous = 20 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_Spare2 = 21 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_Spare3 = 22 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_Spare4 = 23 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_Spare5 = 24 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrCalloutStack = 25 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrKernel = 26 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrResource = 27 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrPushLock = 28 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrMutex = 29 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrQuantumEnd = 30 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrDispatchInt = 31 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrPreempted = 32 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrYieldExecution = 33 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrFastMutex = 34 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrGuardedMutex = 35 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

_WrRundown = 36 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

THREADWAITREASON = enum_anon_552 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 265

enum_anon_553 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 273

size_byte = 1 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 273

size_word = 2 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 273

size_dword = 4 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 273

size_qword = 8 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 273

MEMORY_SIZE = enum_anon_553 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 273

VALUE_SIZE = MEMORY_SIZE # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 276

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 337
class struct_SYMBOLINFO_(Structure):
    pass

SYMBOLINFO = struct_SYMBOLINFO_ # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 277

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 278
class struct_DBGFUNCTIONS_(Structure):
    pass

DBGFUNCTIONS = struct_DBGFUNCTIONS_ # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 278

CBSYMBOLENUM = CFUNCTYPE(UNCHECKED(None), POINTER(SYMBOLINFO), POINTER(None)) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 280

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 287
class struct_anon_554(Structure):
    pass

struct_anon_554.__slots__ = [
    'mbi',
    'info',
]
struct_anon_554._fields_ = [
    ('mbi', MEMORY_BASIC_INFORMATION),
    ('info', c_char * 256),
]

MEMPAGE = struct_anon_554 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 287

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 293
class struct_anon_555(Structure):
    pass

struct_anon_555.__slots__ = [
    'count',
    'page',
]
struct_anon_555._fields_ = [
    ('count', c_int),
    ('page', POINTER(MEMPAGE)),
]

MEMMAP = struct_anon_555 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 293

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 305
class struct_anon_556(Structure):
    pass

struct_anon_556.__slots__ = [
    'type',
    'addr',
    'enabled',
    'singleshoot',
    'active',
    'name',
    'mod',
    'slot',
]
struct_anon_556._fields_ = [
    ('type', BPXTYPE),
    ('addr', duint),
    ('enabled', c_bool),
    ('singleshoot', c_bool),
    ('active', c_bool),
    ('name', c_char * 256),
    ('mod', c_char * 256),
    ('slot', c_ushort),
]

BRIDGEBP = struct_anon_556 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 305

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 311
class struct_anon_557(Structure):
    pass

struct_anon_557.__slots__ = [
    'count',
    'bp',
]
struct_anon_557._fields_ = [
    ('count', c_int),
    ('bp', POINTER(BRIDGEBP)),
]

BPMAP = struct_anon_557 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 311

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 317
class struct_anon_558(Structure):
    pass

struct_anon_558.__slots__ = [
    'start',
    'end',
]
struct_anon_558._fields_ = [
    ('start', duint),
    ('end', duint),
]

FUNCTION = struct_anon_558 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 317

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 324
class struct_anon_559(Structure):
    pass

struct_anon_559.__slots__ = [
    'depth',
    'start',
    'end',
]
struct_anon_559._fields_ = [
    ('depth', c_int),
    ('start', duint),
    ('end', duint),
]

LOOP = struct_anon_559 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 324

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 335
class struct_anon_560(Structure):
    pass

struct_anon_560.__slots__ = [
    'flags',
    'module',
    'label',
    'comment',
    'isbookmark',
    'function',
    'loop',
]
struct_anon_560._fields_ = [
    ('flags', c_int),
    ('module', c_char * 256),
    ('label', c_char * 256),
    ('comment', c_char * 512),
    ('isbookmark', c_bool),
    ('function', FUNCTION),
    ('loop', LOOP),
]

ADDRINFO = struct_anon_560 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 335

struct_SYMBOLINFO_.__slots__ = [
    'addr',
    'decoratedSymbol',
    'undecoratedSymbol',
]
struct_SYMBOLINFO_._fields_ = [
    ('addr', duint),
    ('decoratedSymbol', String),
    ('undecoratedSymbol', String),
]

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 348
class struct_anon_561(Structure):
    pass

struct_anon_561.__slots__ = [
    'base',
    'name',
]
struct_anon_561._fields_ = [
    ('base', duint),
    ('name', c_char * 256),
]

SYMBOLMODULEINFO = struct_anon_561 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 348

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 355
class struct_anon_562(Structure):
    pass

struct_anon_562.__slots__ = [
    'base',
    'cbSymbolEnum',
    'user',
]
struct_anon_562._fields_ = [
    ('base', duint),
    ('cbSymbolEnum', CBSYMBOLENUM),
    ('user', POINTER(None)),
]

SYMBOLCBINFO = struct_anon_562 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 355

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 368
class struct_anon_563(Structure):
    pass

struct_anon_563.__slots__ = [
    'c',
    'p',
    'a',
    'z',
    's',
    't',
    'i',
    'd',
    'o',
]
struct_anon_563._fields_ = [
    ('c', c_bool),
    ('p', c_bool),
    ('a', c_bool),
    ('z', c_bool),
    ('s', c_bool),
    ('t', c_bool),
    ('i', c_bool),
    ('d', c_bool),
    ('o', c_bool),
]

FLAGS = struct_anon_563 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 368

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 388
class struct_anon_564(Structure):
    pass

struct_anon_564.__slots__ = [
    'FZ',
    'PM',
    'UM',
    'OM',
    'ZM',
    'IM',
    'DM',
    'DAZ',
    'PE',
    'UE',
    'OE',
    'ZE',
    'DE',
    'IE',
    'RC',
]
struct_anon_564._fields_ = [
    ('FZ', c_bool),
    ('PM', c_bool),
    ('UM', c_bool),
    ('OM', c_bool),
    ('ZM', c_bool),
    ('IM', c_bool),
    ('DM', c_bool),
    ('DAZ', c_bool),
    ('PE', c_bool),
    ('UE', c_bool),
    ('OE', c_bool),
    ('ZE', c_bool),
    ('DE', c_bool),
    ('IE', c_bool),
    ('RC', c_ushort),
]

MXCSRFIELDS = struct_anon_564 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 388

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 408
class struct_anon_565(Structure):
    pass

struct_anon_565.__slots__ = [
    'B',
    'C3',
    'C2',
    'C1',
    'C0',
    'IR',
    'SF',
    'P',
    'U',
    'O',
    'Z',
    'D',
    'I',
    'TOP',
]
struct_anon_565._fields_ = [
    ('B', c_bool),
    ('C3', c_bool),
    ('C2', c_bool),
    ('C1', c_bool),
    ('C0', c_bool),
    ('IR', c_bool),
    ('SF', c_bool),
    ('P', c_bool),
    ('U', c_bool),
    ('O', c_bool),
    ('Z', c_bool),
    ('D', c_bool),
    ('I', c_bool),
    ('TOP', c_ushort),
]

X87STATUSWORDFIELDS = struct_anon_565 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 408

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 424
class struct_anon_566(Structure):
    pass

struct_anon_566.__slots__ = [
    'IC',
    'IEM',
    'PM',
    'UM',
    'OM',
    'ZM',
    'DM',
    'IM',
    'RC',
    'PC',
]
struct_anon_566._fields_ = [
    ('IC', c_bool),
    ('IEM', c_bool),
    ('PM', c_bool),
    ('UM', c_bool),
    ('OM', c_bool),
    ('ZM', c_bool),
    ('DM', c_bool),
    ('IM', c_bool),
    ('RC', c_ushort),
    ('PC', c_ushort),
]

X87CONTROLWORDFIELDS = struct_anon_566 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 424

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 443
class struct_anon_568(Structure):
    pass

struct_anon_568.__slots__ = [
    'data',
    'st_value',
    'tag',
]
struct_anon_568._fields_ = [
    ('data', BYTE * 10),
    ('st_value', c_int),
    ('tag', c_int),
]

X87FPUREGISTER = struct_anon_568 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 443

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 455
class struct_anon_569(Structure):
    pass

struct_anon_569.__slots__ = [
    'ControlWord',
    'StatusWord',
    'TagWord',
    'ErrorOffset',
    'ErrorSelector',
    'DataOffset',
    'DataSelector',
    'Cr0NpxState',
]
struct_anon_569._fields_ = [
    ('ControlWord', WORD),
    ('StatusWord', WORD),
    ('TagWord', WORD),
    ('ErrorOffset', DWORD),
    ('ErrorSelector', DWORD),
    ('DataOffset', DWORD),
    ('DataSelector', DWORD),
    ('Cr0NpxState', DWORD),
]

X87FPU = struct_anon_569 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 455

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 522
class struct_anon_572(Structure):
    pass

struct_anon_572.__slots__ = [
    'type',
    'segment',
    'mnemonic',
    'constant',
    'value',
    'memvalue',
]
struct_anon_572._fields_ = [
    ('type', DISASM_ARGTYPE),
    ('segment', SEGMENTREG),
    ('mnemonic', c_char * 64),
    ('constant', duint),
    ('value', duint),
    ('memvalue', duint),
]

DISASM_ARG = struct_anon_572 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 522

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 531
class struct_anon_573(Structure):
    pass

struct_anon_573.__slots__ = [
    'instruction',
    'type',
    'argcount',
    'instr_size',
    'arg',
]
struct_anon_573._fields_ = [
    ('instruction', c_char * 64),
    ('type', DISASM_INSTRTYPE),
    ('argcount', c_int),
    ('instr_size', c_int),
    ('arg', DISASM_ARG * 3),
]

DISASM_INSTR = struct_anon_573 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 531

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 537
class struct_anon_574(Structure):
    pass

struct_anon_574.__slots__ = [
    'color',
    'comment',
]
struct_anon_574._fields_ = [
    ('color', c_char * 8),
    ('comment', c_char * 512),
]

STACK_COMMENT = struct_anon_574 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 537

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 547
class struct_anon_575(Structure):
    pass

struct_anon_575.__slots__ = [
    'ThreadNumber',
    'hThread',
    'dwThreadId',
    'ThreadStartAddress',
    'ThreadLocalBase',
    'threadName',
]
struct_anon_575._fields_ = [
    ('ThreadNumber', c_int),
    ('hThread', HANDLE),
    ('dwThreadId', DWORD),
    ('ThreadStartAddress', duint),
    ('ThreadLocalBase', duint),
    ('threadName', c_char * 256),
]

THREADINFO = struct_anon_575 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 547

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 557
class struct_anon_576(Structure):
    pass

struct_anon_576.__slots__ = [
    'BasicInfo',
    'ThreadCip',
    'SuspendCount',
    'Priority',
    'WaitReason',
    'LastError',
]
struct_anon_576._fields_ = [
    ('BasicInfo', THREADINFO),
    ('ThreadCip', duint),
    ('SuspendCount', DWORD),
    ('Priority', THREADPRIORITY),
    ('WaitReason', THREADWAITREASON),
    ('LastError', DWORD),
]

THREADALLINFO = struct_anon_576 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 557

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 564
class struct_anon_577(Structure):
    pass

struct_anon_577.__slots__ = [
    'count',
    'list',
    'CurrentThread',
]
struct_anon_577._fields_ = [
    ('count', c_int),
    ('list', POINTER(THREADALLINFO)),
    ('CurrentThread', c_int),
]

THREADLIST = struct_anon_577 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 564

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 571
class struct_anon_578(Structure):
    pass

struct_anon_578.__slots__ = [
    'value',
    'size',
    'mnemonic',
]
struct_anon_578._fields_ = [
    ('value', ULONG_PTR),
    ('size', MEMORY_SIZE),
    ('mnemonic', c_char * 64),
]

MEMORY_INFO = struct_anon_578 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 571

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 577
class struct_anon_579(Structure):
    pass

struct_anon_579.__slots__ = [
    'value',
    'size',
]
struct_anon_579._fields_ = [
    ('value', ULONG_PTR),
    ('size', VALUE_SIZE),
]

VALUE_INFO = struct_anon_579 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 577

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 589
class struct_anon_580(Structure):
    pass

struct_anon_580.__slots__ = [
    'type',
    'value',
    'memory',
    'addr',
    'branch',
    'call',
    'size',
    'instruction',
]
struct_anon_580._fields_ = [
    ('type', DWORD),
    ('value', VALUE_INFO),
    ('memory', MEMORY_INFO),
    ('addr', ULONG_PTR),
    ('branch', c_bool),
    ('call', c_bool),
    ('size', c_int),
    ('instruction', c_char * (64 * 4)),
]

BASIC_INSTRUCTION_INFO = struct_anon_580 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 589

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 596
class struct_anon_581(Structure):
    pass

struct_anon_581.__slots__ = [
    'type',
    'dest',
    'branchlabel',
]
struct_anon_581._fields_ = [
    ('type', SCRIPTBRANCHTYPE),
    ('dest', c_int),
    ('branchlabel', c_char * 256),
]

SCRIPTBRANCH = struct_anon_581 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 596

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 605
class struct_anon_582(Structure):
    pass

struct_anon_582.__slots__ = [
    'addr',
    'start',
    'end',
    'manual',
    'depth',
]
struct_anon_582._fields_ = [
    ('addr', duint),
    ('start', duint),
    ('end', duint),
    ('manual', c_bool),
    ('depth', c_int),
]

FUNCTION_LOOP_INFO = struct_anon_582 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 605

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 608
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgInit'):
        continue
    DbgInit = _lib.DbgInit
    DbgInit.argtypes = []
    if sizeof(c_int) == sizeof(c_void_p):
        DbgInit.restype = ReturnString
    else:
        DbgInit.restype = String
        DbgInit.errcheck = ReturnString
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 609
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgExit'):
        continue
    DbgExit = _lib.DbgExit
    DbgExit.argtypes = []
    DbgExit.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 610
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgMemRead'):
        continue
    DbgMemRead = _lib.DbgMemRead
    DbgMemRead.argtypes = [duint, POINTER(c_ubyte), duint]
    DbgMemRead.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 611
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgMemWrite'):
        continue
    DbgMemWrite = _lib.DbgMemWrite
    DbgMemWrite.argtypes = [duint, POINTER(c_ubyte), duint]
    DbgMemWrite.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 612
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgMemGetPageSize'):
        continue
    DbgMemGetPageSize = _lib.DbgMemGetPageSize
    DbgMemGetPageSize.argtypes = [duint]
    DbgMemGetPageSize.restype = duint
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 613
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgMemFindBaseAddr'):
        continue
    DbgMemFindBaseAddr = _lib.DbgMemFindBaseAddr
    DbgMemFindBaseAddr.argtypes = [duint, POINTER(duint)]
    DbgMemFindBaseAddr.restype = duint
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 614
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgCmdExec'):
        continue
    DbgCmdExec = _lib.DbgCmdExec
    DbgCmdExec.argtypes = [String]
    DbgCmdExec.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 615
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgCmdExecDirect'):
        continue
    DbgCmdExecDirect = _lib.DbgCmdExecDirect
    DbgCmdExecDirect.argtypes = [String]
    DbgCmdExecDirect.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 616
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgMemMap'):
        continue
    DbgMemMap = _lib.DbgMemMap
    DbgMemMap.argtypes = [POINTER(MEMMAP)]
    DbgMemMap.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 617
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgIsValidExpression'):
        continue
    DbgIsValidExpression = _lib.DbgIsValidExpression
    DbgIsValidExpression.argtypes = [String]
    DbgIsValidExpression.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 618
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgIsDebugging'):
        continue
    DbgIsDebugging = _lib.DbgIsDebugging
    DbgIsDebugging.argtypes = []
    DbgIsDebugging.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 619
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgIsJumpGoingToExecute'):
        continue
    DbgIsJumpGoingToExecute = _lib.DbgIsJumpGoingToExecute
    DbgIsJumpGoingToExecute.argtypes = [duint]
    DbgIsJumpGoingToExecute.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 620
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgGetLabelAt'):
        continue
    DbgGetLabelAt = _lib.DbgGetLabelAt
    DbgGetLabelAt.argtypes = [duint, SEGMENTREG, String]
    DbgGetLabelAt.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 621
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgSetLabelAt'):
        continue
    DbgSetLabelAt = _lib.DbgSetLabelAt
    DbgSetLabelAt.argtypes = [duint, String]
    DbgSetLabelAt.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 622
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgGetCommentAt'):
        continue
    DbgGetCommentAt = _lib.DbgGetCommentAt
    DbgGetCommentAt.argtypes = [duint, String]
    DbgGetCommentAt.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 623
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgSetCommentAt'):
        continue
    DbgSetCommentAt = _lib.DbgSetCommentAt
    DbgSetCommentAt.argtypes = [duint, String]
    DbgSetCommentAt.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 624
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgGetBookmarkAt'):
        continue
    DbgGetBookmarkAt = _lib.DbgGetBookmarkAt
    DbgGetBookmarkAt.argtypes = [duint]
    DbgGetBookmarkAt.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 625
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgSetBookmarkAt'):
        continue
    DbgSetBookmarkAt = _lib.DbgSetBookmarkAt
    DbgSetBookmarkAt.argtypes = [duint, c_bool]
    DbgSetBookmarkAt.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 626
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgGetModuleAt'):
        continue
    DbgGetModuleAt = _lib.DbgGetModuleAt
    DbgGetModuleAt.argtypes = [duint, String]
    DbgGetModuleAt.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 627
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgGetBpxTypeAt'):
        continue
    DbgGetBpxTypeAt = _lib.DbgGetBpxTypeAt
    DbgGetBpxTypeAt.argtypes = [duint]
    DbgGetBpxTypeAt.restype = BPXTYPE
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 628
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgValFromString'):
        continue
    DbgValFromString = _lib.DbgValFromString
    DbgValFromString.argtypes = [String]
    DbgValFromString.restype = duint
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 630
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgValToString'):
        continue
    DbgValToString = _lib.DbgValToString
    DbgValToString.argtypes = [String, duint]
    DbgValToString.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 631
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgMemIsValidReadPtr'):
        continue
    DbgMemIsValidReadPtr = _lib.DbgMemIsValidReadPtr
    DbgMemIsValidReadPtr.argtypes = [duint]
    DbgMemIsValidReadPtr.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 632
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgGetBpList'):
        continue
    DbgGetBpList = _lib.DbgGetBpList
    DbgGetBpList.argtypes = [BPXTYPE, POINTER(BPMAP)]
    DbgGetBpList.restype = c_int
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 633
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgGetFunctionTypeAt'):
        continue
    DbgGetFunctionTypeAt = _lib.DbgGetFunctionTypeAt
    DbgGetFunctionTypeAt.argtypes = [duint]
    DbgGetFunctionTypeAt.restype = FUNCTYPE
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 634
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgGetLoopTypeAt'):
        continue
    DbgGetLoopTypeAt = _lib.DbgGetLoopTypeAt
    DbgGetLoopTypeAt.argtypes = [duint, c_int]
    DbgGetLoopTypeAt.restype = LOOPTYPE
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 635
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgGetBranchDestination'):
        continue
    DbgGetBranchDestination = _lib.DbgGetBranchDestination
    DbgGetBranchDestination.argtypes = [duint]
    DbgGetBranchDestination.restype = duint
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 636
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgScriptLoad'):
        continue
    DbgScriptLoad = _lib.DbgScriptLoad
    DbgScriptLoad.argtypes = [String]
    DbgScriptLoad.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 637
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgScriptUnload'):
        continue
    DbgScriptUnload = _lib.DbgScriptUnload
    DbgScriptUnload.argtypes = []
    DbgScriptUnload.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 638
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgScriptRun'):
        continue
    DbgScriptRun = _lib.DbgScriptRun
    DbgScriptRun.argtypes = [c_int]
    DbgScriptRun.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 639
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgScriptStep'):
        continue
    DbgScriptStep = _lib.DbgScriptStep
    DbgScriptStep.argtypes = []
    DbgScriptStep.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 640
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgScriptBpToggle'):
        continue
    DbgScriptBpToggle = _lib.DbgScriptBpToggle
    DbgScriptBpToggle.argtypes = [c_int]
    DbgScriptBpToggle.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 641
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgScriptBpGet'):
        continue
    DbgScriptBpGet = _lib.DbgScriptBpGet
    DbgScriptBpGet.argtypes = [c_int]
    DbgScriptBpGet.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 642
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgScriptCmdExec'):
        continue
    DbgScriptCmdExec = _lib.DbgScriptCmdExec
    DbgScriptCmdExec.argtypes = [String]
    DbgScriptCmdExec.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 643
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgScriptAbort'):
        continue
    DbgScriptAbort = _lib.DbgScriptAbort
    DbgScriptAbort.argtypes = []
    DbgScriptAbort.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 644
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgScriptGetLineType'):
        continue
    DbgScriptGetLineType = _lib.DbgScriptGetLineType
    DbgScriptGetLineType.argtypes = [c_int]
    DbgScriptGetLineType.restype = SCRIPTLINETYPE
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 645
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgScriptSetIp'):
        continue
    DbgScriptSetIp = _lib.DbgScriptSetIp
    DbgScriptSetIp.argtypes = [c_int]
    DbgScriptSetIp.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 646
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgScriptGetBranchInfo'):
        continue
    DbgScriptGetBranchInfo = _lib.DbgScriptGetBranchInfo
    DbgScriptGetBranchInfo.argtypes = [c_int, POINTER(SCRIPTBRANCH)]
    DbgScriptGetBranchInfo.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 647
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgSymbolEnum'):
        continue
    DbgSymbolEnum = _lib.DbgSymbolEnum
    DbgSymbolEnum.argtypes = [duint, CBSYMBOLENUM, POINTER(None)]
    DbgSymbolEnum.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 648
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgAssembleAt'):
        continue
    DbgAssembleAt = _lib.DbgAssembleAt
    DbgAssembleAt.argtypes = [duint, String]
    DbgAssembleAt.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 649
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgModBaseFromName'):
        continue
    DbgModBaseFromName = _lib.DbgModBaseFromName
    DbgModBaseFromName.argtypes = [String]
    DbgModBaseFromName.restype = duint
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 650
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgDisasmAt'):
        continue
    DbgDisasmAt = _lib.DbgDisasmAt
    DbgDisasmAt.argtypes = [duint, POINTER(DISASM_INSTR)]
    DbgDisasmAt.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 651
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgStackCommentGet'):
        continue
    DbgStackCommentGet = _lib.DbgStackCommentGet
    DbgStackCommentGet.argtypes = [duint, POINTER(STACK_COMMENT)]
    DbgStackCommentGet.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 652
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgGetThreadList'):
        continue
    DbgGetThreadList = _lib.DbgGetThreadList
    DbgGetThreadList.argtypes = [POINTER(THREADLIST)]
    DbgGetThreadList.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 653
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgSettingsUpdated'):
        continue
    DbgSettingsUpdated = _lib.DbgSettingsUpdated
    DbgSettingsUpdated.argtypes = []
    DbgSettingsUpdated.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 654
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgDisasmFastAt'):
        continue
    DbgDisasmFastAt = _lib.DbgDisasmFastAt
    DbgDisasmFastAt.argtypes = [duint, POINTER(BASIC_INSTRUCTION_INFO)]
    DbgDisasmFastAt.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 655
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgMenuEntryClicked'):
        continue
    DbgMenuEntryClicked = _lib.DbgMenuEntryClicked
    DbgMenuEntryClicked.argtypes = [c_int]
    DbgMenuEntryClicked.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 656
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgFunctionGet'):
        continue
    DbgFunctionGet = _lib.DbgFunctionGet
    DbgFunctionGet.argtypes = [duint, POINTER(duint), POINTER(duint)]
    DbgFunctionGet.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 657
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgFunctionOverlaps'):
        continue
    DbgFunctionOverlaps = _lib.DbgFunctionOverlaps
    DbgFunctionOverlaps.argtypes = [duint, duint]
    DbgFunctionOverlaps.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 658
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgFunctionAdd'):
        continue
    DbgFunctionAdd = _lib.DbgFunctionAdd
    DbgFunctionAdd.argtypes = [duint, duint]
    DbgFunctionAdd.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 659
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgFunctionDel'):
        continue
    DbgFunctionDel = _lib.DbgFunctionDel
    DbgFunctionDel.argtypes = [duint]
    DbgFunctionDel.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 660
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgLoopGet'):
        continue
    DbgLoopGet = _lib.DbgLoopGet
    DbgLoopGet.argtypes = [c_int, duint, POINTER(duint), POINTER(duint)]
    DbgLoopGet.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 661
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgLoopOverlaps'):
        continue
    DbgLoopOverlaps = _lib.DbgLoopOverlaps
    DbgLoopOverlaps.argtypes = [c_int, duint, duint]
    DbgLoopOverlaps.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 662
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgLoopAdd'):
        continue
    DbgLoopAdd = _lib.DbgLoopAdd
    DbgLoopAdd.argtypes = [duint, duint]
    DbgLoopAdd.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 663
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgLoopDel'):
        continue
    DbgLoopDel = _lib.DbgLoopDel
    DbgLoopDel.argtypes = [c_int, duint]
    DbgLoopDel.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 664
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgIsRunLocked'):
        continue
    DbgIsRunLocked = _lib.DbgIsRunLocked
    DbgIsRunLocked.argtypes = []
    DbgIsRunLocked.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 665
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgIsBpDisabled'):
        continue
    DbgIsBpDisabled = _lib.DbgIsBpDisabled
    DbgIsBpDisabled.argtypes = [duint]
    DbgIsBpDisabled.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 666
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgSetAutoCommentAt'):
        continue
    DbgSetAutoCommentAt = _lib.DbgSetAutoCommentAt
    DbgSetAutoCommentAt.argtypes = [duint, String]
    DbgSetAutoCommentAt.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 667
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgClearAutoCommentRange'):
        continue
    DbgClearAutoCommentRange = _lib.DbgClearAutoCommentRange
    DbgClearAutoCommentRange.argtypes = [duint, duint]
    DbgClearAutoCommentRange.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 668
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgSetAutoLabelAt'):
        continue
    DbgSetAutoLabelAt = _lib.DbgSetAutoLabelAt
    DbgSetAutoLabelAt.argtypes = [duint, String]
    DbgSetAutoLabelAt.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 669
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgClearAutoLabelRange'):
        continue
    DbgClearAutoLabelRange = _lib.DbgClearAutoLabelRange
    DbgClearAutoLabelRange.argtypes = [duint, duint]
    DbgClearAutoLabelRange.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 670
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgSetAutoBookmarkAt'):
        continue
    DbgSetAutoBookmarkAt = _lib.DbgSetAutoBookmarkAt
    DbgSetAutoBookmarkAt.argtypes = [duint]
    DbgSetAutoBookmarkAt.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 671
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgClearAutoBookmarkRange'):
        continue
    DbgClearAutoBookmarkRange = _lib.DbgClearAutoBookmarkRange
    DbgClearAutoBookmarkRange.argtypes = [duint, duint]
    DbgClearAutoBookmarkRange.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 672
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgSetAutoFunctionAt'):
        continue
    DbgSetAutoFunctionAt = _lib.DbgSetAutoFunctionAt
    DbgSetAutoFunctionAt.argtypes = [duint, duint]
    DbgSetAutoFunctionAt.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 673
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgClearAutoFunctionRange'):
        continue
    DbgClearAutoFunctionRange = _lib.DbgClearAutoFunctionRange
    DbgClearAutoFunctionRange.argtypes = [duint, duint]
    DbgClearAutoFunctionRange.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 674
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgGetStringAt'):
        continue
    DbgGetStringAt = _lib.DbgGetStringAt
    DbgGetStringAt.argtypes = [duint, String]
    DbgGetStringAt.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 675
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgFunctions'):
        continue
    DbgFunctions = _lib.DbgFunctions
    DbgFunctions.argtypes = []
    DbgFunctions.restype = POINTER(DBGFUNCTIONS)
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 676
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgWinEvent'):
        continue
    DbgWinEvent = _lib.DbgWinEvent
    DbgWinEvent.argtypes = [POINTER(MSG), POINTER(c_long)]
    DbgWinEvent.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 677
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgWinEventGlobal'):
        continue
    DbgWinEventGlobal = _lib.DbgWinEventGlobal
    DbgWinEventGlobal.argtypes = [POINTER(MSG)]
    DbgWinEventGlobal.restype = c_bool
    break

enum_anon_583 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_DISASSEMBLE_AT = 0 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SET_DEBUG_STATE = (GUI_DISASSEMBLE_AT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_ADD_MSG_TO_LOG = (GUI_SET_DEBUG_STATE + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_CLEAR_LOG = (GUI_ADD_MSG_TO_LOG + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_UPDATE_REGISTER_VIEW = (GUI_CLEAR_LOG + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_UPDATE_DISASSEMBLY_VIEW = (GUI_UPDATE_REGISTER_VIEW + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_UPDATE_BREAKPOINTS_VIEW = (GUI_UPDATE_DISASSEMBLY_VIEW + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_UPDATE_WINDOW_TITLE = (GUI_UPDATE_BREAKPOINTS_VIEW + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_GET_WINDOW_HANDLE = (GUI_UPDATE_WINDOW_TITLE + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_DUMP_AT = (GUI_GET_WINDOW_HANDLE + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SCRIPT_ADD = (GUI_DUMP_AT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SCRIPT_CLEAR = (GUI_SCRIPT_ADD + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SCRIPT_SETIP = (GUI_SCRIPT_CLEAR + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SCRIPT_ERROR = (GUI_SCRIPT_SETIP + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SCRIPT_SETTITLE = (GUI_SCRIPT_ERROR + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SCRIPT_SETINFOLINE = (GUI_SCRIPT_SETTITLE + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SCRIPT_MESSAGE = (GUI_SCRIPT_SETINFOLINE + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SCRIPT_MSGYN = (GUI_SCRIPT_MESSAGE + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SYMBOL_LOG_ADD = (GUI_SCRIPT_MSGYN + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SYMBOL_LOG_CLEAR = (GUI_SYMBOL_LOG_ADD + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SYMBOL_SET_PROGRESS = (GUI_SYMBOL_LOG_CLEAR + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SYMBOL_UPDATE_MODULE_LIST = (GUI_SYMBOL_SET_PROGRESS + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_REF_ADDCOLUMN = (GUI_SYMBOL_UPDATE_MODULE_LIST + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_REF_SETROWCOUNT = (GUI_REF_ADDCOLUMN + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_REF_GETROWCOUNT = (GUI_REF_SETROWCOUNT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_REF_DELETEALLCOLUMNS = (GUI_REF_GETROWCOUNT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_REF_SETCELLCONTENT = (GUI_REF_DELETEALLCOLUMNS + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_REF_GETCELLCONTENT = (GUI_REF_SETCELLCONTENT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_REF_RELOADDATA = (GUI_REF_GETCELLCONTENT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_REF_SETSINGLESELECTION = (GUI_REF_RELOADDATA + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_REF_SETPROGRESS = (GUI_REF_SETSINGLESELECTION + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_REF_SETSEARCHSTARTCOL = (GUI_REF_SETPROGRESS + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_STACK_DUMP_AT = (GUI_REF_SETSEARCHSTARTCOL + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_UPDATE_DUMP_VIEW = (GUI_STACK_DUMP_AT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_UPDATE_THREAD_VIEW = (GUI_UPDATE_DUMP_VIEW + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_ADD_RECENT_FILE = (GUI_UPDATE_THREAD_VIEW + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SET_LAST_EXCEPTION = (GUI_ADD_RECENT_FILE + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_GET_DISASSEMBLY = (GUI_SET_LAST_EXCEPTION + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_MENU_ADD = (GUI_GET_DISASSEMBLY + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_MENU_ADD_ENTRY = (GUI_MENU_ADD + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_MENU_ADD_SEPARATOR = (GUI_MENU_ADD_ENTRY + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_MENU_CLEAR = (GUI_MENU_ADD_SEPARATOR + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SELECTION_GET = (GUI_MENU_CLEAR + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SELECTION_SET = (GUI_SELECTION_GET + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_GETLINE_WINDOW = (GUI_SELECTION_SET + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_AUTOCOMPLETE_ADDCMD = (GUI_GETLINE_WINDOW + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_AUTOCOMPLETE_DELCMD = (GUI_AUTOCOMPLETE_ADDCMD + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_AUTOCOMPLETE_CLEARALL = (GUI_AUTOCOMPLETE_DELCMD + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SCRIPT_ENABLEHIGHLIGHTING = (GUI_AUTOCOMPLETE_CLEARALL + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_ADD_MSG_TO_STATUSBAR = (GUI_SCRIPT_ENABLEHIGHLIGHTING + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_UPDATE_SIDEBAR = (GUI_ADD_MSG_TO_STATUSBAR + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_REPAINT_TABLE_VIEW = (GUI_UPDATE_SIDEBAR + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_UPDATE_PATCHES = (GUI_REPAINT_TABLE_VIEW + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_UPDATE_CALLSTACK = (GUI_UPDATE_PATCHES + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_SYMBOL_REFRESH_CURRENT = (GUI_UPDATE_CALLSTACK + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_UPDATE_MEMORY_VIEW = (GUI_SYMBOL_REFRESH_CURRENT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUI_REF_INITIALIZE = (GUI_UPDATE_MEMORY_VIEW + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

GUIMSG = enum_anon_583 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 749

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 757
class struct_anon_584(Structure):
    pass

struct_anon_584.__slots__ = [
    'row',
    'col',
    'str',
]
struct_anon_584._fields_ = [
    ('row', c_int),
    ('col', c_int),
    ('str', String),
]

CELLINFO = struct_anon_584 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 757

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 763
class struct_anon_585(Structure):
    pass

struct_anon_585.__slots__ = [
    'start',
    'end',
]
struct_anon_585._fields_ = [
    ('start', duint),
    ('end', duint),
]

SELECTIONDATA = struct_anon_585 # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 763

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 766
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiDisasmAt'):
        continue
    GuiDisasmAt = _lib.GuiDisasmAt
    GuiDisasmAt.argtypes = [duint, duint]
    GuiDisasmAt.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 767
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiSetDebugState'):
        continue
    GuiSetDebugState = _lib.GuiSetDebugState
    GuiSetDebugState.argtypes = [DBGSTATE]
    GuiSetDebugState.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 768
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiAddLogMessage'):
        continue
    GuiAddLogMessage = _lib.GuiAddLogMessage
    GuiAddLogMessage.argtypes = [String]
    GuiAddLogMessage.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 769
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiLogClear'):
        continue
    GuiLogClear = _lib.GuiLogClear
    GuiLogClear.argtypes = []
    GuiLogClear.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 770
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiUpdateAllViews'):
        continue
    GuiUpdateAllViews = _lib.GuiUpdateAllViews
    GuiUpdateAllViews.argtypes = []
    GuiUpdateAllViews.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 771
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiUpdateRegisterView'):
        continue
    GuiUpdateRegisterView = _lib.GuiUpdateRegisterView
    GuiUpdateRegisterView.argtypes = []
    GuiUpdateRegisterView.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 772
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiUpdateDisassemblyView'):
        continue
    GuiUpdateDisassemblyView = _lib.GuiUpdateDisassemblyView
    GuiUpdateDisassemblyView.argtypes = []
    GuiUpdateDisassemblyView.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 773
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiUpdateBreakpointsView'):
        continue
    GuiUpdateBreakpointsView = _lib.GuiUpdateBreakpointsView
    GuiUpdateBreakpointsView.argtypes = []
    GuiUpdateBreakpointsView.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 774
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiUpdateWindowTitle'):
        continue
    GuiUpdateWindowTitle = _lib.GuiUpdateWindowTitle
    GuiUpdateWindowTitle.argtypes = [String]
    GuiUpdateWindowTitle.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 775
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiGetWindowHandle'):
        continue
    GuiGetWindowHandle = _lib.GuiGetWindowHandle
    GuiGetWindowHandle.argtypes = []
    GuiGetWindowHandle.restype = HWND
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 776
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiDumpAt'):
        continue
    GuiDumpAt = _lib.GuiDumpAt
    GuiDumpAt.argtypes = [duint]
    GuiDumpAt.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 777
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiScriptAdd'):
        continue
    GuiScriptAdd = _lib.GuiScriptAdd
    GuiScriptAdd.argtypes = [c_int, POINTER(POINTER(c_char))]
    GuiScriptAdd.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 778
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiScriptClear'):
        continue
    GuiScriptClear = _lib.GuiScriptClear
    GuiScriptClear.argtypes = []
    GuiScriptClear.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 779
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiScriptSetIp'):
        continue
    GuiScriptSetIp = _lib.GuiScriptSetIp
    GuiScriptSetIp.argtypes = [c_int]
    GuiScriptSetIp.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 780
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiScriptError'):
        continue
    GuiScriptError = _lib.GuiScriptError
    GuiScriptError.argtypes = [c_int, String]
    GuiScriptError.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 781
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiScriptSetTitle'):
        continue
    GuiScriptSetTitle = _lib.GuiScriptSetTitle
    GuiScriptSetTitle.argtypes = [String]
    GuiScriptSetTitle.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 782
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiScriptSetInfoLine'):
        continue
    GuiScriptSetInfoLine = _lib.GuiScriptSetInfoLine
    GuiScriptSetInfoLine.argtypes = [c_int, String]
    GuiScriptSetInfoLine.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 783
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiScriptMessage'):
        continue
    GuiScriptMessage = _lib.GuiScriptMessage
    GuiScriptMessage.argtypes = [String]
    GuiScriptMessage.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 784
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiScriptMsgyn'):
        continue
    GuiScriptMsgyn = _lib.GuiScriptMsgyn
    GuiScriptMsgyn.argtypes = [String]
    GuiScriptMsgyn.restype = c_int
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 785
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiScriptEnableHighlighting'):
        continue
    GuiScriptEnableHighlighting = _lib.GuiScriptEnableHighlighting
    GuiScriptEnableHighlighting.argtypes = [c_bool]
    GuiScriptEnableHighlighting.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 786
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiSymbolLogAdd'):
        continue
    GuiSymbolLogAdd = _lib.GuiSymbolLogAdd
    GuiSymbolLogAdd.argtypes = [String]
    GuiSymbolLogAdd.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 787
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiSymbolLogClear'):
        continue
    GuiSymbolLogClear = _lib.GuiSymbolLogClear
    GuiSymbolLogClear.argtypes = []
    GuiSymbolLogClear.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 788
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiSymbolSetProgress'):
        continue
    GuiSymbolSetProgress = _lib.GuiSymbolSetProgress
    GuiSymbolSetProgress.argtypes = [c_int]
    GuiSymbolSetProgress.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 789
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiSymbolUpdateModuleList'):
        continue
    GuiSymbolUpdateModuleList = _lib.GuiSymbolUpdateModuleList
    GuiSymbolUpdateModuleList.argtypes = [c_int, POINTER(SYMBOLMODULEINFO)]
    GuiSymbolUpdateModuleList.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 790
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiSymbolRefreshCurrent'):
        continue
    GuiSymbolRefreshCurrent = _lib.GuiSymbolRefreshCurrent
    GuiSymbolRefreshCurrent.argtypes = []
    GuiSymbolRefreshCurrent.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 791
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiReferenceAddColumn'):
        continue
    GuiReferenceAddColumn = _lib.GuiReferenceAddColumn
    GuiReferenceAddColumn.argtypes = [c_int, String]
    GuiReferenceAddColumn.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 792
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiReferenceSetRowCount'):
        continue
    GuiReferenceSetRowCount = _lib.GuiReferenceSetRowCount
    GuiReferenceSetRowCount.argtypes = [c_int]
    GuiReferenceSetRowCount.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 793
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiReferenceGetRowCount'):
        continue
    GuiReferenceGetRowCount = _lib.GuiReferenceGetRowCount
    GuiReferenceGetRowCount.argtypes = []
    GuiReferenceGetRowCount.restype = c_int
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 794
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiReferenceDeleteAllColumns'):
        continue
    GuiReferenceDeleteAllColumns = _lib.GuiReferenceDeleteAllColumns
    GuiReferenceDeleteAllColumns.argtypes = []
    GuiReferenceDeleteAllColumns.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 795
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiReferenceInitialize'):
        continue
    GuiReferenceInitialize = _lib.GuiReferenceInitialize
    GuiReferenceInitialize.argtypes = [String]
    GuiReferenceInitialize.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 796
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiReferenceSetCellContent'):
        continue
    GuiReferenceSetCellContent = _lib.GuiReferenceSetCellContent
    GuiReferenceSetCellContent.argtypes = [c_int, c_int, String]
    GuiReferenceSetCellContent.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 797
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiReferenceGetCellContent'):
        continue
    GuiReferenceGetCellContent = _lib.GuiReferenceGetCellContent
    GuiReferenceGetCellContent.argtypes = [c_int, c_int]
    if sizeof(c_int) == sizeof(c_void_p):
        GuiReferenceGetCellContent.restype = ReturnString
    else:
        GuiReferenceGetCellContent.restype = String
        GuiReferenceGetCellContent.errcheck = ReturnString
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 798
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiReferenceReloadData'):
        continue
    GuiReferenceReloadData = _lib.GuiReferenceReloadData
    GuiReferenceReloadData.argtypes = []
    GuiReferenceReloadData.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 799
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiReferenceSetSingleSelection'):
        continue
    GuiReferenceSetSingleSelection = _lib.GuiReferenceSetSingleSelection
    GuiReferenceSetSingleSelection.argtypes = [c_int, c_bool]
    GuiReferenceSetSingleSelection.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 800
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiReferenceSetProgress'):
        continue
    GuiReferenceSetProgress = _lib.GuiReferenceSetProgress
    GuiReferenceSetProgress.argtypes = [c_int]
    GuiReferenceSetProgress.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 801
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiReferenceSetSearchStartCol'):
        continue
    GuiReferenceSetSearchStartCol = _lib.GuiReferenceSetSearchStartCol
    GuiReferenceSetSearchStartCol.argtypes = [c_int]
    GuiReferenceSetSearchStartCol.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 802
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiStackDumpAt'):
        continue
    GuiStackDumpAt = _lib.GuiStackDumpAt
    GuiStackDumpAt.argtypes = [duint, duint]
    GuiStackDumpAt.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 803
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiUpdateDumpView'):
        continue
    GuiUpdateDumpView = _lib.GuiUpdateDumpView
    GuiUpdateDumpView.argtypes = []
    GuiUpdateDumpView.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 804
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiUpdateThreadView'):
        continue
    GuiUpdateThreadView = _lib.GuiUpdateThreadView
    GuiUpdateThreadView.argtypes = []
    GuiUpdateThreadView.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 805
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiUpdateMemoryView'):
        continue
    GuiUpdateMemoryView = _lib.GuiUpdateMemoryView
    GuiUpdateMemoryView.argtypes = []
    GuiUpdateMemoryView.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 806
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiAddRecentFile'):
        continue
    GuiAddRecentFile = _lib.GuiAddRecentFile
    GuiAddRecentFile.argtypes = [String]
    GuiAddRecentFile.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 807
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiSetLastException'):
        continue
    GuiSetLastException = _lib.GuiSetLastException
    GuiSetLastException.argtypes = [c_uint]
    GuiSetLastException.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 808
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiGetDisassembly'):
        continue
    GuiGetDisassembly = _lib.GuiGetDisassembly
    GuiGetDisassembly.argtypes = [duint, String]
    GuiGetDisassembly.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 809
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiMenuAdd'):
        continue
    GuiMenuAdd = _lib.GuiMenuAdd
    GuiMenuAdd.argtypes = [c_int, String]
    GuiMenuAdd.restype = c_int
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 810
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiMenuAddEntry'):
        continue
    GuiMenuAddEntry = _lib.GuiMenuAddEntry
    GuiMenuAddEntry.argtypes = [c_int, String]
    GuiMenuAddEntry.restype = c_int
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 811
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiMenuAddSeparator'):
        continue
    GuiMenuAddSeparator = _lib.GuiMenuAddSeparator
    GuiMenuAddSeparator.argtypes = [c_int]
    GuiMenuAddSeparator.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 812
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiMenuClear'):
        continue
    GuiMenuClear = _lib.GuiMenuClear
    GuiMenuClear.argtypes = [c_int]
    GuiMenuClear.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 813
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiSelectionGet'):
        continue
    GuiSelectionGet = _lib.GuiSelectionGet
    GuiSelectionGet.argtypes = [c_int, POINTER(SELECTIONDATA)]
    GuiSelectionGet.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 814
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiSelectionSet'):
        continue
    GuiSelectionSet = _lib.GuiSelectionSet
    GuiSelectionSet.argtypes = [c_int, POINTER(SELECTIONDATA)]
    GuiSelectionSet.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 815
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiGetLineWindow'):
        continue
    GuiGetLineWindow = _lib.GuiGetLineWindow
    GuiGetLineWindow.argtypes = [String, String]
    GuiGetLineWindow.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 816
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiAutoCompleteAddCmd'):
        continue
    GuiAutoCompleteAddCmd = _lib.GuiAutoCompleteAddCmd
    GuiAutoCompleteAddCmd.argtypes = [String]
    GuiAutoCompleteAddCmd.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 817
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiAutoCompleteDelCmd'):
        continue
    GuiAutoCompleteDelCmd = _lib.GuiAutoCompleteDelCmd
    GuiAutoCompleteDelCmd.argtypes = [String]
    GuiAutoCompleteDelCmd.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 818
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiAutoCompleteClearAll'):
        continue
    GuiAutoCompleteClearAll = _lib.GuiAutoCompleteClearAll
    GuiAutoCompleteClearAll.argtypes = []
    GuiAutoCompleteClearAll.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 819
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiAddStatusBarMessage'):
        continue
    GuiAddStatusBarMessage = _lib.GuiAddStatusBarMessage
    GuiAddStatusBarMessage.argtypes = [String]
    GuiAddStatusBarMessage.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 820
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiUpdateSideBar'):
        continue
    GuiUpdateSideBar = _lib.GuiUpdateSideBar
    GuiUpdateSideBar.argtypes = []
    GuiUpdateSideBar.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 821
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiRepaintTableView'):
        continue
    GuiRepaintTableView = _lib.GuiRepaintTableView
    GuiRepaintTableView.argtypes = []
    GuiRepaintTableView.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 822
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiUpdatePatches'):
        continue
    GuiUpdatePatches = _lib.GuiUpdatePatches
    GuiUpdatePatches.argtypes = []
    GuiUpdatePatches.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 823
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiUpdateCallStack'):
        continue
    GuiUpdateCallStack = _lib.GuiUpdateCallStack
    GuiUpdateCallStack.argtypes = []
    GuiUpdateCallStack.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 824
for _lib in _libs.itervalues():
    if not hasattr(_lib, 'GuiUpdateMemoryView'):
        continue
    GuiUpdateMemoryView = _lib.GuiUpdateMemoryView
    GuiUpdateMemoryView.argtypes = []
    GuiUpdateMemoryView.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 39
try:
    MAX_SETTING_SIZE = 65536
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 40
try:
    DBG_VERSION = 23
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 54
try:
    MAX_LABEL_SIZE = 256
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 55
try:
    MAX_COMMENT_SIZE = 512
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 56
try:
    MAX_MODULE_SIZE = 256
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 57
try:
    MAX_BREAKPOINT_SIZE = 256
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 58
try:
    MAX_SCRIPT_LINE_SIZE = 2048
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 59
try:
    MAX_THREAD_NAME_SIZE = 256
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 60
try:
    MAX_STRING_SIZE = 512
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 61
try:
    MAX_ERROR_SIZE = 512
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 62
try:
    RIGHTS_STRING_SIZE = (sizeof('ERWCG') + 1)
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 64
try:
    TYPE_VALUE = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 65
try:
    TYPE_MEMORY = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 66
try:
    TYPE_ADDR = 4
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 67
try:
    MAX_MNEMONIC_SIZE = 64
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 68
try:
    PAGE_SIZE = 4096
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 680
try:
    GUI_PLUGIN_MENU = 0
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 682
try:
    GUI_DISASSEMBLY = 0
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 683
try:
    GUI_DUMP = 1
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 684
try:
    GUI_STACK = 2
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 686
try:
    GUI_MAX_LINE_SIZE = 65536
except:
    pass

# E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 687
try:
    GUI_MAX_DISASSEMBLY_SIZE = 2048
except:
    pass

SYMBOLINFO_ = struct_SYMBOLINFO_ # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 337

DBGFUNCTIONS_ = struct_DBGFUNCTIONS_ # E:\\x64dbg_pythonloader\\pluginsdk\\BridgeMain.h: 278

# No inserted files

##
## Extra definitions from x64bd_struct.py.extra
##

# Forward the api functions

import x64dbg_api

put_log = x64dbg_api.put_log
register_command = x64dbg_api.register_command
unregister_command = x64dbg_api.unregister_command

# ctype unpack

def Unpack(ctype, buf):
    cstring = create_string_buffer(buf)
    ctype_instance = cast(pointer(cstring), POINTER(ctype)).contents
    return ctype_instance

# Decorator functions

def register(debug_only):
	def _register(func):
		register_command(sys.modules[func.__module__], func.__name__, debug_only)
		
		def register_and_call(*args, **kwargs):
			return func(*args, **kwargs)
		return register_and_call
	return _register
	
def event_param(param_type):
	def _event_param(func):
		def event_param_and_call(*args, **kwargs):
			ctype_struct = Unpack(param_type, args[0])
			return func(ctype_struct)
		return event_param_and_call
	return _event_param

# XMMREGISTER struct

class struct__XMMREGISTER(Structure):
    pass

struct__XMMREGISTER.__slots__ = [
    'Low',
    'High'
]
struct__XMMREGISTER._fields_ = [
    ('Low', c_ulonglong),
    ('High', c_longlong)
]

XMMREGISTER = struct__XMMREGISTER

# YMMREGISTER struct

class struct__YMMREGISTER(Structure):
    pass

struct__YMMREGISTER.__slots__ = [
    'Low',
    'High'
]
struct__YMMREGISTER._fields_ = [
    ('Low', XMMREGISTER),
    ('High', XMMREGISTER)
]

YMMREGISTER = struct__YMMREGISTER

# REGISTERCONTEXT struct

class struct__REGISTERCONTEXT(Structure):
    pass

if (x64dbg_api.is_32_bit()):
	struct__REGISTERCONTEXT.__slots__ = [
		'cax',
		'ccx',
		'cdx',
		'cbx',
		'csp',
		'cbp',
		'csi',
		'cdi',
		'cip',
		'eflags',
		'gs',
		'fs',
		'es',
		'ds',
		'cs',
		'ss',
		'dr0',
		'dr1',
		'dr2',
		'dr3',
		'dr6',
		'dr7',
		'RegisteredArea',
		'x87fpu',
		'MxCsr',
		'XmmRegisters',
		'YmmRegisters'
	]
	struct__REGISTERCONTEXT._fields_ = [
		('cax', POINTER(c_ulong)),
		('ccx', POINTER(c_ulong)),
		('cdx', POINTER(c_ulong)),
		('cbx', POINTER(c_ulong)),
		('csp', POINTER(c_ulong)),
		('cbp', POINTER(c_ulong)),
		('csi', POINTER(c_ulong)),
		('cdi', POINTER(c_ulong)),
		('cip', POINTER(c_ulong)),
		('eflags', POINTER(c_ulong)),
		('gs', c_ushort),
		('fs', c_ushort),
		('es', c_ushort),
		('ds', c_ushort),
		('cs', c_ushort),
		('ss', c_ushort),
		('dr0', POINTER(c_ulong)),
		('dr1', POINTER(c_ulong)),
		('dr2', POINTER(c_ulong)),
		('dr3', POINTER(c_ulong)),
		('dr6', POINTER(c_ulong)),
		('dr7', POINTER(c_ulong)),
		('RegisteredArea', c_char * 80),
		('x87fpu', X87FPU),
		('MxCsr', DWORD),
		('XmmRegisters', XMMREGISTER * 8),  # is this correct?
		('YmmRegisters', YMMREGISTER * 8)   # is this correct?
	]
else:
	struct__REGISTERCONTEXT.__slots__ = [
		'cax',
		'ccx',
		'cdx',
		'cbx',
		'csp',
		'cbp',
		'csi',
		'cdi',
		'r8',
		'r9',
		'r10',
		'r11',
		'r12',
		'r13',
		'r14',
		'r15',
		'cip',
		'eflags',
		'gs',
		'fs',
		'es',
		'ds',
		'cs',
		'ss',
		'dr0',
		'dr1',
		'dr2',
		'dr3',
		'dr6',
		'dr7',
		'RegisteredArea',
		'x87fpu',
		'MxCsr',
		'XmmRegisters',
		'YmmRegisters'
	]
	struct__REGISTERCONTEXT._fields_ = [
		('cax', POINTER(c_ulong)),
		('ccx', POINTER(c_ulong)),
		('cdx', POINTER(c_ulong)),
		('cbx', POINTER(c_ulong)),
		('csp', POINTER(c_ulong)),
		('cbp', POINTER(c_ulong)),
		('csi', POINTER(c_ulong)),
		('cdi', POINTER(c_ulong)),
		('r8', POINTER(c_ulong)),
		('r9', POINTER(c_ulong)),
		('r10', POINTER(c_ulong)),
		('r11', POINTER(c_ulong)),
		('r12', POINTER(c_ulong)),
		('r13', POINTER(c_ulong)),
		('r14', POINTER(c_ulong)),
		('r15', POINTER(c_ulong)),
		('cip', POINTER(c_ulong)),
		('eflags', POINTER(c_ulong)),
		('gs', c_ushort),
		('fs', c_ushort),
		('es', c_ushort),
		('ds', c_ushort),
		('cs', c_ushort),
		('ss', c_ushort),
		('dr0', POINTER(c_ulong)),
		('dr1', POINTER(c_ulong)),
		('dr2', POINTER(c_ulong)),
		('dr3', POINTER(c_ulong)),
		('dr6', POINTER(c_ulong)),
		('dr7', POINTER(c_ulong)),
		('RegisteredArea', c_char * 80),
		('x87fpu', X87FPU),
		('MxCsr', DWORD),
		('XmmRegisters', XMMREGISTER * 16),
		('YmmRegisters', YMMREGISTER * 16)
	]
REGISTERCONTEXT = struct__REGISTERCONTEXT

# REGDUMP struct

class struct__REGDUMP(Structure):
    pass

struct__REGDUMP.__slots__ = [
    'regcontext',
    'flags',
	'x87FPURegisters',
	'mmx',
	'MxCsrFields',
	'x87StatusWordFields',
	'x87ControlWordFields',
]
struct__REGDUMP._fields_ = [
    ('regcontext', REGISTERCONTEXT),
	('flags', FLAGS),
	('x87FPURegisters', X87FPUREGISTER * 8),
	('mmx', c_ulonglong * 8),
	('MxCsrFields', MXCSRFIELDS),
	('x87StatusWordFields', X87STATUSWORDFIELDS),
    ('x87ControlWordFields', X87CONTROLWORDFIELDS)
]

REGDUMP = struct__REGDUMP

# DbgGetRegDump function

for _lib in _libs.itervalues():
    if not hasattr(_lib, 'DbgGetRegDump'):
        continue
    DbgGetRegDump = _lib.DbgGetRegDump
    DbgGetRegDump.argtypes = [POINTER(REGDUMP)]
    DbgGetRegDump.restype = c_bool
    break