'''Wrapper for _plugins.h

Generated with:
E:\x64dbg_pythonloader\ctypesgen\ctypesgen.py -o bin\data\plugins\pybase\x64dbg_struct.py pluginsdk\_plugins.h

Do not modify this file.
'''

__docformat__ =  'restructuredtext'

# Begin preamble

import ctypes, os, sys
from ctypes import *

_int_types = (c_int16, c_int32)
if hasattr(ctypes, 'c_int64'):
    # Some builds of ctypes apparently do not have c_int64
    # defined; it's a pretty good bet that these builds do not
    # have 64-bit pointers.
    _int_types += (c_int64,)
for t in _int_types:
    if sizeof(t) == sizeof(c_size_t):
        c_ptrdiff_t = t
del t
del _int_types

class c_void(Structure):
    # c_void_p is a buggy return type, converting to int, so
    # POINTER(None) == c_void_p is actually written as
    # POINTER(c_void), so it can be treated as a real pointer.
    _fields_ = [('dummy', c_int)]

def POINTER(obj):
    p = ctypes.POINTER(obj)

    # Convert None to a real NULL pointer to work around bugs
    # in how ctypes handles None on 64-bit platforms
    if not isinstance(p.from_param, classmethod):
        def from_param(cls, x):
            if x is None:
                return cls()
            else:
                return x
        p.from_param = classmethod(from_param)

    return p

class UserString:
    def __init__(self, seq):
        if isinstance(seq, basestring):
            self.data = seq
        elif isinstance(seq, UserString):
            self.data = seq.data[:]
        else:
            self.data = str(seq)
    def __str__(self): return str(self.data)
    def __repr__(self): return repr(self.data)
    def __int__(self): return int(self.data)
    def __long__(self): return long(self.data)
    def __float__(self): return float(self.data)
    def __complex__(self): return complex(self.data)
    def __hash__(self): return hash(self.data)

    def __cmp__(self, string):
        if isinstance(string, UserString):
            return cmp(self.data, string.data)
        else:
            return cmp(self.data, string)
    def __contains__(self, char):
        return char in self.data

    def __len__(self): return len(self.data)
    def __getitem__(self, index): return self.__class__(self.data[index])
    def __getslice__(self, start, end):
        start = max(start, 0); end = max(end, 0)
        return self.__class__(self.data[start:end])

    def __add__(self, other):
        if isinstance(other, UserString):
            return self.__class__(self.data + other.data)
        elif isinstance(other, basestring):
            return self.__class__(self.data + other)
        else:
            return self.__class__(self.data + str(other))
    def __radd__(self, other):
        if isinstance(other, basestring):
            return self.__class__(other + self.data)
        else:
            return self.__class__(str(other) + self.data)
    def __mul__(self, n):
        return self.__class__(self.data*n)
    __rmul__ = __mul__
    def __mod__(self, args):
        return self.__class__(self.data % args)

    # the following methods are defined in alphabetical order:
    def capitalize(self): return self.__class__(self.data.capitalize())
    def center(self, width, *args):
        return self.__class__(self.data.center(width, *args))
    def count(self, sub, start=0, end=sys.maxint):
        return self.data.count(sub, start, end)
    def decode(self, encoding=None, errors=None): # XXX improve this?
        if encoding:
            if errors:
                return self.__class__(self.data.decode(encoding, errors))
            else:
                return self.__class__(self.data.decode(encoding))
        else:
            return self.__class__(self.data.decode())
    def encode(self, encoding=None, errors=None): # XXX improve this?
        if encoding:
            if errors:
                return self.__class__(self.data.encode(encoding, errors))
            else:
                return self.__class__(self.data.encode(encoding))
        else:
            return self.__class__(self.data.encode())
    def endswith(self, suffix, start=0, end=sys.maxint):
        return self.data.endswith(suffix, start, end)
    def expandtabs(self, tabsize=8):
        return self.__class__(self.data.expandtabs(tabsize))
    def find(self, sub, start=0, end=sys.maxint):
        return self.data.find(sub, start, end)
    def index(self, sub, start=0, end=sys.maxint):
        return self.data.index(sub, start, end)
    def isalpha(self): return self.data.isalpha()
    def isalnum(self): return self.data.isalnum()
    def isdecimal(self): return self.data.isdecimal()
    def isdigit(self): return self.data.isdigit()
    def islower(self): return self.data.islower()
    def isnumeric(self): return self.data.isnumeric()
    def isspace(self): return self.data.isspace()
    def istitle(self): return self.data.istitle()
    def isupper(self): return self.data.isupper()
    def join(self, seq): return self.data.join(seq)
    def ljust(self, width, *args):
        return self.__class__(self.data.ljust(width, *args))
    def lower(self): return self.__class__(self.data.lower())
    def lstrip(self, chars=None): return self.__class__(self.data.lstrip(chars))
    def partition(self, sep):
        return self.data.partition(sep)
    def replace(self, old, new, maxsplit=-1):
        return self.__class__(self.data.replace(old, new, maxsplit))
    def rfind(self, sub, start=0, end=sys.maxint):
        return self.data.rfind(sub, start, end)
    def rindex(self, sub, start=0, end=sys.maxint):
        return self.data.rindex(sub, start, end)
    def rjust(self, width, *args):
        return self.__class__(self.data.rjust(width, *args))
    def rpartition(self, sep):
        return self.data.rpartition(sep)
    def rstrip(self, chars=None): return self.__class__(self.data.rstrip(chars))
    def split(self, sep=None, maxsplit=-1):
        return self.data.split(sep, maxsplit)
    def rsplit(self, sep=None, maxsplit=-1):
        return self.data.rsplit(sep, maxsplit)
    def splitlines(self, keepends=0): return self.data.splitlines(keepends)
    def startswith(self, prefix, start=0, end=sys.maxint):
        return self.data.startswith(prefix, start, end)
    def strip(self, chars=None): return self.__class__(self.data.strip(chars))
    def swapcase(self): return self.__class__(self.data.swapcase())
    def title(self): return self.__class__(self.data.title())
    def translate(self, *args):
        return self.__class__(self.data.translate(*args))
    def upper(self): return self.__class__(self.data.upper())
    def zfill(self, width): return self.__class__(self.data.zfill(width))

class MutableString(UserString):
    """mutable string objects

    Python strings are immutable objects.  This has the advantage, that
    strings may be used as dictionary keys.  If this property isn't needed
    and you insist on changing string values in place instead, you may cheat
    and use MutableString.

    But the purpose of this class is an educational one: to prevent
    people from inventing their own mutable string class derived
    from UserString and than forget thereby to remove (override) the
    __hash__ method inherited from UserString.  This would lead to
    errors that would be very hard to track down.

    A faster and better solution is to rewrite your program using lists."""
    def __init__(self, string=""):
        self.data = string
    def __hash__(self):
        raise TypeError("unhashable type (it is mutable)")
    def __setitem__(self, index, sub):
        if index < 0:
            index += len(self.data)
        if index < 0 or index >= len(self.data): raise IndexError
        self.data = self.data[:index] + sub + self.data[index+1:]
    def __delitem__(self, index):
        if index < 0:
            index += len(self.data)
        if index < 0 or index >= len(self.data): raise IndexError
        self.data = self.data[:index] + self.data[index+1:]
    def __setslice__(self, start, end, sub):
        start = max(start, 0); end = max(end, 0)
        if isinstance(sub, UserString):
            self.data = self.data[:start]+sub.data+self.data[end:]
        elif isinstance(sub, basestring):
            self.data = self.data[:start]+sub+self.data[end:]
        else:
            self.data =  self.data[:start]+str(sub)+self.data[end:]
    def __delslice__(self, start, end):
        start = max(start, 0); end = max(end, 0)
        self.data = self.data[:start] + self.data[end:]
    def immutable(self):
        return UserString(self.data)
    def __iadd__(self, other):
        if isinstance(other, UserString):
            self.data += other.data
        elif isinstance(other, basestring):
            self.data += other
        else:
            self.data += str(other)
        return self
    def __imul__(self, n):
        self.data *= n
        return self

class String(MutableString, Union):

    _fields_ = [('raw', POINTER(c_char)),
                ('data', c_char_p)]

    def __init__(self, obj=""):
        if isinstance(obj, (str, unicode, UserString)):
            self.data = str(obj)
        else:
            self.raw = obj

    def __len__(self):
        return self.data and len(self.data) or 0

    def from_param(cls, obj):
        # Convert None or 0
        if obj is None or obj == 0:
            return cls(POINTER(c_char)())

        # Convert from String
        elif isinstance(obj, String):
            return obj

        # Convert from str
        elif isinstance(obj, str):
            return cls(obj)

        # Convert from c_char_p
        elif isinstance(obj, c_char_p):
            return obj

        # Convert from POINTER(c_char)
        elif isinstance(obj, POINTER(c_char)):
            return obj

        # Convert from raw pointer
        elif isinstance(obj, int):
            return cls(cast(obj, POINTER(c_char)))

        # Convert from object
        else:
            return String.from_param(obj._as_parameter_)
    from_param = classmethod(from_param)

def ReturnString(obj, func=None, arguments=None):
    return String.from_param(obj)

# As of ctypes 1.0, ctypes does not support custom error-checking
# functions on callbacks, nor does it support custom datatypes on
# callbacks, so we must ensure that all callbacks return
# primitive datatypes.
#
# Non-primitive return values wrapped with UNCHECKED won't be
# typechecked, and will be converted to c_void_p.
def UNCHECKED(type):
    if (hasattr(type, "_type_") and isinstance(type._type_, str)
        and type._type_ != "P"):
        return type
    else:
        return c_void_p

# ctypes doesn't have direct support for variadic functions, so we have to write
# our own wrapper class
class _variadic_function(object):
    def __init__(self,func,restype,argtypes):
        self.func=func
        self.func.restype=restype
        self.argtypes=argtypes
    def _as_parameter_(self):
        # So we can pass this variadic function as a function pointer
        return self.func
    def __call__(self,*args):
        fixed_args=[]
        i=0
        for argtype in self.argtypes:
            # Typecheck what we can
            fixed_args.append(argtype.from_param(args[i]))
            i+=1
        return self.func(*fixed_args+list(args[i:]))

# End preamble

_libs = {}
_libdirs = []

# Begin loader

# ----------------------------------------------------------------------------
# Copyright (c) 2008 David James
# Copyright (c) 2006-2008 Alex Holkner
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of pyglet nor the names of its
#    contributors may be used to endorse or promote products
#    derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# ----------------------------------------------------------------------------

import os.path, re, sys, glob
import platform
import ctypes
import ctypes.util

def _environ_path(name):
    if name in os.environ:
        return os.environ[name].split(":")
    else:
        return []

class LibraryLoader(object):
    def __init__(self):
        self.other_dirs=[]

    def load_library(self,libname):
        """Given the name of a library, load it."""
        paths = self.getpaths(libname)

        for path in paths:
            if os.path.exists(path):
                return self.load(path)

        raise ImportError("%s not found." % libname)

    def load(self,path):
        """Given a path to a library, load it."""
        try:
            # Darwin requires dlopen to be called with mode RTLD_GLOBAL instead
            # of the default RTLD_LOCAL.  Without this, you end up with
            # libraries not being loadable, resulting in "Symbol not found"
            # errors
            if sys.platform == 'darwin':
                return ctypes.CDLL(path, ctypes.RTLD_GLOBAL)
            else:
                return ctypes.cdll.LoadLibrary(path)
        except OSError,e:
            raise ImportError(e)

    def getpaths(self,libname):
        """Return a list of paths where the library might be found."""
        if os.path.isabs(libname):
            yield libname
        else:
            # FIXME / TODO return '.' and os.path.dirname(__file__)
            for path in self.getplatformpaths(libname):
                yield path

            path = ctypes.util.find_library(libname)
            if path: yield path

    def getplatformpaths(self, libname):
        return []

# Darwin (Mac OS X)

class DarwinLibraryLoader(LibraryLoader):
    name_formats = ["lib%s.dylib", "lib%s.so", "lib%s.bundle", "%s.dylib",
                "%s.so", "%s.bundle", "%s"]

    def getplatformpaths(self,libname):
        if os.path.pathsep in libname:
            names = [libname]
        else:
            names = [format % libname for format in self.name_formats]

        for dir in self.getdirs(libname):
            for name in names:
                yield os.path.join(dir,name)

    def getdirs(self,libname):
        '''Implements the dylib search as specified in Apple documentation:

        http://developer.apple.com/documentation/DeveloperTools/Conceptual/
            DynamicLibraries/Articles/DynamicLibraryUsageGuidelines.html

        Before commencing the standard search, the method first checks
        the bundle's ``Frameworks`` directory if the application is running
        within a bundle (OS X .app).
        '''

        dyld_fallback_library_path = _environ_path("DYLD_FALLBACK_LIBRARY_PATH")
        if not dyld_fallback_library_path:
            dyld_fallback_library_path = [os.path.expanduser('~/lib'),
                                          '/usr/local/lib', '/usr/lib']

        dirs = []

        if '/' in libname:
            dirs.extend(_environ_path("DYLD_LIBRARY_PATH"))
        else:
            dirs.extend(_environ_path("LD_LIBRARY_PATH"))
            dirs.extend(_environ_path("DYLD_LIBRARY_PATH"))

        dirs.extend(self.other_dirs)
        dirs.append(".")
        dirs.append(os.path.dirname(__file__))

        if hasattr(sys, 'frozen') and sys.frozen == 'macosx_app':
            dirs.append(os.path.join(
                os.environ['RESOURCEPATH'],
                '..',
                'Frameworks'))

        dirs.extend(dyld_fallback_library_path)

        return dirs

# Posix

class PosixLibraryLoader(LibraryLoader):
    _ld_so_cache = None

    def _create_ld_so_cache(self):
        # Recreate search path followed by ld.so.  This is going to be
        # slow to build, and incorrect (ld.so uses ld.so.cache, which may
        # not be up-to-date).  Used only as fallback for distros without
        # /sbin/ldconfig.
        #
        # We assume the DT_RPATH and DT_RUNPATH binary sections are omitted.

        directories = []
        for name in ("LD_LIBRARY_PATH",
                     "SHLIB_PATH", # HPUX
                     "LIBPATH", # OS/2, AIX
                     "LIBRARY_PATH", # BE/OS
                    ):
            if name in os.environ:
                directories.extend(os.environ[name].split(os.pathsep))
        directories.extend(self.other_dirs)
        directories.append(".")
        directories.append(os.path.dirname(__file__))

        try: directories.extend([dir.strip() for dir in open('/etc/ld.so.conf')])
        except IOError: pass

        unix_lib_dirs_list = ['/lib', '/usr/lib', '/lib64', '/usr/lib64']
        if sys.platform.startswith('linux'):
            # Try and support multiarch work in Ubuntu
            # https://wiki.ubuntu.com/MultiarchSpec
            bitage = platform.architecture()[0]
            if bitage.startswith('32'):
                # Assume Intel/AMD x86 compat
                unix_lib_dirs_list += ['/lib/i386-linux-gnu', '/usr/lib/i386-linux-gnu']
            elif bitage.startswith('64'):
                # Assume Intel/AMD x86 compat
                unix_lib_dirs_list += ['/lib/x86_64-linux-gnu', '/usr/lib/x86_64-linux-gnu']
            else:
                # guess...
                unix_lib_dirs_list += glob.glob('/lib/*linux-gnu')
        directories.extend(unix_lib_dirs_list)

        cache = {}
        lib_re = re.compile(r'lib(.*)\.s[ol]')
        ext_re = re.compile(r'\.s[ol]$')
        for dir in directories:
            try:
                for path in glob.glob("%s/*.s[ol]*" % dir):
                    file = os.path.basename(path)

                    # Index by filename
                    if file not in cache:
                        cache[file] = path

                    # Index by library name
                    match = lib_re.match(file)
                    if match:
                        library = match.group(1)
                        if library not in cache:
                            cache[library] = path
            except OSError:
                pass

        self._ld_so_cache = cache

    def getplatformpaths(self, libname):
        if self._ld_so_cache is None:
            self._create_ld_so_cache()

        result = self._ld_so_cache.get(libname)
        if result: yield result

        path = ctypes.util.find_library(libname)
        if path: yield os.path.join("/lib",path)

# Windows

class _WindowsLibrary(object):
    def __init__(self, path):
        self.cdll = ctypes.cdll.LoadLibrary(path)
        self.windll = ctypes.windll.LoadLibrary(path)

    def __getattr__(self, name):
        try: return getattr(self.cdll,name)
        except AttributeError:
            try: return getattr(self.windll,name)
            except AttributeError:
                raise

class WindowsLibraryLoader(LibraryLoader):
    name_formats = ["%s.dll", "lib%s.dll", "%slib.dll"]

    def load_library(self, libname):
        try:
            result = LibraryLoader.load_library(self, libname)
        except ImportError:
            result = None
            if os.path.sep not in libname:
                for name in self.name_formats:
                    try:
                        result = getattr(ctypes.cdll, name % libname)
                        if result:
                            break
                    except WindowsError:
                        result = None
            if result is None:
                try:
                    result = getattr(ctypes.cdll, libname)
                except WindowsError:
                    result = None
            if result is None:
                raise ImportError("%s not found." % libname)
        return result

    def load(self, path):
        return _WindowsLibrary(path)

    def getplatformpaths(self, libname):
        if os.path.sep not in libname:
            for name in self.name_formats:
                dll_in_current_dir = os.path.abspath(name % libname)
                if os.path.exists(dll_in_current_dir):
                    yield dll_in_current_dir
                path = ctypes.util.find_library(name % libname)
                if path:
                    yield path

# Platform switching

# If your value of sys.platform does not appear in this dict, please contact
# the Ctypesgen maintainers.

loaderclass = {
    "darwin":   DarwinLibraryLoader,
    "cygwin":   WindowsLibraryLoader,
    "win32":    WindowsLibraryLoader
}

loader = loaderclass.get(sys.platform, PosixLibraryLoader)()

def add_library_search_dirs(other_dirs):
    loader.other_dirs = other_dirs

load_library = loader.load_library

del loaderclass

# End loader

add_library_search_dirs([])

# No libraries

# No modules

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winnt.h: 2446
class struct__EXCEPTION_RECORD(Structure):
    pass

WORD = c_ushort # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 116

DWORD = c_ulong # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 117

LPVOID = POINTER(None) # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 129

UINT = c_uint # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 135

UINT_PTR = c_uint # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\basetsd.h: 54

LONG_PTR = c_long # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\basetsd.h: 55

ULONG_PTR = c_ulong # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\basetsd.h: 56

PVOID = POINTER(None) # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winnt.h: 171

CHAR = c_char # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winnt.h: 190

LONG = c_long # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winnt.h: 192

LPSTR = POINTER(CHAR) # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winnt.h: 209

HANDLE = POINTER(None) # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winnt.h: 247

struct__EXCEPTION_RECORD.__slots__ = [
    'ExceptionCode',
    'ExceptionFlags',
    'ExceptionRecord',
    'ExceptionAddress',
    'NumberParameters',
    'ExceptionInformation',
]
struct__EXCEPTION_RECORD._fields_ = [
    ('ExceptionCode', DWORD),
    ('ExceptionFlags', DWORD),
    ('ExceptionRecord', POINTER(struct__EXCEPTION_RECORD)),
    ('ExceptionAddress', PVOID),
    ('NumberParameters', DWORD),
    ('ExceptionInformation', ULONG_PTR * 15),
]

EXCEPTION_RECORD = struct__EXCEPTION_RECORD # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winnt.h: 2453

WPARAM = UINT_PTR # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 144

LPARAM = LONG_PTR # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 145

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 168
class struct_HWND__(Structure):
    pass

struct_HWND__.__slots__ = [
    'unused',
]
struct_HWND__._fields_ = [
    ('unused', c_int),
]

HWND = POINTER(struct_HWND__) # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 168

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 252
class struct_tagPOINT(Structure):
    pass

struct_tagPOINT.__slots__ = [
    'x',
    'y',
]
struct_tagPOINT._fields_ = [
    ('x', LONG),
    ('y', LONG),
]

POINT = struct_tagPOINT # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\windef.h: 252

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 191
class struct__PROCESS_INFORMATION(Structure):
    pass

struct__PROCESS_INFORMATION.__slots__ = [
    'hProcess',
    'hThread',
    'dwProcessId',
    'dwThreadId',
]
struct__PROCESS_INFORMATION._fields_ = [
    ('hProcess', HANDLE),
    ('hThread', HANDLE),
    ('dwProcessId', DWORD),
    ('dwThreadId', DWORD),
]

PROCESS_INFORMATION = struct__PROCESS_INFORMATION # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 191

PTHREAD_START_ROUTINE = CFUNCTYPE(UNCHECKED(DWORD), LPVOID) # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 215

LPTHREAD_START_ROUTINE = PTHREAD_START_ROUTINE # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 216

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 546
class struct__EXCEPTION_DEBUG_INFO(Structure):
    pass

struct__EXCEPTION_DEBUG_INFO.__slots__ = [
    'ExceptionRecord',
    'dwFirstChance',
]
struct__EXCEPTION_DEBUG_INFO._fields_ = [
    ('ExceptionRecord', EXCEPTION_RECORD),
    ('dwFirstChance', DWORD),
]

EXCEPTION_DEBUG_INFO = struct__EXCEPTION_DEBUG_INFO # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 546

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 552
class struct__CREATE_THREAD_DEBUG_INFO(Structure):
    pass

struct__CREATE_THREAD_DEBUG_INFO.__slots__ = [
    'hThread',
    'lpThreadLocalBase',
    'lpStartAddress',
]
struct__CREATE_THREAD_DEBUG_INFO._fields_ = [
    ('hThread', HANDLE),
    ('lpThreadLocalBase', LPVOID),
    ('lpStartAddress', LPTHREAD_START_ROUTINE),
]

CREATE_THREAD_DEBUG_INFO = struct__CREATE_THREAD_DEBUG_INFO # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 552

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 565
class struct__CREATE_PROCESS_DEBUG_INFO(Structure):
    pass

struct__CREATE_PROCESS_DEBUG_INFO.__slots__ = [
    'hFile',
    'hProcess',
    'hThread',
    'lpBaseOfImage',
    'dwDebugInfoFileOffset',
    'nDebugInfoSize',
    'lpThreadLocalBase',
    'lpStartAddress',
    'lpImageName',
    'fUnicode',
]
struct__CREATE_PROCESS_DEBUG_INFO._fields_ = [
    ('hFile', HANDLE),
    ('hProcess', HANDLE),
    ('hThread', HANDLE),
    ('lpBaseOfImage', LPVOID),
    ('dwDebugInfoFileOffset', DWORD),
    ('nDebugInfoSize', DWORD),
    ('lpThreadLocalBase', LPVOID),
    ('lpStartAddress', LPTHREAD_START_ROUTINE),
    ('lpImageName', LPVOID),
    ('fUnicode', WORD),
]

CREATE_PROCESS_DEBUG_INFO = struct__CREATE_PROCESS_DEBUG_INFO # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 565

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 569
class struct__EXIT_THREAD_DEBUG_INFO(Structure):
    pass

struct__EXIT_THREAD_DEBUG_INFO.__slots__ = [
    'dwExitCode',
]
struct__EXIT_THREAD_DEBUG_INFO._fields_ = [
    ('dwExitCode', DWORD),
]

EXIT_THREAD_DEBUG_INFO = struct__EXIT_THREAD_DEBUG_INFO # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 569

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 573
class struct__EXIT_PROCESS_DEBUG_INFO(Structure):
    pass

struct__EXIT_PROCESS_DEBUG_INFO.__slots__ = [
    'dwExitCode',
]
struct__EXIT_PROCESS_DEBUG_INFO._fields_ = [
    ('dwExitCode', DWORD),
]

EXIT_PROCESS_DEBUG_INFO = struct__EXIT_PROCESS_DEBUG_INFO # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 573

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 582
class struct__LOAD_DLL_DEBUG_INFO(Structure):
    pass

struct__LOAD_DLL_DEBUG_INFO.__slots__ = [
    'hFile',
    'lpBaseOfDll',
    'dwDebugInfoFileOffset',
    'nDebugInfoSize',
    'lpImageName',
    'fUnicode',
]
struct__LOAD_DLL_DEBUG_INFO._fields_ = [
    ('hFile', HANDLE),
    ('lpBaseOfDll', LPVOID),
    ('dwDebugInfoFileOffset', DWORD),
    ('nDebugInfoSize', DWORD),
    ('lpImageName', LPVOID),
    ('fUnicode', WORD),
]

LOAD_DLL_DEBUG_INFO = struct__LOAD_DLL_DEBUG_INFO # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 582

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 586
class struct__UNLOAD_DLL_DEBUG_INFO(Structure):
    pass

struct__UNLOAD_DLL_DEBUG_INFO.__slots__ = [
    'lpBaseOfDll',
]
struct__UNLOAD_DLL_DEBUG_INFO._fields_ = [
    ('lpBaseOfDll', LPVOID),
]

UNLOAD_DLL_DEBUG_INFO = struct__UNLOAD_DLL_DEBUG_INFO # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 586

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 592
class struct__OUTPUT_DEBUG_STRING_INFO(Structure):
    pass

struct__OUTPUT_DEBUG_STRING_INFO.__slots__ = [
    'lpDebugStringData',
    'fUnicode',
    'nDebugStringLength',
]
struct__OUTPUT_DEBUG_STRING_INFO._fields_ = [
    ('lpDebugStringData', LPSTR),
    ('fUnicode', WORD),
    ('nDebugStringLength', WORD),
]

OUTPUT_DEBUG_STRING_INFO = struct__OUTPUT_DEBUG_STRING_INFO # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 592

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 597
class struct__RIP_INFO(Structure):
    pass

struct__RIP_INFO.__slots__ = [
    'dwError',
    'dwType',
]
struct__RIP_INFO._fields_ = [
    ('dwError', DWORD),
    ('dwType', DWORD),
]

RIP_INFO = struct__RIP_INFO # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 597

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 603
class union_anon_84(Union):
    pass

union_anon_84.__slots__ = [
    'Exception',
    'CreateThread',
    'CreateProcessInfo',
    'ExitThread',
    'ExitProcess',
    'LoadDll',
    'UnloadDll',
    'DebugString',
    'RipInfo',
]
union_anon_84._fields_ = [
    ('Exception', EXCEPTION_DEBUG_INFO),
    ('CreateThread', CREATE_THREAD_DEBUG_INFO),
    ('CreateProcessInfo', CREATE_PROCESS_DEBUG_INFO),
    ('ExitThread', EXIT_THREAD_DEBUG_INFO),
    ('ExitProcess', EXIT_PROCESS_DEBUG_INFO),
    ('LoadDll', LOAD_DLL_DEBUG_INFO),
    ('UnloadDll', UNLOAD_DLL_DEBUG_INFO),
    ('DebugString', OUTPUT_DEBUG_STRING_INFO),
    ('RipInfo', RIP_INFO),
]

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 614
class struct__DEBUG_EVENT(Structure):
    pass

struct__DEBUG_EVENT.__slots__ = [
    'dwDebugEventCode',
    'dwProcessId',
    'dwThreadId',
    'u',
]
struct__DEBUG_EVENT._fields_ = [
    ('dwDebugEventCode', DWORD),
    ('dwProcessId', DWORD),
    ('dwThreadId', DWORD),
    ('u', union_anon_84),
]

DEBUG_EVENT = struct__DEBUG_EVENT # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winbase.h: 614

# c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winuser.h: 834
class struct_tagMSG(Structure):
    pass

struct_tagMSG.__slots__ = [
    'hwnd',
    'message',
    'wParam',
    'lParam',
    'time',
    'pt',
]
struct_tagMSG._fields_ = [
    ('hwnd', HWND),
    ('message', UINT),
    ('wParam', WPARAM),
    ('lParam', LPARAM),
    ('time', DWORD),
    ('pt', POINT),
]

MSG = struct_tagMSG # c:\\qt\\tools\\mingw48_32\\i686-w64-mingw32\\include\\winuser.h: 834

duint = c_ulong # E:\\x64dbg_pythonloader\\pluginsdk\\bridgemain.h: 21

enum_anon_569 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\bridgemain.h: 106

BPXTYPE = enum_anon_569 # E:\\x64dbg_pythonloader\\pluginsdk\\bridgemain.h: 106

# E:\\x64dbg_pythonloader\\pluginsdk\\bridgemain.h: 305
class struct_anon_583(Structure):
    pass

struct_anon_583.__slots__ = [
    'type',
    'addr',
    'enabled',
    'singleshoot',
    'active',
    'name',
    'mod',
    'slot',
]
struct_anon_583._fields_ = [
    ('type', BPXTYPE),
    ('addr', duint),
    ('enabled', c_bool),
    ('singleshoot', c_bool),
    ('active', c_bool),
    ('name', c_char * 256),
    ('mod', c_char * 256),
    ('slot', c_ushort),
]

BRIDGEBP = struct_anon_583 # E:\\x64dbg_pythonloader\\pluginsdk\\bridgemain.h: 305

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 37
class struct_anon_617(Structure):
    pass

struct_anon_617.__slots__ = [
    'pluginHandle',
    'sdkVersion',
    'pluginVersion',
    'pluginName',
]
struct_anon_617._fields_ = [
    ('pluginHandle', c_int),
    ('sdkVersion', c_int),
    ('pluginVersion', c_int),
    ('pluginName', c_char * 256),
]

PLUG_INITSTRUCT = struct_anon_617 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 37

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 44
class struct_anon_618(Structure):
    pass

struct_anon_618.__slots__ = [
    'hwndDlg',
    'hMenu',
]
struct_anon_618._fields_ = [
    ('hwndDlg', HWND),
    ('hMenu', c_int),
]

PLUG_SETUPSTRUCT = struct_anon_618 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 44

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 50
class struct_anon_619(Structure):
    pass

struct_anon_619.__slots__ = [
    'szFileName',
]
struct_anon_619._fields_ = [
    ('szFileName', String),
]

PLUG_CB_INITDEBUG = struct_anon_619 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 50

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 55
class struct_anon_620(Structure):
    pass

struct_anon_620.__slots__ = [
    'reserved',
]
struct_anon_620._fields_ = [
    ('reserved', POINTER(None)),
]

PLUG_CB_STOPDEBUG = struct_anon_620 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 55

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 68
class struct_anon_622(Structure):
    pass

struct_anon_622.__slots__ = [
    'ExitProcess',
]
struct_anon_622._fields_ = [
    ('ExitProcess', POINTER(EXIT_PROCESS_DEBUG_INFO)),
]

PLUG_CB_EXITPROCESS = struct_anon_622 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 68

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 74
class struct_anon_623(Structure):
    pass

struct_anon_623.__slots__ = [
    'CreateThread',
    'dwThreadId',
]
struct_anon_623._fields_ = [
    ('CreateThread', POINTER(CREATE_THREAD_DEBUG_INFO)),
    ('dwThreadId', DWORD),
]

PLUG_CB_CREATETHREAD = struct_anon_623 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 74

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 80
class struct_anon_624(Structure):
    pass

struct_anon_624.__slots__ = [
    'ExitThread',
    'dwThreadId',
]
struct_anon_624._fields_ = [
    ('ExitThread', POINTER(EXIT_THREAD_DEBUG_INFO)),
    ('dwThreadId', DWORD),
]

PLUG_CB_EXITTHREAD = struct_anon_624 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 80

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 85
class struct_anon_625(Structure):
    pass

struct_anon_625.__slots__ = [
    'reserved',
]
struct_anon_625._fields_ = [
    ('reserved', POINTER(None)),
]

PLUG_CB_SYSTEMBREAKPOINT = struct_anon_625 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 85

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 97
class struct_anon_627(Structure):
    pass

struct_anon_627.__slots__ = [
    'UnloadDll',
]
struct_anon_627._fields_ = [
    ('UnloadDll', POINTER(UNLOAD_DLL_DEBUG_INFO)),
]

PLUG_CB_UNLOADDLL = struct_anon_627 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 97

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 102
class struct_anon_628(Structure):
    pass

struct_anon_628.__slots__ = [
    'DebugString',
]
struct_anon_628._fields_ = [
    ('DebugString', POINTER(OUTPUT_DEBUG_STRING_INFO)),
]

PLUG_CB_OUTPUTDEBUGSTRING = struct_anon_628 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 102

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 107
class struct_anon_629(Structure):
    pass

struct_anon_629.__slots__ = [
    'Exception',
]
struct_anon_629._fields_ = [
    ('Exception', POINTER(EXCEPTION_DEBUG_INFO)),
]

PLUG_CB_EXCEPTION = struct_anon_629 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 107

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 112
class struct_anon_630(Structure):
    pass

struct_anon_630.__slots__ = [
    'breakpoint',
]
struct_anon_630._fields_ = [
    ('breakpoint', POINTER(BRIDGEBP)),
]

PLUG_CB_BREAKPOINT = struct_anon_630 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 112

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 117
class struct_anon_631(Structure):
    pass

struct_anon_631.__slots__ = [
    'reserved',
]
struct_anon_631._fields_ = [
    ('reserved', POINTER(None)),
]

PLUG_CB_PAUSEDEBUG = struct_anon_631 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 117

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 122
class struct_anon_632(Structure):
    pass

struct_anon_632.__slots__ = [
    'reserved',
]
struct_anon_632._fields_ = [
    ('reserved', POINTER(None)),
]

PLUG_CB_RESUMEDEBUG = struct_anon_632 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 122

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 127
class struct_anon_633(Structure):
    pass

struct_anon_633.__slots__ = [
    'reserved',
]
struct_anon_633._fields_ = [
    ('reserved', POINTER(None)),
]

PLUG_CB_STEPPED = struct_anon_633 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 127

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 132
class struct_anon_634(Structure):
    pass

struct_anon_634.__slots__ = [
    'dwProcessId',
]
struct_anon_634._fields_ = [
    ('dwProcessId', DWORD),
]

PLUG_CB_ATTACH = struct_anon_634 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 132

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 137
class struct_anon_635(Structure):
    pass

struct_anon_635.__slots__ = [
    'fdProcessInfo',
]
struct_anon_635._fields_ = [
    ('fdProcessInfo', POINTER(PROCESS_INFORMATION)),
]

PLUG_CB_DETACH = struct_anon_635 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 137

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 142
class struct_anon_636(Structure):
    pass

struct_anon_636.__slots__ = [
    'DebugEvent',
]
struct_anon_636._fields_ = [
    ('DebugEvent', POINTER(DEBUG_EVENT)),
]

PLUG_CB_DEBUGEVENT = struct_anon_636 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 142

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 147
class struct_anon_637(Structure):
    pass

struct_anon_637.__slots__ = [
    'hEntry',
]
struct_anon_637._fields_ = [
    ('hEntry', c_int),
]

PLUG_CB_MENUENTRY = struct_anon_637 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 147

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 154
class struct_anon_638(Structure):
    pass

struct_anon_638.__slots__ = [
    'message',
    'result',
    'retval',
]
struct_anon_638._fields_ = [
    ('message', POINTER(MSG)),
    ('result', POINTER(c_long)),
    ('retval', c_bool),
]

PLUG_CB_WINEVENT = struct_anon_638 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 154

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 160
class struct_anon_639(Structure):
    pass

struct_anon_639.__slots__ = [
    'message',
    'retval',
]
struct_anon_639._fields_ = [
    ('message', POINTER(MSG)),
    ('retval', c_bool),
]

PLUG_CB_WINEVENTGLOBAL = struct_anon_639 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 160

enum_anon_640 = c_int # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_INITDEBUG = 0 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_STOPDEBUG = (CB_INITDEBUG + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_CREATEPROCESS = (CB_STOPDEBUG + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_EXITPROCESS = (CB_CREATEPROCESS + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_CREATETHREAD = (CB_EXITPROCESS + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_EXITTHREAD = (CB_CREATETHREAD + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_SYSTEMBREAKPOINT = (CB_EXITTHREAD + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_LOADDLL = (CB_SYSTEMBREAKPOINT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_UNLOADDLL = (CB_LOADDLL + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_OUTPUTDEBUGSTRING = (CB_UNLOADDLL + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_EXCEPTION = (CB_OUTPUTDEBUGSTRING + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_BREAKPOINT = (CB_EXCEPTION + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_PAUSEDEBUG = (CB_BREAKPOINT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_RESUMEDEBUG = (CB_PAUSEDEBUG + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_STEPPED = (CB_RESUMEDEBUG + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_ATTACH = (CB_STEPPED + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_DETACH = (CB_ATTACH + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_DEBUGEVENT = (CB_DETACH + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_MENUENTRY = (CB_DEBUGEVENT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_WINEVENT = (CB_MENUENTRY + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CB_WINEVENTGLOBAL = (CB_WINEVENT + 1) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CBTYPE = enum_anon_640 # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 186

CBPLUGIN = CFUNCTYPE(UNCHECKED(None), CBTYPE, POINTER(None)) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 189

CBPLUGINCOMMAND = CFUNCTYPE(UNCHECKED(c_bool), c_int, POINTER(POINTER(c_char))) # E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 190

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 198
for _lib in _libs.itervalues():
    if not hasattr(_lib, '_plugin_registercallback'):
        continue
    _plugin_registercallback = _lib._plugin_registercallback
    _plugin_registercallback.argtypes = [c_int, CBTYPE, CBPLUGIN]
    _plugin_registercallback.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 199
for _lib in _libs.itervalues():
    if not hasattr(_lib, '_plugin_unregistercallback'):
        continue
    _plugin_unregistercallback = _lib._plugin_unregistercallback
    _plugin_unregistercallback.argtypes = [c_int, CBTYPE]
    _plugin_unregistercallback.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 200
for _lib in _libs.itervalues():
    if not hasattr(_lib, '_plugin_registercommand'):
        continue
    _plugin_registercommand = _lib._plugin_registercommand
    _plugin_registercommand.argtypes = [c_int, String, CBPLUGINCOMMAND, c_bool]
    _plugin_registercommand.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 201
for _lib in _libs.itervalues():
    if not hasattr(_lib, '_plugin_unregistercommand'):
        continue
    _plugin_unregistercommand = _lib._plugin_unregistercommand
    _plugin_unregistercommand.argtypes = [c_int, String]
    _plugin_unregistercommand.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 202
for _lib in _libs.values():
    if hasattr(_lib, '_plugin_logprintf'):
        _func = _lib._plugin_logprintf
        _restype = None
        _argtypes = [String]
        _plugin_logprintf = _variadic_function(_func,_restype,_argtypes)

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 203
for _lib in _libs.itervalues():
    if not hasattr(_lib, '_plugin_logputs'):
        continue
    _plugin_logputs = _lib._plugin_logputs
    _plugin_logputs.argtypes = [String]
    _plugin_logputs.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 204
for _lib in _libs.itervalues():
    if not hasattr(_lib, '_plugin_debugpause'):
        continue
    _plugin_debugpause = _lib._plugin_debugpause
    _plugin_debugpause.argtypes = []
    _plugin_debugpause.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 205
for _lib in _libs.itervalues():
    if not hasattr(_lib, '_plugin_debugskipexceptions'):
        continue
    _plugin_debugskipexceptions = _lib._plugin_debugskipexceptions
    _plugin_debugskipexceptions.argtypes = [c_bool]
    _plugin_debugskipexceptions.restype = None
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 206
for _lib in _libs.itervalues():
    if not hasattr(_lib, '_plugin_menuadd'):
        continue
    _plugin_menuadd = _lib._plugin_menuadd
    _plugin_menuadd.argtypes = [c_int, String]
    _plugin_menuadd.restype = c_int
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 207
for _lib in _libs.itervalues():
    if not hasattr(_lib, '_plugin_menuaddentry'):
        continue
    _plugin_menuaddentry = _lib._plugin_menuaddentry
    _plugin_menuaddentry.argtypes = [c_int, c_int, String]
    _plugin_menuaddentry.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 208
for _lib in _libs.itervalues():
    if not hasattr(_lib, '_plugin_menuaddseparator'):
        continue
    _plugin_menuaddseparator = _lib._plugin_menuaddseparator
    _plugin_menuaddseparator.argtypes = [c_int]
    _plugin_menuaddseparator.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 209
for _lib in _libs.itervalues():
    if not hasattr(_lib, '_plugin_menuclear'):
        continue
    _plugin_menuclear = _lib._plugin_menuclear
    _plugin_menuclear.argtypes = [c_int]
    _plugin_menuclear.restype = c_bool
    break

# E:\\x64dbg_pythonloader\\pluginsdk\\_plugins.h: 26
try:
    PLUG_SDKVERSION = 1
except:
    pass

# No inserted files

##
## Extra definitions from x64bd_struct.py.extra
##

# MAX_PATH
MAX_PATH = 260

# DWORD64
DWORD64 = c_ulonglong

# GUID struct

class struct__GUID(Structure):
    pass

struct__GUID.__slots__ = [
    'Data1',
    'Data2',
	'Data3',
    'Data4'
]
struct__GUID._fields_ = [
    ('Data1', c_ulong),
    ('Data2', c_ushort),
	('Data3', c_ushort),
    ('Data4', c_char * 8)
]

GUID = struct__GUID

# IMAGEHLP_MODULE64 struct

class struct__IMAGEHLP_MODULE64(Structure):
    pass

struct__IMAGEHLP_MODULE64.__slots__ = [
    'SizeOfStruct',
    'BaseOfImage',
	'ImageSize',
	'TimeDateStamp',
	'CheckSum',
	'NumSyms',
	'SymType',
	'ModuleName',
	'ImageName',
	'LoadedImageName',
	'LoadedPdbName',
	'CVSig',
	'CVData',
	'PdbSig',
	'PdbSig70',
	'PdbAge',
	'PdbUnmatched',
	'DbgUnmatched',
	'LineNumbers',
	'GlobalSymbols',
	'TypeInfo',
	'SourceIndexed',
    'Publics'
]
struct__IMAGEHLP_MODULE64._fields_ = [
    ('SizeOfStruct', DWORD),
    ('BaseOfImage', DWORD64),
	('ImageSize', DWORD),
	('TimeDateStamp', DWORD),
	('CheckSum', DWORD),
	('NumSyms', DWORD),
	('SymType', c_int),
	('ModuleName', c_char * 32),
	('ImageName', c_char * 256),
	('LoadedImageName', c_char * 256),
	('LoadedPdbName', c_char * 256),
	('CVSig', DWORD),
	('CVData', c_char * MAX_PATH * 3),
	('PdbSig', DWORD),
	('PdbSig70', GUID),
	('PdbAge', DWORD),
	('PdbUnmatched', c_bool),
	('DbgUnmatched', c_bool),
	('LineNumbers', c_bool),
	('GlobalSymbols', c_bool),
	('TypeInfo', c_bool),
	('SourceIndexed', c_bool),
	('Publics', c_bool)
]

IMAGEHLP_MODULE64 = struct__IMAGEHLP_MODULE64


# PLUG_CB_LOADDLL struct

class struct__PLUG_CB_LOADDLL(Structure):
    pass

struct__PLUG_CB_LOADDLL.__slots__ = [
    'LoadDll',
    'modInfo',
    'modname'
]
struct__PLUG_CB_LOADDLL._fields_ = [
    ('LoadDll', POINTER(LOAD_DLL_DEBUG_INFO)),
    ('modInfo', POINTER(IMAGEHLP_MODULE64)),
    ('modname', c_char_p)
]

PLUG_CB_LOADDLL = struct__PLUG_CB_LOADDLL

# PLUG_CB_CREATEPROCESS struct

class struct__PLUG_CB_CREATEPROCESS(Structure):
    pass

struct__PLUG_CB_CREATEPROCESS.__slots__ = [
    'CreateProcessInfo',
    'modInfo',
    'DebugFileName',
	'fdProcessInfo'
]
struct__PLUG_CB_CREATEPROCESS._fields_ = [
    ('CreateProcessInfo', POINTER(CREATE_PROCESS_DEBUG_INFO)),
    ('modInfo', POINTER(IMAGEHLP_MODULE64)),
    ('DebugFileName', c_char_p),
	('fdProcessInfo', POINTER(PROCESS_INFORMATION))
]

PLUG_CB_CREATEPROCESS = struct__PLUG_CB_CREATEPROCESS
