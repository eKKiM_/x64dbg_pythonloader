#include "../pluginsdk/_plugins.h"
#include "../python_extra.h"

#include "_plugins_wrapper.h"

#include <memory>

typedef struct
{
    PyObject* module;
	bool debugOnly;
    char command[MAX_CMD_SIZE];
} REGISTERED_COMMAND;

extern int CurrentPluginHandle;

std::vector<REGISTERED_COMMAND> RegisteredCommands;

void put_log(const char *text)
{
	_plugin_logputs(text);
}

bool register_command(PyObject *module, const char *functionName, bool debugOnly)
{
	REGISTERED_COMMAND cmd;
	
	cmd.module = module;
	cmd.debugOnly = debugOnly;
	
	strncpy(cmd.command, functionName, MAX_CMD_SIZE);

	bool retVal = _plugin_registercommand(CurrentPluginHandle, cmd.command, command_callback, debugOnly);

	if (retVal)
		RegisteredCommands.push_back(cmd);

	return retVal;
}

bool unregister_command(PyObject *module, const char *functionName)
{
	for (auto it = RegisteredCommands.begin(); it != RegisteredCommands.end(); it++) {
		if (it->module == module && strcmp(it->command, functionName) == 0)
		{
			RegisteredCommands.erase(it);
			_plugin_unregistercommand(CurrentPluginHandle, functionName);
			return true;
		}
	}
	return false;
}

static bool command_callback(int argc, char* argv[])
{
	for (auto it = RegisteredCommands.begin(); it != RegisteredCommands.end(); it++) 
	{
		if (strncmp(it->command, it->command, strlen(it->command)) == 0)
		{
			if (!it->debugOnly || DbgIsDebugging())
			{

				PyObject *pFunc = PyObject_GetAttrString(it->module, it->command);

				if (pFunc && PyCallable_Check(pFunc)) 
				{
					PyListObject *list = (PyListObject *) Py_BuildValue("[]");
					for (int i = 0; i < argc; i++) {
						PyList_Append((PyObject *)list, Py_BuildValue("s", (char*) argv[i]));
					}
					
					PyObject *args = PyTuple_New(1);
					PyTuple_SetItem(args, 0, (PyObject *) list);

					PyObject *pValue = PyObject_CallObject(pFunc, args);
					Py_XDECREF(args);

					if (pValue != NULL) {
						Py_DECREF(pValue);
					}
					if (PyErr_Occurred())
					{
						PyObject *ptype, *pvalue, *ptraceback;
						PyErr_Fetch(&ptype, &pvalue, &ptraceback);

						if (ptype && pvalue && ptraceback)
						{
							char *pStrErrorMessage = PyString_AsString(pvalue);

							PyTracebackObject* traceback = (PyTracebackObject *) ptraceback;
							PyFrameObject *frame = (PyFrameObject *) traceback->tb_frame;

							while (NULL != frame) {
								int line = frame->f_lineno;
								const char *filename = PyString_AsString(frame->f_code->co_filename);
								const char *funcname = PyString_AsString(frame->f_code->co_name);
								_plugin_logprintf("[PythonLoader] ERROR - %s(%d): %s - %s\n", filename, line, funcname, pStrErrorMessage);
								frame = frame->f_back;
							}
						}

						Py_XDECREF(ptype);
						Py_XDECREF(pvalue);
						Py_XDECREF(ptraceback);
					}
				}
				Py_XDECREF(pFunc);
			}
		}
	}
    return true;
}

bool is_32_bit()
{
	return !is_64_bit();
}

bool is_64_bit()
{
#ifdef _WIN64
	return true;
#else
	return false;
#endif
}