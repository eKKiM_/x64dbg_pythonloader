#define PY_SSIZE_T_CLEAN
#include <Python.h>

PyMODINIT_FUNC
#if defined(__GNUC__) && __GNUC__ >= 4
__attribute__ ((visibility("default")))
#endif
initx64dbg_api(void);
