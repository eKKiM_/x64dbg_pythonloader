#include <vector>
#include <Python.h>

#define MAX_CMD_SIZE 128

void put_log(const char *text);

bool register_command(PyObject *module, const char *functionName, bool debugOnly);
bool unregister_command(PyObject *module, const char *functionName);

bool is_64_bit();
bool is_32_bit();

static bool command_callback(int argc, char* argv[]);
