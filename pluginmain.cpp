#include "pluginmain.h"
#include "PythonLoader.h"

#include <Python.h>

#define plugin_name "PythonLoader"
#define plugin_version 0

int pluginHandle;
HWND hwndDlg;
int hMenu;

DLL_EXPORT bool pluginit(PLUG_INITSTRUCT* initStruct)
{
    initStruct->pluginVersion = plugin_version;
    initStruct->sdkVersion = PLUG_SDKVERSION;
    strcpy(initStruct->pluginName, plugin_name);
    pluginHandle = initStruct->pluginHandle;
    pythonLoaderInit(initStruct);
    return true;
}

DLL_EXPORT bool plugstop()
{
	/* PythonLoader unloading is unsupported (atm) */
	return false;
}

DLL_EXPORT void plugsetup(PLUG_SETUPSTRUCT* setupStruct)
{
    hwndDlg = setupStruct->hwndDlg;
    hMenu = setupStruct->hMenu;
    pythonLoaderSetup();
}

extern "C" DLL_EXPORT BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    return TRUE;
}
