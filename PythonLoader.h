#include "pluginsdk/_plugins.h"

#include <Python.h>
#include <vector>

void pythonLoaderInit(PLUG_INITSTRUCT* initStruct);
void pythonLoaderStop(); /* UNUSED */
void pythonLoaderSetup(); /* TODO */

void pythonEventExecute(const char *eventName, void *info);
bool pythonLogError();

void LoadDirectory(const char *dir, bool runInit);