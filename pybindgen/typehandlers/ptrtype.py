# docstrings not neede here (the type handler interfaces are fully
# documented in base.py) pylint: disable-msg=C0111

from base import ReturnValue, Parameter, PointerParameter, PointerReturnValue, \
     ReverseWrapperBase, ForwardWrapperBase, TypeConfigurationError, NotSupportedError

class PtrParam(Parameter):

    DIRECTIONS = [Parameter.DIRECTION_IN]
    CTYPES = ['unsigned char *', 'byte *']

    def get_ctype_without_ref(self):
        return str(self.type_traits.ctype_no_const)

    def convert_c_to_python(self, wrapper):
        assert isinstance(wrapper, ReverseWrapperBase)
        wrapper.build_params.add_parameter('k', [self.value])

    def convert_python_to_c(self, wrapper):
        assert isinstance(wrapper, ForwardWrapperBase)
        name = wrapper.declarations.declare_variable(self.get_ctype_without_ref(), self.name, self.default_value)
        wrapper.parse_params.add_parameter('k', ['&'+name], self.name, optional=bool(self.default_value))
        wrapper.call_params.append(name)
		
class PtrReturn(ReturnValue):

    CTYPES = ['unsigned char *', 'byte *']

    def get_c_error_return(self):
        return "return 0;"
    
    def convert_python_to_c(self, wrapper):
        wrapper.parse_params.add_parameter("k", ["&"+self.value], prepend=True)

    def convert_c_to_python(self, wrapper):
        wrapper.build_params.add_parameter("k", [self.value], prepend=True)